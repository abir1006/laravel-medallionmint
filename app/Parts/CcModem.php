<?php

namespace App\Parts;

use Illuminate\Database\Eloquent\Model;

class CcModem extends Model
{
    protected $fillable= ['model_id','serial'];

    public function machine(){

        return $this->hasMany('App\Machine');
    }

    public function setModelIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['model_id'] = $value ;
    }

    public function model(){
        return $this->belongsTo('App\MachineSettings','model_id');
    }

    public function getFullTextAttribute(){

        $full_text = '';
        foreach(json_decode($this->model->data) as $key=>$value){
            $full_text.= $value.' | ';
        }

        return $full_text.$this->serial;
    }
}
