<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	
	protected $table = 'users';	
	protected $fillable = ['active', 'name', 'email', 'password','role'];
	protected $hidden = ['password', 'remember_token'];

	public function locations(){
		return $this->belongsToMany('App\Location','location_managers');
	}

	public function companies(){
		return $this->belongsToMany('App\Company','company_managers');
	}

	// though admin has all location access NEVER return true for admin user
	public function hasLocationAccess($location_id){		

		if($this->role == 'location_manager' && $this->locations->where('id',$location_id)->count()){
			return true;
		}		

		if($this->role == 'company_manager' && $this->companies->first()->locations->where('id',$location_id)->count()){
			return true;
		}

		return false;		
	}
	

}
