<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Dies extends Model {

    protected $fillable = ['active', 'name','location_id', 'cabinet','drawer','row', 'col','photo'];

    public function location(){

        return $this->belongsTo('App\Location');
    }

}
