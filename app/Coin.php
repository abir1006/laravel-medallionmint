<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model {

    protected $fillable = [
        'active',
        'name',
        'location_id',
        'top_inventory',
        'inventory',
        'notes',
        'metal',
        'finish',
        'front_dies_type',
        'back_dies_type',
        'front_dies',
        'back_dies'
    ];

    public function location(){

        return $this->belongsTo('App\Location');
    }
    public function front_dies(){

        return Dies::where('id','=',$this->front_dies)->first();
    }

    public function back_dies(){

        return Dies::where('id','=',$this->back_dies)->first();
    }

    /*public function machine(){
        return $this->hasMany('App\Machine','coin_1');
    }*/

}
