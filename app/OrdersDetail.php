<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersDetail extends Model
{
    protected $table = 'orders_details';

    protected $hidden = ['id', 'order_id', 'coin_id'];

    protected $guarded = ['id', 'order_id', 'coin_id'];

    public function Order(){
        $this->belongsTo('App\Order');
    }

    public function Coin() {
        return $this->belongsTo('App\Coin');
    }

    public function getDates() {
        return ['boxed_at','sprayed_at','stamped_at','created_at', 'updated_at'];
    }
}
