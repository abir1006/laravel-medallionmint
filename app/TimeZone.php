<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeZone extends Model
{
	protected $table = 'time_zones';

	protected $fillable = ['name', 'value', 'active'];
}
