<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class LocationRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'name'=>'required',
			'rep_company_id' => 'required'
		];
	}

	public function messages()
	{
		return [
		'name.required' => 'Location Name is required.',
		'rep_company_id.required' => 'Rep Company is required.'
		];
	}
}
