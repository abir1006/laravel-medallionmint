<?php

namespace App\Http\Requests\Manager;

use App\Http\Requests\Request;

class ManagerUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(\App\User::findOrFail($this->managers)->hasLocationAccess($this->location)){
            return true;
        }
        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $manager = \App\User::find($this->managers);        
    
        $rules = [

            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$manager->id,
            'password'=> 'min:3'
        ];        

        return $rules;
    }
}
