<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Route;

class DiesRequest extends Request {

    //protected $dontFlash = ['die_photo'];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {



        if($this->isMethod('PATCH')){

            $rules = [

                'name'=>'required',
                'location_id'=>'required',
                'cabinet' => 'required',
                'drawer' => 'required',
                'row' => 'required',
                'col' => 'required',
                'photo' =>'image'
            ];

        }else{

            $rules = [

                'name'=>'required',
                'location_id'=>'required',
                'cabinet' => 'required',
                'drawer' => 'required',
                'row' => 'required',
                'col' => 'required',
                'photo' =>'required|image'
            ];

        }



        return $rules;
    }

    /*
    public function messages()
    {
        return [
            'photo.required' => 'custom die photo required msg'
        ];
    }
    */

}
