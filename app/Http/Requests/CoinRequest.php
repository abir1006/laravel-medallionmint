<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CoinRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'location_id'=>'required',
            'front_dies'=>'required',
            'back_dies'=>'required',
        ];
    }

    public function messages()
    {
	    return [
	        'name.required' => 'Coin name is required.',
	        'location_id.required' => 'Location is required.',
	        'front_dies.required' => 'Front Die is required.',
	        'back_dies.required' => 'Back Die is required.',

	    ];
    }

}
