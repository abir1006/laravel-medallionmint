<?php

namespace App\Http\Controllers\admin;

use App\Transaction;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $today = date('Y-m-d');
        //$today = date('2016-12-21');
        $today_transactions = Transaction::where('transaction_time','>=',$today)->get();

        $today_summary = array();

        $today_summary['total_amount'] = $today_transactions->sum('total_amount');
        $today_summary['total_cc_amount'] = $today_transactions->where('transaction_type','R')->sum('total_amount');
        $today_summary['total_cash_amount'] = $today_transactions->where('transaction_type','C')->sum('total_amount');
        $today_summary['total_coins'] = $today_transactions->sum('items');
        $today_summary['total_transactions'] = $today_transactions->groupBy('transaction_ref')->count();

        if($today_summary['total_amount']){
            $today_summary['cc_percent'] = round( $today_summary['total_cc_amount'] / $today_summary['total_amount'] * 100 );
            $today_summary['cash_percent'] = round( $today_summary['total_cash_amount'] / $today_summary['total_amount'] * 100 );
        }else{
            $today_summary['cc_percent'] = 0;
            $today_summary['cash_percent'] = 0;
        }

        if($today_summary['total_coins']){
            $today_summary['per_trans'] = round($today_summary['total_amount'] / $today_summary['total_coins']);
        }else{
            $today_summary['per_trans'] = 0;
        }

        //return $today_summary;

        $transactions = Transaction::with(['machine'=>function($query){
            $query->with('location');

        },'coin'])->orderBy('transaction_time','desc')->paginate(100);

        return view('admin.transactions.index',compact('transactions','today_summary'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
