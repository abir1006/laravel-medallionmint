<?php namespace App\Http\Controllers\admin;

use App\Location;
use App\Machine;
use App\MachineSettings;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\MachineRequest;

use App\Parts\BillValidator;
use App\Parts\CcModem;
use App\Parts\CcReader;
use App\Parts\Gizmo;
use App\Parts\Hopper;
use App\Parts\MeterBox;
use App\Parts\TouchScreen;
use App\Parts\Vault;
use App\Transaction;
use Illuminate\Http\Request;

use File;
use Image;

class AdminMachinesController extends Controller {


	public function index()
	{
		$machines = Machine::with(['location','cc_modem'])->get();
		return view('admin.machines.index',compact('machines'));
	}


	public function create()
	{
        $locations = Location::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();


        $machine_settings = MachineSettings::all()->groupBy('setting');

        $bill_validators = BillValidator::all()->lists('full_text','id')->toArray();
        $cc_modems = CcModem::all()->lists('full_text','id')->toArray();
        $cc_readers = CcReader::all()->lists('full_text','id')->toArray();
        $gizmos = Gizmo::all()->lists('full_text','id')->toArray();
        $hoppers = Hopper::all()->lists('full_text','id')->toArray();
        $meter_boxes = MeterBox::all()->lists('full_text','id')->toArray();
        $touch_screens = TouchScreen::all()->lists('full_text','id')->toArray();
        $vaults = Vault::all()->lists('full_text','id')->toArray();

		return view('admin.machines.create',compact(
		    'locations',
            'machine_settings',
            'bill_validators',
            'cc_modems',
            'cc_readers',
            'gizmos',
            'hoppers',
            'meter_boxes',
            'touch_screens',
            'vaults'

        ));
	}


	public function store(MachineRequest $request)
	{
		$request['date_shipped'] = date('Y-m-d',strtotime($request['date_shipped']));
		$request['date_installed'] = date('Y-m-d',strtotime($request['date_installed']));

        $request['active'] = isset($request['active']);
		$request['has_cc']= isset($request['has_cc']);


		Machine::create($request->all());

		return redirect()->back()->with([
            'flash_message' => 'New Machine created!'
        ]);
                
	}


	public function show($id)
	{
		$machine = Machine::findOrFail($id);
		return view('admin.machines.show',compact('machine'));
	}


	public function edit($id)
	{

		$machine = Machine::with(['transactions'=>function($query){
                $query->with('coin')->orderBy('transaction_time','desc')->take(100);
        }])->findOrFail($id);

		$locations = Location::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
        $machine_settings = MachineSettings::all()->groupBy('setting');

        $bill_validators = BillValidator::all()->lists('full_text','id')->toArray();
        $cc_modems = CcModem::all()->lists('full_text','id')->toArray();
        $cc_readers = CcReader::all()->lists('full_text','id')->toArray();
        $gizmos = Gizmo::all()->lists('full_text','id')->toArray();
        $hoppers = Hopper::all()->lists('full_text','id')->toArray();
        $meter_boxes = MeterBox::all()->lists('full_text','id')->toArray();
        $touch_screens = TouchScreen::all()->lists('full_text','id')->toArray();
        $vaults = Vault::all()->lists('full_text','id')->toArray();

        $machine->first_transaction = Transaction::where('machine_id',$machine->id)->orderBy('transaction_time','asc')->first();
        $machine->last_transaction = Transaction::where('machine_id',$machine->id)->orderBy('transaction_time','desc')->first();

        //return $machine;


        return view('admin.machines.edit',compact(
            'machine',
            'locations',
            'machine_settings',
            'bill_validators',
            'cc_modems',
            'cc_readers',
            'gizmos',
            'hoppers',
            'meter_boxes',
            'touch_screens',
            'vaults'
        ));


	}


	public function update($id, MachineRequest $request)
	{
		$machine = Machine::findOrFail($id);

		$request['date_shipped'] = date('Y-m-d',strtotime($request['date_shipped']));
		$request['date_installed'] = date('Y-m-d',strtotime($request['date_installed']));

        $request['active'] = isset($request['active']);
        $request['has_cc']= isset($request['has_cc']);

		$machine->update($request->all());

		return redirect()->back()->with([
				'flash_message'=>'Machine Updated Successfully'

			]);

	}


	public function destroy($id)
	{
		$machine = Machine::findOrFail($id);
		$machine->delete();

		return response('success');
	}


	/**************************************************************************************/
	/**************************************************************************************/

	public function addPhoto($machine_id, Request $request){
		

		// also has dropzone js validation enabled (optional)
		$this->validate($request,[
			'photo' => 'required|mimes:jpg,jpeg,png,btm'
		]);

		$machine = Machine::findOrFail($machine_id);

		
		$file = $request->file('photo');
		$filename = time().'_' . $file->getClientOriginalName();
	    $destinationPath = public_path() . '/upload/machine_photo/';
	    $file->move($destinationPath, $filename);

	    $img = Image::make($destinationPath.$filename);        

        $img->resize(200, null, function($constraint){$constraint->aspectRatio();})
            ->save($destinationPath.'200-'.$filename);

         $img->resize(75, null, function($constraint){$constraint->aspectRatio();})
            ->save($destinationPath.'75-'.$filename);

        $img->destroy();

        $photo_id = $machine->photos()->create([        	
        	'name' => $filename
        ])->id;

		return response([
			'photo_id'=>$photo_id,
			'machine_id' => $machine_id,
			'thumb'=>$filename
		]);

	}

	public function delPhoto($machine_id,$photo_id){
		$machine = Machine::findOrFail($machine_id);

		if($machine->default_photo_id == $photo_id){
			$machine->default_photo_id = 0;
			$machine->save();
		}

		$photo = $machine->photos->find($photo_id);


		if($photo->delete()){
			File::delete(public_path().'/upload/machine_photo/'.$photo->name);
			File::delete(public_path().'/upload/machine_photo/200-'.$photo->name);
			File::delete(public_path().'/upload/machine_photo/75-'.$photo->name);
			return 'success';
		}else{
			return 'Failed to delete';
		}

	}

	public function setDefaultPhoto($machine_id,$photo_id){
		$machine = Machine::findOrFail($machine_id);
		$machine->default_photo_id = $photo_id;
		$machine->save();

		return 'success';
	}

}
