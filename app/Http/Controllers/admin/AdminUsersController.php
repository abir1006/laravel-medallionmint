<?php namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\User;
use App\Location;
use App\Company;
use \Hash;

class AdminUsersController extends Controller {

	
	public function index()
	{
		$locations = Location::where('active','=','1')->orderBy('name', 'asc')->lists('name','id')->all();
		$users = User::orderBy('role')->orderby('created_at','desc')->get();
        return view('admin.users.index',compact('users','locations'));
	}

	public function profile(){

		$user = \Auth::user();
		return view('admin.users.edit',compact('user'));
	}
	
	public function create()
	{
		$locations = Location::where('active','=','1')->orderBy('name', 'asc')->lists('name','id')->all();
		$companies = Company::where('active','=','1')->orderBy('name', 'asc')->lists('name','id')->all();
		return view('admin.users.create',compact('locations','companies'));
	}

	
	public function store(UserRequest $request)
	{

        $request['active'] = isset($request['active']);

		$request->merge(['password' => Hash::make($request->password)]);
		$user = User::create($request->all());

		if($request->role == 'admin'){
			// maybe send a email notification to others admin
		}else if($request->role == 'company_manager'){			
			$user->companies()->attach($request['company_id']);
		}else if($request->role == 'location_manager'){
			$user->locations()->attach($request['location_id']);
		}


		return redirect()->back()->with([
			'flash_message' => 'New '.$request->role.' Created'
			]);
	}

	
	public function show($id)
	{
		//
	}

	
	public function edit($id)
	{
		$user = User::findOrFail($id);
		return view('admin.users.edit',compact('user'));
	}

	
	public function update($id,UserRequest $request)
	{
		$user = User::findOrFail($id);

        $request['active'] = isset($request['active']);

		if($request['password']==''){
			$user->update($request->except('password'));
		}else{
			$user->update($request->except('password')+array('password'=>Hash::make($request['password'])));
		}

		return redirect('admin/users')->with([
				'flash_message' => $user->name.' user info updated'
			]);

	}
	
	public function attach(Request $data){
		$user = User::findOrFail($data['user_id']);
		//$user->locations()->attach($data['location_id']);
		$user->locations()->sync($data['location_id'], false);

		return redirect()->back()->with([
			'flash_message' => 'Location Access updated'
			]);

	}


	public function remove($user_id,$location_id){
		
		$user = User::findOrFail($user_id);
		$user->locations()->detach($location_id);
		return response('success');
	}


	public function destroy($id)
	{
		$user = User::findOrFail($id);
		$user->delete();

		return response('success');
	}

}
