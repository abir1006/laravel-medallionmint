<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CarrierRequest;
use App\Carrier;

class AdminCarriersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oCarriers = Carrier::all();
	    return view('admin.carriers.index',compact('oCarriers'));

//	    $oPrograms = Carrier::all();
//	    return view('admin.programs.index',compact('oPrograms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.carriers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CarrierRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarrierRequest $request)
    {
	    if(!isset($request['active'])){
		    $request['active']=0;
	    }

	    $request['slug']=strtolower($request['name']);

	    Carrier::create($request->all());

	    return redirect('admin/carriers')->with([
	    	'flash_message' => '<b>' . $request['name'] . '</b> created!'
	    ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	    $oCarrier = Carrier::where('id', $id)->firstOrFail();

	    return view('admin.carriers.edit',compact('oCarrier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\CarrierRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarrierRequest $request, $id)
    {
        $carrier = Carrier::where('id',$id)->firstOrFail();

	    if(!isset($request['active'])){
		    $request['active']=0;
	    }

	    $request['slug']=strtolower($request['name']);

	    $carrier->update($request->all());

	    return redirect('admin/carriers')->with([
	    	'flash_message' => '<b>' . $request->name . "</b> updated!"
	    ]);
    }

	/**
	 * Change from enabled to disabled
	 *
	 * @param int $id
	 * @return Response
	 */


	public function disable($id){

//		$this->_checkAjaxRequest();
		//$this->_debugging($token);
		$oCarrier = Carrier::findOrFail($id);
		if(!$oCarrier) {
			return ('No record found');
		}

		if($oCarrier->active === 1){
			$oCarrier->active = 0;
//			return response('false');
		}else{
			$oCarrier->active = 1;
//			return response('true');
		}

		//$this->_debugging($oProgram);

		$oCarrier->save();

		return response($oCarrier->active);

	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oCarrier = Carrier::findOrFail($id);

	    $oCarrier->delete();

	    return response('success');
    }
}
