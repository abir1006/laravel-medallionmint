<?php namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\MachineSettingsRequest;
use Illuminate\Http\Request;

use App\MachineSettings;

class AdminMachineSettingsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$MachineSettings = MachineSettings::get();

		return view('admin.machine_settings.index',compact('MachineSettings'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(MachineSettingsRequest $request)
	{

		$data = json_encode($request->except(['setting','_token']));

		$setting = MachineSettings::create([
			'setting' => $request['setting'],
			'data'	=> $data
		]);

		return response([
			'id' => $setting->id,
			'data' => $data
		]);

		/*
		return redirect()->back()->with([
            'flash_message' => '<b>' . $request['setting'] . '</b> created!'
        ]);;
        */
		
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,MachineSettingsRequest $request)
	{
		$data = json_encode($request->except(['setting','_token','_method']));
		$MachineSetting = MachineSettings::findOrFail($id);

		$MachineSetting->setting = $request['setting'];
		$MachineSetting->data = $data;
		$MachineSetting->update();	

		return response([
			'status' => 'success',
			'data' => $data
		]);

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
    {
       	MachineSettings::findOrFail($id)->delete();
        

        return response('success');
    }

}
