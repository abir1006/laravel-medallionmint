<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Carrier;
use App\Location;
use App\ManagingCompany;
use App\Term;
use App\TimeZone;
use App\Http\Requests\ManagingCompanyRequest;

class AdminManagingCompaniesController extends Controller
{

    public function index()
    {
	    $managing_companies = ManagingCompany::with('locations','machines')->orderBy('name', 'asc')->get();
	    return view('admin.managing_companies.index',compact('managing_companies'));
    }


    public function create()
    {
	    $carriers = Carrier::where('active',true)->orderBy('name', 'asc')->lists('name', 'id')->all();
	    $terms = Term::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
	    $time_zones = TimeZone::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
	    return view('admin.managing_companies.create', compact('carriers', 'terms','time_zones'));
    }


    public function store(ManagingCompanyRequest $request)
    {
        $request['active'] = isset($request['active']);

	    ManagingCompany::create($request->all());

	    return redirect('admin/managing_companies')->with([
		    'flash_message' => '<b>' . $request['name'] . '</b> created!'
	    ]);
    }


    public function show($id)
    {
	    $managing_company = ManagingCompany::findOrFail($id);

        $time_zones = TimeZone::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();


        return view('admin.managing_companies.show',compact('managing_company','time_zones'));
    }

    public function edit($id)
    {
	    $managing_company = ManagingCompany::where('id', $id)->firstOrFail();

	    $carriers = Carrier::where('active',true)->orderBy('name', 'asc')->lists('name', 'id')->all();
	    $terms = Term::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
        $time_zones = TimeZone::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();

        return view('admin.managing_companies.edit',compact('managing_company', 'carriers', 'terms','time_zones'));
    }

    public function update(ManagingCompanyRequest $request, $id)
    {
        $managing_company = ManagingCompany::findOrFail($id);

        $request['active'] = isset($request['active']);
	    $managing_company->update($request->all());

	    return redirect('admin/managing_companies')->with([
		    'flash_message'=> '<b>' . $request['name'] . '</b> updated!'
	    ]);
    }


    public function destroy($id)
    {
	    $managing_company = ManagingCompany::findOrFail($id);
        $managing_company->addresses()->delete();
        $managing_company->delete();

	    return response('success');
    }
}
