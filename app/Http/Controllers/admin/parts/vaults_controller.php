<?php

namespace App\Http\Controllers\admin\parts;

use App\MachineSettings;
use App\Parts\Vault;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class vaults_controller extends Controller
{
    public function index()
    {
        $vaults = Vault::all();

        return view('admin.parts.vaults.index',compact('vaults'));
    }


    public function create()
    {
        $models = MachineSettings::where('setting','vault_models')->get();
        return view('admin.parts.vaults.create',compact('models'));
    }


    public function store(Request $request)
    {

        Vault::create($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Vault Added'
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $models = MachineSettings::where('setting','vault_models')->get();
        $vault = Vault::findOrFail($id);

        return view('admin.parts.vaults.edit',compact('vault','models'));
    }


    public function update(Request $request, $id)
    {
        $vault = Vault::findOrFail($id);

        $vault->update($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Vault Updated'
        ]);
    }

    public function destroy($id)
    {
        $vault = Vault::findOrFail($id);
        $vault->delete();

        return response('success');
    }
}
