<?php

namespace App\Http\Controllers\admin\parts;

use App\MachineSettings;
use App\Parts\Hopper;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class hoppers_controller extends Controller
{
    public function index()
    {
        $hoppers = Hopper::all();

        return view('admin.parts.hoppers.index',compact('hoppers'));
    }


    public function create()
    {
        $models = MachineSettings::where('setting','hopper_models')->get();
        return view('admin.parts.hoppers.create',compact('models'));
    }


    public function store(Request $request)
    {

        Hopper::create($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Hopper Added'
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $models = MachineSettings::where('setting','hopper_models')->get();
        $hopper = Hopper::findOrFail($id);

        return view('admin.parts.hoppers.edit',compact('hopper','models'));
    }


    public function update(Request $request, $id)
    {
        $hopper = Hopper::findOrFail($id);

        $hopper->update($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Hopper Updated'
        ]);
    }

    public function destroy($id)
    {
        $hopper = Hopper::findOrFail($id);
        $hopper->delete();

        return response('success');
    }
}
