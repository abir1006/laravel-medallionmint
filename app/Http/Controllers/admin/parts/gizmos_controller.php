<?php

namespace App\Http\Controllers\admin\parts;

use App\MachineSettings;
use App\Parts\Gizmo;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class gizmos_controller extends Controller
{
    public function index()
    {
        $gizmos = Gizmo::all();

        return view('admin.parts.gizmos.index',compact('gizmos'));
    }


    public function create()
    {
        $models = MachineSettings::where('setting','gizmo_models')->get();
        return view('admin.parts.gizmos.create',compact('models'));
    }


    public function store(Request $request)
    {

        Gizmo::create($request->all());

        return redirect()->back()->with([
            'flash_message' => 'New Gizmo Created'
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $models = MachineSettings::where('setting','gizmo_models')->get();
        $gizmo = Gizmo::findOrFail($id);

        return view('admin.parts.gizmos.edit',compact('gizmo','models'));
    }


    public function update(Request $request, $id)
    {
        $gizmo = Gizmo::findOrFail($id);

        $gizmo->update($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Gizmo Updated'
        ]);
    }

    public function destroy($id)
    {
        $gizmo = Gizmo::findOrFail($id);
        $gizmo->delete();

        return response('success');
    }
}
