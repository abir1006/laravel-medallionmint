<?php

namespace App\Http\Controllers\admin\parts;

use App\MachineSettings;
use App\Parts\TouchScreen;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class touch_screens_controller extends Controller
{
    public function index()
    {
        $touch_screens = TouchScreen::all();

        return view('admin.parts.touch_screens.index',compact('touch_screens'));
    }


    public function create()
    {
        $models = MachineSettings::where('setting','touch_screen_models')->get();

        return view('admin.parts.touch_screens.create',compact('models'));
    }


    public function store(Request $request)
    {

        TouchScreen::create($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Touch Screen Added'
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $models = MachineSettings::where('setting','touch_screen_models')->get();
        $touch_screen = TouchScreen::findOrFail($id);

        return view('admin.parts.touch_screens.edit',compact('touch_screen','models'));
    }


    public function update(Request $request, $id)
    {
        $touch_screen = TouchScreen::findOrFail($id);

        $touch_screen->update($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Touch Screen Updated'
        ]);
    }

    public function destroy($id)
    {
        $touch_screen = TouchScreen::findOrFail($id);
        $touch_screen->delete();

        return response('success');
    }
}
