<?php

namespace App\Http\Controllers\admin\parts;

use App\MachineSettings;
use App\Parts\CcModem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class cc_modems_controller extends Controller
{

    public function index()
    {
        $cc_modems = CcModem::all();

        return view('admin.parts.cc_modems.index',compact('cc_modems'));
    }


    public function create()
    {
        $models = MachineSettings::where('setting','cc_modem_models')->get();
        return view('admin.parts.cc_modems.create',compact('models'));
    }


    public function store(Request $request)
    {

        CcModem::create($request->all());

        return redirect()->back()->with([
            'flash_message' => 'CC Modem Added'
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $models = MachineSettings::where('setting','cc_modem_models')->get();
        $cc_modem = CcModem::findOrFail($id);

        return view('admin.parts.cc_modems.edit',compact('cc_modem','models'));
    }


    public function update(Request $request, $id)
    {
        $cc_modem = CcModem::findOrFail($id);

        $cc_modem->update($request->all());

        return redirect()->back()->with([
            'flash_message' => 'CC Modem Updated'
        ]);
    }

    public function destroy($id)
    {
        $cc_modem = CcModem::findOrFail($id);
        $cc_modem->delete();

        return response('success');
    }
}
