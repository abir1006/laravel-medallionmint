<?php

namespace App\Http\Controllers\admin\parts;

use App\MachineSettings;
use App\Parts\MeterBox;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class meter_boxes_controller extends Controller
{
    public function index()
    {
        $meter_boxes = MeterBox::all();

        return view('admin.parts.meter_boxes.index',compact('meter_boxes'));
    }


    public function create()
    {
        $models = MachineSettings::where('setting','meter_box_models')->get();
        return view('admin.parts.meter_boxes.create',compact('models'));
    }


    public function store(Request $request)
    {

        MeterBox::create($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Meter Box Added'
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $models = MachineSettings::where('setting','meter_box_models')->get();
        $meter_box = MeterBox::findOrFail($id);

        return view('admin.parts.meter_boxes.edit',compact('meter_box','models'));
    }


    public function update(Request $request, $id)
    {
        $meter_box = MeterBox::findOrFail($id);

        $meter_box->update($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Meter Box Updated'
        ]);
    }

    public function destroy($id)
    {
        $meter_box = MeterBox::findOrFail($id);
        $meter_box->delete();

        return response('success');
    }
}
