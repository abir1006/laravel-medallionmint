<?php

namespace App\Http\Controllers\admin\parts;

use App\MachineSettings;
use App\Parts\BillValidator;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class bill_validators_controller extends Controller
{

    public function index()
    {
        $bill_validators = BillValidator::all();

        return view('admin.parts.bill_validators.index',compact('bill_validators'));
    }


    public function create()
    {
        $models = MachineSettings::where('setting','bill_validator_models')->get();
        return view('admin.parts.bill_validators.create',compact('models'));
    }


    public function store(Request $request)
    {

        BillValidator::create($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Bill Validator Added'
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $models = MachineSettings::where('setting','bill_validator_models')->get();
        $bill_validator = BillValidator::findOrFail($id);

        return view('admin.parts.bill_validators.edit',compact('bill_validator','models'));
    }


    public function update(Request $request, $id)
    {
        $bill_validator = BillValidator::findOrFail($id);

        $bill_validator->update($request->all());

        return redirect()->back()->with([
            'flash_message' => 'Bill Validator Updated'
        ]);
    }

    public function destroy($id)
    {
        $bill_validator = BillValidator::findOrFail($id);
        $bill_validator->delete();

        return response('success');
    }
}
