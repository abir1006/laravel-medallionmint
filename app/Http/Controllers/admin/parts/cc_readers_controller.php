<?php

namespace App\Http\Controllers\admin\parts;

use App\MachineSettings;
use App\Parts\CcReader;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class cc_readers_controller extends Controller
{
    public function index()
    {
        $cc_readers = CcReader::all();

        return view('admin.parts.cc_readers.index',compact('cc_readers'));
    }


    public function create()
    {
        $models = MachineSettings::where('setting','cc_reader_models')->get();
        return view('admin.parts.cc_readers.create',compact('models'));
    }


    public function store(Request $request)
    {

        CcReader::create($request->all());

        return redirect()->back()->with([
            'flash_message' => 'CC Reader Added'
        ]);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $models = MachineSettings::where('setting','cc_reader_models')->get();
        $cc_reader = CcReader::findOrFail($id);

        return view('admin.parts.cc_readers.edit',compact('cc_reader','models'));
    }


    public function update(Request $request, $id)
    {
        $cc_reader = CcReader::findOrFail($id);

        $cc_reader->update($request->all());

        return redirect()->back()->with([
            'flash_message' => 'CC Reader Updated'
        ]);
    }

    public function destroy($id)
    {
        $cc_reader = CcReader::findOrFail($id);
        $cc_reader->delete();

        return response('success');
    }
}
