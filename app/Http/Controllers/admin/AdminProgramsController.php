<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ProgramRequest;
use App\Program;

class AdminProgramsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
	    $oPrograms = Program::all();
	    return view('admin.programs.index',compact('oPrograms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
	    return view('admin.programs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProgramRequest  $request
     * @return Response
     */
    public function store(ProgramRequest $request)
    {
	    if(!isset($request['active'])){
		    $request['active']=0;
	    }

	    Program::create($request->all());

	    return redirect('admin/programs')->with([
		    'flash_message' => '<b>' . $request['name'] . '</b> created!'
	    ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
	    $oProgram = Program::where('id', $id)->firstOrFail();

	    return view('admin.programs.edit',compact('oProgram'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProgramRequest  $request
     * @param  int  $id
     * @return Response
     */
    public function update(ProgramRequest $request, $id)
    {
	    $program = Program::where('id', $id)->firstOrFail();

	    if(!isset($request['active'])){
		    $request['active']=0;
	    }

	    $program->update($request->all());

	    return redirect('admin/programs')->with([
		    'flash_message' => '<b>' . $request['name'] . '</b> updated!'
	    ]);
    }


	/**
	 * Change from enabled to disabled
	 *
	 * @param int $id
	 * @return Response
	 */


	public function disable($id){

//		$this->_checkAjaxRequest();
		//$this->_debugging($token);
		$oProgram = Program::findOrFail($id);
		if(!$oProgram) {
			return ('No record found');
		}

		if($oProgram->active === 1){
			$oProgram->active = 0;
//			return response('false');
		}else{
			$oProgram->active = 1;
//			return response('true');
		}

		//$this->_debugging($oProgram);

		$oProgram->save();

		return response($oProgram->active);

	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
	    $program = Program::findOrFail($id);


	    $program->delete();

	    return response('success');
    }
}
