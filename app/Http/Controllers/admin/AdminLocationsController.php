<?php namespace App\Http\Controllers\admin;


use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\LocationRequest;
use App\TimeZone;
use Illuminate\Http\Request;

use App\Carrier;
use App\Coin;
use App\Company;
use App\Dies;
use App\Location;
use App\LocationType;
use App\Machine;
use App\ManagingCompany;
use App\MmRep;
use App\Program;
use App\RepCompany;
use App\SalesType;
use App\Split;
use App\Term;


use Image;
use File;

class AdminLocationsController extends Controller {


	public function index()
	{
		$locations = Location::with('machines','company','rep_company','managing_company')->orderBy('name', 'asc')->get();
		return view('admin.locations.index',compact('locations'));
		
	}


	public function create()
	{

		$carriers = Carrier::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$companies = Company::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$location_types = LocationType::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$managing_companies = ManagingCompany::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$mm_reps = MmRep::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$programs = Program::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$rep_companies = RepCompany::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$sales_types = SalesType::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$splits = Split::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$terms = Term::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$time_zones = TimeZone::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();

		return view('admin.locations.create',compact(
			'carriers',
			'companies',
			'location_types',
			'managing_companies',
			'mm_reps',
			'programs',
			'rep_companies',
			'sales_types',
			'splits',
			'terms',
			'time_zones'
		));
	}


	public function store(LocationRequest $request)
	{

        $request['active'] = isset($request['active']);
        $request['has_custom_cc_percent'] = isset($request['has_custom_cc_percent']);
        $request['has_seasons'] = isset($request['has_seasons']);
        $request['has_tax_stamp'] = isset($request['has_tax_stamp']);

        $request['season_start_at'] = date('Y-m-d',strtotime($request['season_start_at']));
        $request['season_end_at'] = date('Y-m-d',strtotime($request['season_end_at']));

        $request['is_exclude'] = isset($request['is_exclude']);


        //return $request->all();

        Location::create($request->all());


		return redirect('admin/locations')->with([
            'flash_message' => '<b>' . $request['name'] . '</b> created!'
        ]);
	}


	public function show($id)
	{
		$location = Location::findOrFail($id);

        $time_zones = TimeZone::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();


        if($location->report->count()){
            $last_report_created_ago = round( (time() - strtotime($location->report()->orderBy('created_at','desc')->first()->created_at)) / (3600*24) );
        }else{
        	$last_report_created_ago = 99999999;
        }        

		return view('admin.locations.show',compact('location','last_report_created_ago','time_zones'));
	}


	public function edit($id)
	{

		$location = Location::findOrFail($id);		
		$carriers = Carrier::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$companies = Company::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$location_types = LocationType::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$managing_companies = ManagingCompany::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$mm_reps = MmRep::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$programs = Program::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$rep_companies = RepCompany::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$sales_types = SalesType::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$splits = Split::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$terms = Term::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$time_zones = TimeZone::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();

		return view('admin.locations.edit')->with(
			array(
				'carriers'=>$carriers,
				'companies'=>$companies,
				'location'=>$location,
				'location_types'=>$location_types,
				'managing_companies'=>$managing_companies,
				'mm_reps'=>$mm_reps,
				'programs'=>$programs,
				'rep_companies'=>$rep_companies,
				'sales_types'=>$sales_types,
				'splits'=>$splits,
				'terms'=>$terms,
				'time_zones'=>$time_zones
			)
		);
	}


	public function update($id, LocationRequest $request)
	{
		$location = Location::findOrFail($id);

        $request['active'] = isset($request['active']);
        $request['has_custom_cc_percent'] = isset($request['has_custom_cc_percent']);
        $request['has_seasons'] = isset($request['has_seasons']);
        $request['has_tax_stamp'] = isset($request['has_tax_stamp']);

        $request['is_exclude'] = isset($request['is_exclude']);


        $request['season_start_at'] = date('Y-m-d',strtotime($request['season_start_at']));
        $request['season_end_at'] = date('Y-m-d',strtotime($request['season_end_at']));

        $location->update($request->all());

		return redirect()->back()->with([
            'flash_message'=> '<b>' . $request['name'] . '</b> updated!'
        ]);
	}




	public function destroy($id)
	{
		$location = Location::findOrFail($id);

		// delete location photos

		// maybe need to delete many others photo ex. machines photo, dies photo etc....		
        
        $location->addresses()->delete();
		$location->delete();

		return response('success');
	}


	/**************************************************************************************/
	/**************************************************************************************/

	public function addPhoto($location_id, Request $request){
		

		// also has dropzone js validation enabled (optional)
		$this->validate($request,[
			'photo' => 'required|mimes:jpg,jpeg,png,btm'
		]);

		$location = Location::findOrFail($location_id);

		
		$file = $request->file('photo');
		$filename = time().'_' . $file->getClientOriginalName();
	    $destinationPath = public_path() . '/upload/location_photo/';
	    $file->move($destinationPath, $filename);

	    $img = Image::make($destinationPath.$filename);        

        $img->resize(200, null, function($constraint){$constraint->aspectRatio();})
            ->save($destinationPath.'200-'.$filename);

         $img->resize(75, null, function($constraint){$constraint->aspectRatio();})
            ->save($destinationPath.'75-'.$filename);

        $img->destroy();

        $photo_id = $location->photos()->create([        	
        	'name' => $filename
        ])->id;

		return response([
			'photo_id'=>$photo_id,
			'location_id' => $location_id,
			'thumb'=>$filename
		]);

	}

	public function delPhoto($location_id,$photo_id){
		$location = Location::findOrFail($location_id);

		if($location->default_photo_id == $photo_id){
			$location->default_photo_id = 0;
			$location->save();
		}

		$photo = $location->photos->find($photo_id);


		if($photo->delete()){
			File::delete(public_path().'/upload/location_photo/'.$photo->name);
			File::delete(public_path().'/upload/location_photo/200-'.$photo->name);
			File::delete(public_path().'/upload/location_photo/75-'.$photo->name);
			return 'success';
		}else{
			return 'Failed to delete';
		}

	}

	public function setDefaultPhoto($location_id,$photo_id){
		$location = Location::findOrFail($location_id);
		$location->default_photo_id = $photo_id;
		$location->save();

		return 'success';
	}
}
