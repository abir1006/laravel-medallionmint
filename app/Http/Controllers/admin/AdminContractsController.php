<?php

namespace App\Http\Controllers\admin;

use App\Contract;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminContractsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'contrcts index';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }


    public function store(Request $request,$location_id)
    {
        $contract = Contract::create([
            'location_id'   => $location_id,
            'terms'         => $request['terms'],
            'start_date'    => date('Y-m-d',strtotime($request['start_date'])),
            'end_date'      => date('Y-m-d',strtotime($request['end_date'])),
            'auto_renew'    => isset($request['auto_renew'])
        ]);

        return redirect()->back()->with([
            'flash_message' => 'New Contract added'
        ]);


    }


    public function show($location_id,$contract_id)
    {
        return Contract::findOrFail($contract_id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function update(Request $request, $location_id,$contract_id)
    {
        $contract = Contract::findOrFail($contract_id);

        $contract->terms = $request['terms'];
        $contract->start_date = date('Y-m-d',strtotime($request['start_date']));
        $contract->end_date = date('Y-m-d',strtotime($request['end_date']));
        $contract->auto_renew = isset($request['auto_renew']);

        $contract->save();

        return redirect()->back()->with([
            'flash_message' => 'Contract Updated'
        ]);
    }


    public function destroy($location_id,$contract_id)
    {
        $contract = Contract::findOrFail($contract_id);

        $contract->delete();

        return response('success');
    }
}
