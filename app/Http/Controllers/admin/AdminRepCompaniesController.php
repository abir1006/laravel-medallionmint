<?php

namespace App\Http\Controllers\admin;

use App\RepCompany;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\RepCompanyRequest;
use App\Http\Controllers\Controller;
use App\TimeZone;
use App\Term;
use App\Carrier;

class AdminRepCompaniesController extends Controller
{

    public function index()
    {
        $rep_companies = RepCompany::with('locations','machines')->orderBy('name', 'asc')->get();

        return view('admin.rep_companies.index',compact('rep_companies'));
    }

    public function create()
    {

        $carriers = Carrier::where('active',1)->lists('name','id')->all();
        $time_zones = TimeZone::where('active',1)->lists('name','id')->all();
        $terms = Term::where('active',1)->lists('name','id')->all();
        return view('admin.rep_companies.create',compact('carriers','time_zones','terms'));
    }


    public function store(RepCompanyRequest $request)
    {
        $request['active'] = isset($request['active']);

        RepCompany::create($request->all());

        return redirect('admin/rep_companies')->with([
            'flash_message' => '<b>' . $request['name'] . '</b> created!'
        ]);
    }


    public function show($id)
    {
        $rep_company = RepCompany::findOrFail($id);
        $time_zones = TimeZone::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();


        return view('admin.rep_companies.show',compact('rep_company','time_zones'));
    }


    public function edit($id)
    {
        $rep_company = RepCompany::findOrFail($id);

        $carriers = \App\Carrier::where('active',1)->lists('name','id')->all();
        $time_zones = \App\TimeZone::where('active',1)->lists('name','id')->all();
        $terms = Term::where('active',1)->lists('name','id')->all();


        return view('admin.rep_companies.edit',compact('rep_company','carriers','time_zones','terms'));
    }


    public function update(RepCompanyRequest $request, $id)
    {
        $request['active'] = isset($request['active']);

        $rep_company = RepCompany::findOrFail($id);
        $rep_company->update($request->all());

        return redirect()->back()->with([
            'flash_message' => '<b>' . $request['name'] . '</b> updated!'
        ]);
    }


    public function destroy($id)
    {
        $rep_company = RepCompany::findOrFail($id);
        $rep_company->addresses()->delete();
        $rep_company->delete();

        return response('success');
    }
}
