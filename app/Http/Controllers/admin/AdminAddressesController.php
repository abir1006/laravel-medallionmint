<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Address;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminAddressesController extends Controller
{



    public function addCompanyAddress(Request $request,$id){

        $request['addressable_type'] = 'App\Company';
        $request['addressable_id'] = $id;

        Address::create($request->all());

        return redirect()->back();

    }

    public function addRepCompanyAddress(Request $request,$id){

        $request['addressable_type'] = 'App\RepCompany';
        $request['addressable_id'] = $id;

        Address::create($request->all());

        return redirect()->back();

    }

    public function addManagingCompanyAddress(Request $request,$id){

        $request['addressable_type'] = 'App\ManagingCompany';
        $request['addressable_id'] = $id;

        Address::create($request->all());

        return redirect()->back();

    }


    public function addLocationAddress(Request $request,$id){

        $request['addressable_type'] = 'App\Location';
        $request['addressable_id'] = $id;

        Address::create($request->all());

        return redirect()->back();

    }

    public function show($id){
        return Address::findOrFail($id);
    }



    public function update(Request $request, $id){

        $address = Address::findOrFail($id);
        $address->fill($request->all());
        $address->save();



        return redirect()->back();
    }

    public function destroy($id){

        $address = Address::findOrFail($id);

        $address->delete();

        return response('success');
    }


}
