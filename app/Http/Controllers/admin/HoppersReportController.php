<?php

namespace App\Http\Controllers\admin;

use App\Machine;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HoppersReportController extends Controller
{

    public function index()
    {
        $machines = Machine::with([
            'location',
            'deviceHealth',
            'coin_1',
            'coin_2',
            'coin_3',
            'coin_4',
            'coin_5',
            'coin_6',
            'coin_7',
            'coin_8'
        ])->where('active',1)->get();

        $machines = $machines->filter(function($item){

            if($item->location->is_exclude == true){
                return false;
            }else{
                return true;
            }

        });

        return view('admin.hoppers_report',compact('machines'));
    }

}
