<?php namespace App\Http\Controllers\admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;

use App\Carrier;
use App\Company;
use App\Location;
use App\Term;
use App\TimeZone;

class AdminCompaniesController extends Controller {

	
	public function index()
	{	
				
		$companies = Company::with('locations','machines')->orderBy('name', 'asc')->get();

		return view('admin.companies.index',compact('companies'));
		
	}

	
	public function create()
	{
		$carriers = Carrier::where('active',true)->orderBy('name', 'asc')->lists('name', 'id')->all();
		$terms = Term::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$time_zones = TimeZone::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		return view('admin.companies.create',compact('carriers','terms','time_zones'));
	}

	
	public function store(CompanyRequest $request)
	{


        $request['active'] = isset($request['active']);

		Company::create($request->all());
		return redirect('admin/companies')->with([
            'flash_message' => '<b>' . $request['name'] . '</b> created!'
        ]);
	}

	
	public function show($id)
	{
		$company = Company::findOrFail($id);
        $time_zones = TimeZone::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();


        return view('admin.companies.show',compact('company','time_zones'));
	}

	
	public function edit($id)
	{
		$company = Company::findOrFail($id);

		$carriers = Carrier::where('active',true)->orderBy('name', 'asc')->lists('name', 'id')->all();
		$terms = Term::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();
		$time_zones = TimeZone::where('active','=','1')->orderBy('name', 'asc')->lists('name', 'id')->all();


		return view('admin.companies.edit',compact('carriers','company', 'terms','time_zones'));
	}

	
	public function update($id, CompanyRequest $request)
	{
		$company = Company::findOrFail($id);

        $request['active'] = isset($request['active']);

		$company->update($request->all());

		return redirect('admin/companies')->with([
            'flash_message' => '<b>' . $request['name'] . '</b> updated!'
        ]);
	}

	
	public function destroy($id)
	{
		
		$company = Company::findOrFail($id);
        $company->addresses()->delete();
		$company->delete();

		return response('success');
	}

}
