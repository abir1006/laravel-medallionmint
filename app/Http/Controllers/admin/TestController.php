<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function duplicate_device_serial(){
        $cc_modems = \App\Parts\CcModem::with('machine')->get();

        foreach($cc_modems as $cc_modem){

            if($cc_modem->machine->count() >1) {

                echo '<strong>modem ' . $cc_modem->id .' '.$cc_modem->serial.'</strong> <br> ';

                foreach ($cc_modem->machine as $machine) {
                    echo 'machine_id = ' . $machine->id . ' ' . ($machine->active ? 'Active' : 'Inactive') . '<br>';
                }


                echo '<hr>';
            }
        }
    }
}
