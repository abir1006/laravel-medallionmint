<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DeviceHealth;

class DeviceHealthController extends Controller
{

    public function index()
    {

        $all_device_health = DeviceHealth::with(['machine'=>function($query){
            $query->with('location');
        }])->orderBy('date_of_last_call_in','desc')->get();

        //return $all_device_health;

        $all_device_health = $all_device_health->filter(function($item){

            if(!$item->machine){
                return true;
            }

            if($item->machine->location->is_exclude == true){
                return false;
            }else{
                return true;
            }

        });

        //return $all_device_health;

        return view('admin.device_health',compact('all_device_health'));
    }


}
