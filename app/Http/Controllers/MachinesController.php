<?php namespace App\Http\Controllers;

use App\Machine;
use App\Location;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class MachinesController extends Controller {

	
	public function index($location_id)
    {
        
        $location = Location::findOrFail($location_id);
        $machines = $location->machines;
        
    	return view('machines.index',compact('machines','location'));      


    }	

}
