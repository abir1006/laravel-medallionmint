<?php namespace App\Http\Controllers;

use App\Location;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CoinsController extends Controller {

	
	public function index($location_id)
    {
        
        $location = Location::findOrFail($location_id);
        $coins = $location->coins;
        
        return view('coins.index',compact('coins','location')); 
    }


}
