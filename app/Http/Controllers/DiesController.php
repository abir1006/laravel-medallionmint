<?php namespace App\Http\Controllers;

use App\Location;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class DiesController extends Controller {

	
	public function index($location_id)
    {
        
        $location = Location::findOrFail($location_id);
        $dies = $location->dies;
        
        return view('dies.index',compact('dies','location'));      


    }

	

}
