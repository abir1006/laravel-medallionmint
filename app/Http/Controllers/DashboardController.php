<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Location;

class DashboardController extends Controller
{
   
    public function __construct(){

        \App\Online::UpdateCurrent();
    }

    public function index()
    {
        $user = Auth::user();


        if($user->role == 'location_manager'){
            if($user->locations->count()){                         
                          
                return redirect('location/'.$user->locations->first()->id.'/dashboard');                

            }else{
                return view('no_permission',compact('user'));
            }
            

        }elseif($user->role == 'company_manager'){            
            
            
            if($user->companies->first()->locations->count() ){
                return redirect('location/'.$user->companies->first()->locations->first()->id.'/dashboard');
            }else{
                return view('no_permission',compact('user'));
            }
            
        }
    }

    public function specific_location($location_id){
        $location = Location::findOrFail($location_id);
        return view('dashboard',compact('location'));

    }

   
}
