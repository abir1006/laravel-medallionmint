<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Requests\Manager\ManagerUpdateRequest;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Location;
use App\User;
use \Hash;

class ManagersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($location_id)    
    {
        $location = Location::findOrFail($location_id);
        $managers = $location->managers;
        return view('managers.index',compact('location','managers'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($location_id)
    {
        $location = Location::findOrFail($location_id);        
        return view('managers.create',compact('location')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store($location_id,UserRequest $request)
    {
        $request->merge(['password' => Hash::make($request->password)]);
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password'=> $request['password'],
            'role'=>'location_manager'
        ]);

        $user->locations()->attach($location_id);

        return redirect()->route('location.managers.index',$location_id)->with([
                'flash_message'=>'New Location Manager Created'
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($location_id,$manager_id)
    {
        //return route('location.managers.update',[$location_id,$manager_id]);
        $location = Location::findOrFail($location_id);
        $manager = $location->managers->where('id',intval($manager_id))->first();
        if(!$manager){
            return view('access_denied');
        }else{                        
            return view('managers.edit',compact('location','manager'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($location_id,$manager_id,ManagerUpdateRequest $request)
    {     

        $user = User::findOrFail($manager_id);      

        // notice: role is also fillable, so careful about mass assignment

        $user->name = $request['name'];
        $user->email = $request['email'];

        if($request['password'] !=''){
            $user->password = Hash::make($request['password']);
        }

        $user->save();

        return redirect()->route('location.managers.index',[$location_id])->with([
                'flash_message' => $user->name.' user info updated'
            ]);
    }

    public function remove($location_id,$manager_id){
        User::findOrFail($manager_id)->locations()->detach($location_id);       
        return response('success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
