<?php

namespace App\Http\Middleware;

use Closure;

class check_location_access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {  
        if(\Auth::user()->hasLocationAccess($request->route()->location)){
            return $next($request);
        }

        if($request->ajax()){
            return response(['msg'=>'You do not have permission to access this location']);
        }

        return view('access_denied',compact('user'));
    }
}
