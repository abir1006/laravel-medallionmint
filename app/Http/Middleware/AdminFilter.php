<?php namespace App\Http\Middleware;

use Closure;

class AdminFilter {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		

		if (\Auth::check())
		{
			if (\Auth::user()->role != 'admin'){

				return redirect('/dashboard');
				
			}
		}else{

			return redirect('/auth/login');
		}

		return $next($request);
	}

}
