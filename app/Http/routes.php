<?php
use App\User;
use App\Dies;

Route::get('/', function(){
    
	return redirect('dashboard');
});

Route::get('home', function()
{
    
    return redirect('dashboard');
});

Route::group(['middleware' => 'auth'], function()
{
	
    
    Route::get('dashboard','DashboardController@index');

    Route::resource('users','UsersController');

    Route::group(['middleware' => 'check_location_access'],function(){

        Route::get('location/{location}/dashboard','DashboardController@specific_location');

        Route::get('location/{location}/reports/create/{type}','ReportsController@create');
        Route::get('location/{location}/reports/{report_id}/{action}','ReportsController@show');    
        Route::resource('location.reports','ReportsController');
        
        Route::resource('location.machines','MachinesController');
        Route::resource('location.dies','DiesController');
        Route::resource('location.coins','CoinsController');

        Route::post('location/{location}/managers/{managers}/remove','ManagersController@remove');
        Route::resource('location.managers','ManagersController');

    });

    


});


Route::group(['prefix' => 'admin', 'middleware'=>'admin'], function()
{
    

    Route::get('dashboard','admin\AdminDashboardController@index');  


    Route::post('users/attach','admin\AdminUsersController@attach');
    Route::post('users/{user_id}/remove/{location_id}','admin\AdminUsersController@remove');

    Route::resource('users','admin\AdminUsersController');
    
    Route::resource('companies','admin\AdminCompaniesController');
    Route::resource('rep_companies','admin\AdminRepCompaniesController');
    Route::resource('managing_companies', 'admin\AdminManagingCompaniesController');

    Route::resource('locations','admin\AdminLocationsController');

    Route::get('locations/{location_id}/reports/create/{type}','admin\AdminReportsController@create');
    Route::get('locations/{location_id}/reports/{report_id}/edit','admin\AdminReportsController@edit');    
    Route::get('locations/{location_id}/reports/{report_id}/{action}','admin\AdminReportsController@show');    
    Route::resource('locations.reports','admin\AdminReportsController');    

    Route::resource('machines','admin\AdminMachinesController');
    Route::resource('machine_settings','admin\AdminMachineSettingsController');

    Route::resource('dies','admin\AdminDiesController');
    Route::resource('coins','admin\AdminCoinsController');

    Route::resource('address','admin\AdminAddressesController');
    Route::post('companies/{company_id}/address','admin\AdminAddressesController@addCompanyAddress');
    Route::post('rep_companies/{company_id}/address','admin\AdminAddressesController@addRepCompanyAddress');
    Route::post('managing_companies/{company_id}/address','admin\AdminAddressesController@addManagingCompanyAddress');
    Route::post('locations/{location_id}/address','admin\AdminAddressesController@addLocationAddress');

    Route::get('help',function(){  return view('admin.help'); });
    Route::get('profile', 'admin\AdminUsersController@profile');

    //maybe we could use another AdminLocationPhotoController, 
    //route like locations.photo same as locations.report ...  not sure - fida
    Route::post('locations/{location_id}/addPhoto','admin\AdminLocationsController@addPhoto');
    Route::delete('locations/{location_id}/delPhoto/{photo_id}','admin\AdminLocationsController@delPhoto');
    Route::post('locations/{location_id}/setDefaultPhoto/{photo_id}','admin\AdminLocationsController@setDefaultPhoto');
    
    Route::post('machines/{machine_id}/addPhoto','admin\AdminMachinesController@addPhoto');
    Route::delete('machines/{machine_id}/delPhoto/{photo_id}','admin\AdminMachinesController@delPhoto');
    Route::post('machines/{machine_id}/setDefaultPhoto/{photo_id}','admin\AdminMachinesController@setDefaultPhoto');

    Route::resource('gizmos','admin\parts\gizmos_controller');
    Route::resource('meter_boxes','admin\parts\meter_boxes_controller');
    Route::resource('cc_modems','admin\parts\cc_modems_controller');
    Route::resource('cc_readers','admin\parts\cc_readers_controller');
    Route::resource('bill_validators','admin\parts\bill_validators_controller');
    Route::resource('vaults','admin\parts\vaults_controller');
    Route::resource('hoppers','admin\parts\hoppers_controller');
    Route::resource('touch_screens','admin\parts\touch_screens_controller');

    Route::resource('locations.contracts','admin\AdminContractsController');

    Route::get('get_dies/{location_id}', function($location_id){
        return \DB::table('dies')->where('location_id', $location_id)->get();

    });

    Route::get('get_coins/{location_id}', function($location_id){
        return \DB::table('coins')->where('location_id', $location_id)->get();

    });


	Route::resource('carriers', 'admin\AdminCarriersController');
	Route::resource('programs', 'admin\AdminProgramsController');

    Route::get('device_health','admin\DeviceHealthController@index');
    Route::get('transactions','admin\TransactionsController@index');
    Route::get('device_alerts','admin\DeviceAlertsController@index');
    Route::get('hoppers_report','admin\HoppersReportController@index');


    Route::get('duplicate_device_serial','admin\TestController@duplicate_device_serial');

});



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);




Route::get('test',function(){

    return $machine = \App\Machine::find(1);


});