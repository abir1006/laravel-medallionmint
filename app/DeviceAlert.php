<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceAlert extends Model
{
    protected $table = 'device_alerts';

    protected $dates = [

        'created_at',
        'updated_at',
        'detected_at',
        'reported_at'
    ];

    public function machine(){
        return $this->belongsTo('\App\Machine');
    }

}
