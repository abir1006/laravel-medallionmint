<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeviceHealth extends Model
{
    protected $table = 'device_health';

    protected $dates = ['created_at', 'updated_at',
        'date_of_last_call_in',
        'date_of_last_credit_tran',
        'date_of_last_cash_tran',
        'date_of_last_fill'

    ];

    public function machine(){
        return $this->belongsTo('\App\Machine');
    }
}
