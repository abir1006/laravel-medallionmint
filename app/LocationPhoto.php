<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationPhoto extends Model
{
    protected $fillable = ['name'];
}
