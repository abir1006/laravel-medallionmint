<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Location extends Model {


    protected $fillable = [
        'name',
        'company_id',
        'rep_company_id',
        'managing_company_id',
        'corporate_shipper_no',
        'has_custom_cc_percent',
        'custom_cc_percent',
        'has_seasons',
        'season_start_at',
        'season_end_at',
        'sales_tax_rate',
        'has_tax_stamp',
        'est_guests',
        'website',
        'carrier_id',
        'location_type_id',
        'mm_rep_id',
        'program_id',
        'sales_type_id',
        'split_id',
        'term_id',
        'time_zone_id',
        'notes',
        'active',
        'is_exclude',
        'default_photo_id'
    ];


    public function managers(){
        return $this->belongsToMany('App\User','location_managers');
    }

	public function company(){

		return $this->belongsTo('App\Company');
	}

	public function rep_company(){

		return $this->belongsTo('App\RepCompany');
	}

	public function managing_company(){

		return $this->belongsTo('App\ManagingCompany');
	}

    public function photos(){

        return $this->hasMany('App\LocationPhoto');
    }

    public function photo(){
        if($this->photos->count()){
            if($this->default_photo_id){
                return $this->photos->find($this->default_photo_id);
            }else{
                return $this->photos->first();
            }
        }

        return;
    }

    public function machines(){
        return $this->hasMany('App\Machine');
    }


    public function report(){
        return $this->hasMany('App\Report');
    }

    public function coins(){
        return $this->hasMany('App\Coin');
    }

    public function dies(){
        return $this->hasMany('App\Dies');
    }
    public function front_dies(){

        return Dies::where('id','=',$this->front_dies)->first();
    }

    public function back_dies(){

        return Dies::where('id','=',$this->back_dies)->first();
    }

    public function addresses(){
        return $this->morphMany('App\Address', 'addressable');
    }

    public function contracts(){
        return $this->hasMany('App\Contract');
    }

    /*********************************************/

    public function setRepCompanyIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['rep_company_id'] = $value ;
    }

    public function setManagingCompanyIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['managing_company_id'] = $value ;
    }

    public function setCompanyIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['company_id'] = $value ;
    }
    /*********************************************/

    public function setCarrierIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['carrier_id'] = $value ;
    }

    public function setLocationTypeIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['location_type_id'] = $value ;
    }

    public function setMmRepIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['mm_rep_id'] = $value ;
    }

    public function setProgramIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['program_id'] = $value ;
    }

    public function setSalesTypeIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['sales_type_id'] = $value ;
    }

    public function setSplitIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['split_id'] = $value ;
    }

    public function setTermIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['term_id'] = $value ;
    }

    public function setTimeZoneIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['time_zone_id'] = $value ;
    }

}

