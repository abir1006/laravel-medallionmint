<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $fillable = [
        'location_id',
        'terms',
        'start_date',
        'end_date',
        'auto_renew'
    ];
}
