<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrier extends Model
{
	protected $fillable = ['active', 'name', 'slug', 'type', 'track_url','rate_per_box'];


	protected $casts = [
		'active'    => 'boolean'
	];

}
