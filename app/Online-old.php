<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
class Online extends Model
{
    protected $table = 'sessions';
    public $timestamps = false;

    protected $fillable = ['user_id'];

    public function scopeGuests($query)
    {
        return $query->whereNull('user_id');
    }

    public function scopeRegistered($query)
    {
        return $query->whereNotNull('user_id')->with('user');
    }

    
 /*   public function scopeUpdateCurrent($query)
    {   	
		
        return $query->where('id', Session::getId())->update(array(
            //'user_id' => (!empty(\Auth::user()) ? \Auth::user()->id : 456 )
            'user_id' => 456
        ));
    }*/

    
   

    public function user()
    {
        return $this->belongsTo('User');
    }
}
