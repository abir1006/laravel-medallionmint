<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachinePhoto extends Model
{
    protected $fillable = ['name'];
}
