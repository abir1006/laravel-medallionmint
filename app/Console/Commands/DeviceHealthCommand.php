<?php

namespace App\Console\Commands;

use App\Machine;
use App\Parts\CcModem;
use Illuminate\Console\Command;

class DeviceHealthCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deviceHealth:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update device health data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('device_health updating...');

        $url = 'http://my.medallionmint.com/export_update/device_health_report_get.php';
        $data = json_decode(file_get_contents($url));

        $total = $new = $not_found = 0;

        foreach($data as $item){

            $total++;

            $device = $item[4];

            $machine = null;
            $cc_modem = null;

            $device_health = \App\DeviceHealth::where('device',$device)->first();


            if(!$device_health){

                $device_health = new \App\DeviceHealth();

                $new++;

                $device_health->days_since_last_call_in = (int)$item[5];
                $device_health->date_of_last_call_in = date('Y-m-d H:i:s',strtotime($item[6]));

                $device_health->days_since_last_credit_tran = (int)$item[7];
                $device_health->date_of_last_credit_tran = date('Y-m-d H:i:s',strtotime($item[8]));

                $device_health->days_since_last_cash_tran = (int)$item[9];
                $device_health->date_of_last_cash_tran = date('Y-m-d H:i:s',strtotime($item[10]));

            }else{

                if( strtotime($device_health->date_of_last_call_in) <  strtotime($item[6]) ){
                    $device_health->days_since_last_call_in = (int)$item[5];
                    $device_health->date_of_last_call_in = date('Y-m-d H:i:s',strtotime($item[6]));
                }

                if(strtotime($device_health->date_of_last_credit_tran) < strtotime($item[8]) ){
                    $device_health->days_since_last_credit_tran = (int)$item[7];
                    $device_health->date_of_last_credit_tran = date('Y-m-d H:i:s',strtotime($item[8]));
                }

                if(strtotime($device_health->date_of_last_cash_tran) < strtotime($item[10]) ){
                    $device_health->days_since_last_cash_tran = (int)$item[9];
                    $device_health->date_of_last_cash_tran = date('Y-m-d H:i:s',strtotime($item[10]));
                }

            }


            /*here.....*/

            //$machine = \App\Machine::where('cc_modem_serial',$device)->first();

            $cc_modem = CcModem::with('machine')->where('serial',$device)->first();

            if($cc_modem){
                if($cc_modem->machine->count()){
                    $machine = $cc_modem->machine->where('active',1)->first();
                }
            }

            if($machine){

                $device_health->machine_id = $machine->id;
            }else{
                $not_found++;
                $device_health->machine_id = null;
            }


            $device_health->customer = $item[0];
            $device_health->region = $item[1];
            $device_health->asset = $item[2];
            $device_health->location = $item[3];
            $device_health->device = $item[4];


            $device_health->days_since_last_fill = (int)$item[11];
            $device_health->date_of_last_fill = date('Y-m-d H:i:s',strtotime($item[12]));

            $device_health->days_since_last_dex = (int)$item[13];
            $device_health->date_of_last_dex = date('Y-m-d H:i:s',strtotime($item[14]));

            $device_health->status = $item[15];
            $device_health->signal_strength = $item[16];
            $device_health->business_type = $item[17];

            $device_health->last_dex_alert = $item[18];
            $device_health->firmware_version = $item[19];
            $device_health->communication_method = $item[20];

            $device_health->save();



        }

        $this->info('complete: '.$total.' total ('.$new.' new) '.$not_found.' device serial not found!');
    }
}
