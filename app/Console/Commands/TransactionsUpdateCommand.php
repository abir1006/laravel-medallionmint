<?php

namespace App\Console\Commands;

use App\DeviceAlert;
use App\DeviceHealth;
use App\Parts\CcModem;
use App\Transaction;
use Illuminate\Console\Command;

class TransactionsUpdateCommand extends Command
{

    protected $signature = 'transactions:update';


    protected $description = 'Read individual transactions csv files and store at database';


    public function __construct()
    {
        parent::__construct();
    }


    public function handle()
    {
        $error_code = array(
            'E004' => 'No SD card detected',
            'E005' => 'No Config file found on SD card',
            'E006' => 'Meter Box not detected',
            'E008' => 'Hopper 1 time-out',
            'E009' => 'Hopper 2 time-out',
            'E00A' => 'Hopper 3 time-out',
            'E00B' => 'Hopper 4 time-out',
            'E00C' => 'Hopper 5 time-out',
            'E00D' => 'Hopper 6 time-out',
            'E00E' => 'Hopper 7 time-out',
            'E00F' => 'Hopper 8 time-out',

        );

        $activity_code = array(

            'F000' => '00.90.00',
            'F001' => '00.91.00',
            'F002' => '00.91.01',
            'F003' => '00.92.00',
            'F004' => '01.00.00',
            'F005' => '01.00.01',
            'F006' => '01.01.00',
            'F007' => '01.01.01',
            'F008' => '01.01.02',
            'F009' => '01.01.03',
            'F00A' => '01.01.04',
            'F00B' => '01.01.05',
            'F00C' => '01.01.06-Epr',
            'F00D' => '01.01.06-Fpr',
            'F00E' => '01.01.06-F',
            'F00F' => '01.02.00-Apr',
            'F010' => '01.02.00-Bpr',
            'F011' => '01.02.00-Cpr',
            'F012' => '01.02.00-Dpr',
            'F013' => '01.02.00-Epr',
            'F014' => '01.02.00-Epr',
            'F015' => '01.02.00-Gpr',
            'F016' => '01.02.11-Hpr',
            'F017' => '01.02.00-Ipr',
            'F018' => '02.00.00-Jpr',
            'F019' => '01.00.04-Kpr',
            'F01A' => '02.00.06-Lpr',
            'F01B' => '02.01.06-Mpr',
            'F01C' => '02.02.09-Npr',
            'F01D' => '02.02-10-Opr',
            'F01E' => '2.2.10-15',
            'F01F' => '2.2.11-1',
            'F020' => '2.2.11-2',
            'F021' => '2.1.11-3',
            'F022' => '2.2.12-1',
            'F023' => '2.3.0-pr1',
            'F024' => '2.3.0-2',
            'F025' => '2.3.1-1',
            'F026' => '2.3.2-1',
            'F027' => '2.3.3-1',
            'F028' => '2.3.4-1',
            'F029' => '2.3.5-1',
            'F02A' => '2.4.0-1',
            'F02B' => '2.4.0-2-exp',
        );


        $this->line('Reading new transactions csv files... ');

        $url = 'http://my.medallionmint.com/export_update/transactions_get.php';
        $transactions = json_decode(file_get_contents($url));


        if(is_array($transactions) && count($transactions) ) {

            $this->info(count($transactions) . ' new transactions found...');

            $bar = $this->output->createProgressBar(count($transactions));

            $not_found_serials = [];

            foreach($transactions as $transaction){

                $device_serial = $transaction->device_serial;

                $machine = null;
                $machine_id = null;
                $cc_modem = null;

                $cc_modem = CcModem::where('serial',$device_serial)->first();

                if($cc_modem){

                    if($cc_modem->machine->count()){
                        $machine = $cc_modem->machine->where('active',1)->first();
                        if($machine){
                            $machine_id = $machine->id;
                        }else{

                            if(!in_array($transaction->device_serial,$not_found_serials)){

                                array_push($not_found_serials,$transaction->device_serial);
                            }
                            //$this->warn('Active Machine with serial '.$transaction->device_serial.' (CcModem ID '.$cc_modem->id.') not found');
                        }
                    }
                }else{

                    if(!in_array($transaction->device_serial,$not_found_serials)){

                        array_push($not_found_serials,$transaction->device_serial);
                    }
                    //$this->warn('CcModem with serial '.$transaction->device_serial.' not found');
                }


                $item_number = $transaction->item_number;

                if(!is_numeric($item_number) && ($item_number != 'FFFF') ){

                    if($item_number[0] == 'E' || $item_number[0] == 'F'){

                        $device_alert = new DeviceAlert();

                        $device_alert->device = $device_serial;
                        $device_alert->machine_id = $machine_id;

                        if($item_number[0] == 'E'){

                            if(array_key_exists($item_number,$error_code)){
                                $alert = $error_code[$item_number];
                            }else{
                                $alert = 'unknown error';
                            }

                            $device_alert->alert = $item_number.' - '.$alert;
                            $device_alert->alert_type = 'error';

                        }elseif($item_number[0] == 'F'){

                            if(array_key_exists($item_number,$activity_code)){
                                $firmware_version = $activity_code[$item_number];
                            }else{
                                $firmware_version = 'unknown version';
                            }

                            $device_alert->alert = $item_number.' - Machine Boot: Firmware Version '.$firmware_version;
                            $device_alert->alert_type = 'normal';
                        }

                        $device_alert->detected_at = $transaction->transaction_time;
                        $device_alert->reported_at = $transaction->transaction_time;

                        $device_alert->save();

                    }

                }else{

                    $new_transaction = new Transaction();
                    $new_transaction->machine_id = $machine_id;
                    $new_transaction->coin_id = null;

                    $new_transaction->device_serial = $device_serial;
                    $new_transaction->transaction_ref = $transaction->transaction_ref;
                    $new_transaction->transaction_type = $transaction->transaction_type;
                    $new_transaction->card_num = $transaction->card_num;
                    $new_transaction->total_amount = $transaction->total_amount;
                    $new_transaction->item_number = $transaction->item_number;
                    $new_transaction->items = $transaction->items;
                    $new_transaction->transaction_time = $transaction->transaction_time;


                    $device_health = DeviceHealth::where('device',$device_serial)->first();

                    if($device_health){
                        if($transaction->transaction_type == 'C'){

                            $device_health->date_of_last_cash_tran = $transaction->transaction_time;
                            $device_health->days_since_last_cash_tran = 0;


                        }elseif ($transaction->transaction_type == 'R' || $transaction->transaction_type == 'P'){

                            $device_health->date_of_last_credit_tran = $transaction->transaction_time;
                            $device_health->days_since_last_credit_tran = 0;

                        }

                        $device_health->date_of_last_call_in = $transaction->transaction_time;
                        $device_health->days_since_last_call_in = 0;

                        //hopper_X_last_transaction_date is updating at machines table,
                        //but as we will run too many migrate:refresh ... keeping this info at deviceHealth table temporarily
                        if (is_numeric($transaction->item_number) && ($transaction->item_number >= 1 ) && ($transaction->item_number <= 8 ) ) {

                            $item_number = (int) $item_number;

                            $device_health->{'hopper_'.$item_number.'_last_transaction_date'} = $transaction->transaction_time;
                        }


                        $device_health->save();
                    }




                    if($machine){

                        //update calculated_meter

                        if($transaction->transaction_type == 'C'){

                            $machine->calculated_meter_1 += $transaction->total_amount;

                        }elseif ($transaction->transaction_type == 'R' || $transaction->transaction_type == 'P'){

                            $machine->calculated_meter_2 += $transaction->total_amount;
                        }

                        // Be Careful: non-numeric FFFF items are still here
                        if (is_numeric($transaction->item_number) && ($transaction->item_number > 0 ) ) {

                            $item_number = (int) $item_number;

                            $machine->{'calculated_inventory_'.$item_number} -= $transaction->items;
                            $machine->{'hopper_'.$item_number.'_last_transaction_date'} = $transaction->transaction_time;


                            // set $new_transaction coin_id
                            if($machine->{'coin_'.$item_number.'_id'}){
                                $new_transaction->coin_id = $machine->{'coin_'.$item_number.'_id'};
                            }

                        }

                        $machine->save();


                    }

                    $new_transaction->save();

                }

                $bar->advance();

            }

            $bar->finish();
            $this->info(' Finished ');


            if(count($not_found_serials)){

                foreach($not_found_serials as $serial){
                    $this->info($serial);
                }

                $this->warn(count($not_found_serials).' device_serial not found');
            }

        }else{
            $this->info('Nothing to update now');
        }


    }
}
