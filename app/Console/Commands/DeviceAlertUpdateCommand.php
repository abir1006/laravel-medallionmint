<?php

namespace App\Console\Commands;


use App\Parts\CcModem;
use Illuminate\Console\Command;

class DeviceAlertUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deviceAlert:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update device alert';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Device Alerts updating...');

        $url = 'http://my.medallionmint.com/export_update/device_alert.php';
        $alerts = json_decode(file_get_contents($url));

        $total = $not_found = 0;

        foreach($alerts as $alert){

            $total++;

            $device_alert = new \App\DeviceAlert();

            $cc_modem = null;
            $machine = null;

            $cc_modem = CcModem::where('serial',$alert->device)->first();

            if($cc_modem){

                $machine = $cc_modem->machine->where('active',1)->first();

                if($machine){
                    $device_alert->machine_id = $machine->id;
                }

            }else{
                $not_found++;
                $this->warn($alert->device.' not found!');
            }

            if(!$machine){
                $device_alert->machine_id = null;
            }


            $device_alert->device = $alert->device;
            $device_alert->alert = $alert->alert;
            $device_alert->alert_type = $alert->alert_type;
            $device_alert->detected_at = $alert->detected;
            $device_alert->reported_at = $alert->reported;

            $device_alert->save();



        }

        $this->info('complete: '.$total.' total '.$not_found.' device serial not found!');

    }
}
