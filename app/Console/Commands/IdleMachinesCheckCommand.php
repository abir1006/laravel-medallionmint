<?php

namespace App\Console\Commands;

use App\DeviceHealth;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class IdleMachinesCheckCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'IdleMachineCheck:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'make a list of machines , those last call in not within 30 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $all_device_health = DeviceHealth::where('date_of_last_call_in','<',date('Y-m-d',strtotime('-30 hours')))
            ->with(['machine'=>function($query){
            $query->with('location');
        }])->orderBy('date_of_last_call_in','desc')->get();

        //return $all_device_health;

        $all_device_health = $all_device_health->filter(function($item){

            if(!$item->machine){
                return false;
            }

            if($item->machine->location->is_exclude == true){
                return false;
            }else{
                return true;
            }

        });


        Mail::send('emails.idle_machines', ['all_device_health' => $all_device_health], function ($message){

            $message->subject('Idle machines list');

            $message->from('info@medallionmint.com', 'Medallionmint LLC');

            $message->to('tim@medallionmint.com', 'Tim McClure');
            $message->cc('timbtpt@gmail.com', 'Tim McClure');
            $message->cc('fidahasan08@gmail.com', 'Fida Hasan');

        });
    }
}
