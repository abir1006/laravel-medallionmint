<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
        'App\Console\Commands\DeviceHealthCommand',
        'App\Console\Commands\TransactionsUpdateCommand',
        'App\Console\Commands\DeviceAlertUpdateCommand',
        'App\Console\Commands\IdleMachinesCheckCommand'
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')->hourly();

        $schedule->command('deviceHealth:update')->everyTenMinutes();
        $schedule->command('deviceAlert:update')->hourly();
        $schedule->command('transactions:update')->everyFiveMinutes();
        $schedule->command('IdleMachineCheck:email')->hourly();

	}

}
