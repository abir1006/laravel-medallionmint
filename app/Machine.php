<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Coin;
use App\MachineSettings;

class Machine extends Model {

    //DataTables_Table_0_length .... transactions tab problem
    protected $guarded = ['DataTables_Table_0_length'];

    protected $casts = [
        //'active'    => 'boolean',
    ];

   /* protected $dates = [
        ''
    ];*/

    public static function boot() {

        parent::boot();

       static::saving(function($model){

           $nullable_fields = array(
               'machine_types',
               'machine_colors',
               'machine_keys',
               'prices',
               'motors',
               'button_lights',

               'gizmo_id',
               'meter_box_id',
               'cc_modem_id',
               'cc_reader_id',
               'bill_validator_id',
               'vault_id',
               'touch_screen_id',

               'hopper_1_id',
               'hopper_2_id',
               'hopper_3_id',
               'hopper_4_id',
               'hopper_5_id',
               'hopper_6_id',
               'hopper_7_id',
               'hopper_8_id',

               'coin_1_id',
               'coin_2_id',
               'coin_3_id',
               'coin_4_id',
               'coin_5_id',
               'coin_6_id',
               'coin_7_id',
               'coin_8_id'

           );

           foreach ($model->attributes as $key => $value) {

               if(in_array($key,$nullable_fields)){

                   $model->{$key} = empty($value) ? null : $value;
               }
           }

       });

    }

	public function location(){

		return $this->belongsTo('App\Location');
	}

	public function coin_1(){
        return $this->belongsTo('App\Coin');
    }

    public function coin_2(){
        return $this->belongsTo('App\Coin');
    }

    public function coin_3(){
        return $this->belongsTo('App\Coin');
    }

    public function coin_4(){
        return $this->belongsTo('App\Coin');
    }

    public function coin_5(){
        return $this->belongsTo('App\Coin');
    }

    public function coin_6(){
        return $this->belongsTo('App\Coin');
    }

    public function coin_7(){
        return $this->belongsTo('App\Coin');
    }

    public function coin_8(){
        return $this->belongsTo('App\Coin');
    }

	public function cc_modem(){
        return $this->belongsTo('App\Parts\CcModem');
    }

	public function deviceHealth(){
        return $this->hasOne('App\DeviceHealth');
    }

    public function deviceAlerts(){
        return $this->hasMany('App\DeviceAlert');
    }

    public function transactions(){
        return $this->hasMany('App\Transaction');
    }

	public function price(){
		$data = json_decode(MachineSettings::where('id',$this->prices)->first()->data);		
		return $data->prices;
	}

	public function price_min(){
		$data = json_decode(MachineSettings::where('id',$this->prices)->first()->data);
		return round($data->level_2_credit/$data->level_2_coin,2);		
	}

	public function price_max(){		
		
		$data = json_decode(MachineSettings::where('id',$this->prices)->first()->data);		
		return round($data->level_1_credit/$data->level_1_coin,2);
	}



	public function photos(){

        return $this->hasMany('App\MachinePhoto');
    }

    public function photo(){
        if($this->photos->count()){
            if($this->default_photo_id){
                return $this->photos->find($this->default_photo_id);
            }else{
                return $this->photos->first();
            }
        }

        return;
    }


}
