<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'transaction_time'
    ];

    public function machine(){
        return $this->belongsTo('App\Machine');
    }

    public function coin(){
        return $this->belongsTo('App\Coin');
    }
}
