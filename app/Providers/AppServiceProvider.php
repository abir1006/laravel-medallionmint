<?php namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//\App\Online::UpdateCurrent();
		/*$session_id = \Session::getId();
		\App\Online::where('id',$session_id)->firstOrFail()->UpdateCurrent();*/
		
		
		
		view()->composer('layout',function($view){
			
			$user = \Auth::user();
			if($user->role == 'location_manager'){                 
            
		        $accessable_locations = $user->locations->lists('name','id');            

		    }elseif($user->role == 'company_manager'){              
		        
		        $accessable_locations = $user->companies->first()->locations->lists('name','id');
		        
		    }
        
			$view->with('accessable_locations',$accessable_locations);
		});

	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
