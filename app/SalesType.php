<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesType extends Model
{
	protected $fillable = ['name', 'active'];

	public function location()
	{

		return $this->hasMany('App\Location');
	}
}
