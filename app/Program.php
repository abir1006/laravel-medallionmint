<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
	protected $fillable = ['active', 'name', 'value'];


	protected $casts = [
		'active'    => 'boolean'
	];
}
