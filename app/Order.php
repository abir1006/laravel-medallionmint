<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $hidden = ['id', 'order_num', 'location_id', 'machine_id'];

    protected $guareded = ['id', 'order_num', 'location_id', 'machine_id'];

    public function OrderDetails() {
        $this->hasMany('App\OrdersDetail');
    }

    public function getDates() {
        return ['ordered_at','due_at','shipped_at','created_at', 'updated_at'];
    }

}
