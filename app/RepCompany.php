<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepCompany extends Model
{

    protected $fillable = ['name','time_zone_id','carrier_id','shipper_no','term_id','website','notes','active'];

    public function locations(){

        return $this->hasMany('App\Location');
    }

    public function machines(){

        return $this->hasManyThrough('App\Machine','App\Location');
    }

    public function term(){
        return $this->belongsTo('App\Term');
    }

    public function carrier(){
        return $this->belongsTo('App\Carrier');
    }

    public function time_zone(){
        return $this->belongsTo('App\TimeZone');
    }

    public function setTermIdAttribute($value){

        if($value==''){
            $value=null;
        }

        $this->attributes['term_id'] = $value;
    }

    public function setCarrierIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['carrier_id'] = $value ;
    }

    public function setTimeZoneIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['time_zone_id'] = $value ;
    }

    public function addresses(){
        return $this->morphMany('App\Address', 'addressable');
    }

}
