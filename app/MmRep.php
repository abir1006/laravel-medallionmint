<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MmRep extends Model
{
	protected $fillable = ['name', 'active'];

	public function location()
	{

		return $this->hasMany('App\Location');
	}
}
