<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    protected $fillable=[
        'type',
        'contact',
        'address_line_1',
        'address_line_2',
        'city',
        'state',
        'zip',
        'country',
        'phone',
        'fax',
        'time_zone_id',
        'addressable_type',
        'addressable_id'
    ];

    public function addressable()
    {
        return $this->morphTo();
    }

    public function time_zone(){
        return $this->belongsTo('App\TimeZone');
    }

    public function setTimeZoneIdAttribute($value){
        if($value == '' ){
            $value = null;
        }
        $this->attributes['time_zone_id'] = $value ;
    }
}
