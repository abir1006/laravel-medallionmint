$(window).load(function(){

    $('.table_sortable').dataTable({
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "iDisplayLength": 50
    });

    $('.nav-side-menu ul.sub-menu li').each(function(index,item){
        if($(item).hasClass('active')){
            $(item).parent().addClass('in');
        }
    });

    $('.nav-side-menu li.collapsed a').click(function(e){
        e.preventDefault();
    });


	alertify.defaults = {

        modal:true,
        movable:true,
        resizable:true,
        closable:true,
        maximizable:true,
        pinnable:true,
        pinned:true,
        padding: true,
        overflow:true,
        maintainFocus:true,
        transition:'zoom',

        // notifier defaults
        notifier:{
            // auto-dismiss wait time (in seconds)  
            delay:5,
            // default position
            position:'top-center'
        },

        // language resources 
        glossary:{
            // dialogs default title
            title:'',
            // ok button text
            ok: 'OK',
            // cancel button text
            cancel: 'Cancel'
        },

        // theme machine_settings
        theme:{
            // class name attached to prompt dialog input textbox.
            input:'ajs-input',
            // class name attached to ok button
            ok:'ajs-ok',
            // class name attached to cancel button 
            cancel:'ajs-cancel'
        }
    };

    function preload(arrayOfImages) {
        $(arrayOfImages).each(function(){
            $('<img/>')[0].src = this;
        });
    }


    preload([
        '/images/loading-bar.gif',
        '/images/coin_brass.png',
        '/images/coin_copper.png',
        '/images/coin_nickel_silver.png'

    ]);

    $('.searchable_select').chosen();

    /**
     * Only show update/save button when changes have been made
     */
    var disabled;
    $('form.edit').each(function(){

        $(this).data('serialized', $(this).serialize());

    }).on('change input', function(){

        if($(this).serialize() != $(this).data('serialized') || $(this).find('input[type="file"]').val() ){
            $(this).find('input:submit, button:submit').prop('disabled', false);
        }else{
            $(this).find('input:submit, button:submit').prop('disabled', true);
        }

    }).find('input:submit, button:submit').prop('disabled', true);


    /**
     * date picker for all inputs with datepicker class
     */
    $('.datepicker').datepicker({
        autoclose               :   true,
        format                  :   'mm/dd/yyyy',
        //endDate                 :   'day:1',
        todayBtn                :   true,
        todayHighlight          :   true,
        //disableTouchKeyboard    :   true
    });


    //http://weareoutman.github.io/clockpicker/
    $('.clockpicker').clockpicker({        
        autoclose: true,
        placement: 'top',                
    });


    $('.popover_link').css({'display':'inline-block'}).popover({
        'trigger':'hover'
    });

    // Javascript to enable link to tab
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
    } 

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        var scrollmem = $('body').scrollTop() || $('html').scrollTop();
        window.location.hash = e.target.hash;
        $('html,body').scrollTop(scrollmem);
    });


    $('.common_del_btn').click(function(e){
        e.preventDefault();

        var target = $(this);
        var url = $(this).data('url');
        var token = $(this).data('token');

        alertify.confirm('Are you sure you want to delete this ? This action cannot be undone.').setting({
            'title':'<span class="glyphicon glyphicon-trash"></span> Delete',
            'labels':{ok:'DELETE'},
            'onok': function(){

                $.ajax({

                    url: url,
                    type: 'POST',
                    data: {_method: 'delete', _token: token},

                    beforeSend:function(){

                        $(target).addClass('disabled');
                        $(target).parent().parent().css('opacity','.5');

                    },
                    success: function (res) {

                        if(res=='success'){

                            $(target).parent().parent().fadeOut(500,function(){
                                $(target).parent().parent().remove();
                            });

                            alertify.success('Item has been deleted');

                        }else{

                            $(target).removeClass('disabled');
                            $(target).parent().parent().css('opacity','1');
                            alertify.error('Failed to delete. <br>'+res);
                        }


                    }
                });


            },
            'oncancel':function(){


            }
        });
    });

});
