@extends('layout')
@section('title','Coins')

@section('main')
    <h1 class="page-header">Coins <small>({{$coins->count()}} item)</small></h1>

    <table class="table table-striped table-bordered coins">
        <tr>
            <th>SL</th>
            <th>Name</th>
            <th>Front Dies</th>
            <th>Back Dies</th>
            <th>Top Inventory</th>
            <th>Inventory</th>
            <th>Metal</th>
            <th>Finish</th>
        </tr>
        @foreach($coins as $index=>$coin)
            <tr>
                <td>{{$index+1}}</td>
                <td>{{$coin->name}}</td>
                <td>

                    {{--{!! Html::image('upload/50-'.$coin->front_dies()->photo, $coin->front_dies()->photo, array('class' => 'dies_photo thumb pull-left')) !!}--}}

                    {{--{{$coin->front_dies()->name}}--}}

                </td>

                <td>

                    {{--{!! Html::image('upload/50-'.$coin->back_dies()->photo, $coin->back_dies()->photo, array('class' => 'dies_photo thumb pull-left')) !!}--}}

                    {{--{{$coin->back_dies()->name}}--}}

                </td>


                <td>{{$coin->top_inventory}}</td>
                <td>{{$coin->inventory}}</td>
                <td>{{$coin->metal}}</td>
                <td>{{$coin->finish}}</td>


            </tr>
        @endforeach
    </table>


@stop