@extends('layout')

@section('main')
	<h1 class="page-header">Users</h1>

	@foreach($locations as $location)
		<h4 class="text-muted">{{$location->name}}</h4>
		@foreach($location->managers as $user)
			<li>{{$user->name}}</li>
		@endforeach
	@endforeach

@stop