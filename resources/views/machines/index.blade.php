@extends('layout')

@section('main')
    <h1 class="page-header">Machines <small>({{$machines->count()}} item)</small></h1>

    
    <?php $sl = 1; ?>
    <table class="table table-striped table-bordered first-last-auto">
        <tr><th>SL</th><th>Name</th><th>Price</th><th>Coins</th></tr>
            
        @foreach($machines as $machine)
            <tr>
                <td>{{$sl++}}</td>
                <td>{{$machine->name}}</td>               
                <td>{{$machine->price()}}</td>               
                <td>
                    @for($i=1;$i<=$machine->coins;$i++)
                        @if($i>1), @endif
                        {{$machine->{'coin_'.$i}()->name }}
                    @endfor
                </td>               
               
            </tr>
        @endforeach
    </table>


@stop