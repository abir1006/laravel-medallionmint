<html>
<head>
	<title>No Permission Yet!</title>
</head>
<body>
	<h4>Sorry {{$user->name}} <small class="text-muted">{{$user->role}}</small></h4>
	
	@if($user->role == 'location_manager')
		<p>You don't have permission to see any location info</p>
	@elseif($user->role == 'company_manager')
		<p>No location found at your company -> {{$user->companies->first()->name}}</p>
	@endif


	<a href="/auth/logout">logout</a>
</body>
</html>