@extends('layout')

@section('main')
    <h1 class="page-header">Edit {{$manager->name}}</h1>
    
    @include('partial.form_error')

    <div class="row">
        {!! Form::model($manager,['method'=>'PATCH','url' => [route('location.managers.update',[$location->id,$manager->id])], 'class'=>'col-sm-5']) !!}

        	        	
        	<div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('name','Full Name',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::text('name',null,array('class'=>'form-control','placeholder'=>'Full Name') ) !!}
	        	</div>
	        </div>

	        <div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('email','Email Address',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email') ) !!}
	        	</div>
	        </div>
        	
        	<div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('password','New Password',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::password('password',array('class'=>'form-control','placeholder'=>'Password') ) !!}
	        		<p class="help-block">Leave empty if you don't want to change the password</p>
	        	</div>
	        </div>

	        <div class="row form-group">
	        	<div class="col-sm-5">
	        		
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::submit('Update Manager',array('class'=>'btn btn-primary form-control'))  !!}
	        	</div>
	        </div>
        	
        {!! Form::close() !!}
    	
    </div>

    @include('managers.script')
@stop