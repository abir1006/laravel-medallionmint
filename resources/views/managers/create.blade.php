@extends('layout')

@section('main')
    <h1 class="page-header"><i class="glyphicon glyphicon-user"></i> New Location Manager</h1>
    
    @include('partial.form_error')

    <div class="row">

        {!! Form::open(['url'=>'location/'.$location->id.'/managers','class'=>'col-sm-5']) !!}
        	
        	<div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('name','Full Name',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::text('name',null,array('class'=>'form-control','placeholder'=>'Full Name') ) !!}
	        	</div>
	        </div>

	              	

	        <div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('email','Email Address',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email') ) !!}
	        	</div>
	        </div>
        	
        	<div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('password','New Password',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::password('password',array('class'=>'form-control','placeholder'=>'Password') ) !!}
	        	</div>
	        </div>

	        <div class="row form-group">
	        	<div class="col-sm-5">
	        		
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::submit('Create New Manager',array('class'=>'btn btn-primary form-control'))  !!}
	        	</div>
	        </div>
        	
        {!! Form::close() !!}
    	
    </div>

@stop