@extends('layout')

@section('main')
	<h1 class="page-header">Location Managers
        <a href="{{route('location.managers.create',$location->id)}}" class="btn btn-sm btn-primary pull-right">
            <i class="glyphicon glyphicon-plus"></i> New Manager
        </a>
    </h1>

	<?php $sl = 1; ?>
    <table class="table table-striped table-bordered first-last-auto">
    	<tr><th>SL</th><th>Name</th><th>Email</th><th class="text-center">User ID</th><th>Created At</th><th>Action</th></tr>
    	@foreach($managers as $manager)
	    	<tr data-user-id="{{$manager->id}}">
                <td>{{$sl++}}</td>
                <td>{{$manager->name}}</td>
                <td>{{$manager->email}}</td>
                <td class="text-center">{{sprintf('%04d',$manager->id)}}</td>
                <td>{{$manager->created_at->diffForHumans()}}</td>
	    		<td>
                    
	    			<a title="Edit Basic Info" href="{{route('location.managers.edit',[$location->id,$manager->id])}}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                    <a title="Remove Access" data-url="{{url('location/'.$location->id.'/managers/'.$manager->id.'/remove')}}" data-token="{{csrf_token()}}" class="location_manager_remove_btn btn btn-xs btn-warning" href="#"><i class="glyphicon glyphicon-remove"></i> Remove Access</a>

	    		</td>
	    	</tr>
    	@endforeach
   	</table>

   	@include('managers.script')

@stop