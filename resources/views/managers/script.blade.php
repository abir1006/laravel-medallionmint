<script type="text/javascript">

$('document').ready(function(){

	$('.location_manager_remove_btn').click(function(e){
        e.preventDefault();

        var target = $(this);
        var url = $(this).data('url');
        var token = $(this).data('token');        

        alertify.confirm('This user can not be able to access this location anymore.').setting({
            'title':'<span class="glyphicon glyphicon-remove"></span> Remove Location Manager Access',
            'labels':{ok:'REMOVE ACCESS'},
            'onok': function(){
                target.closest('tr').remove();
                $.ajax({

                    url: url,
                    type: 'POST',
                    data: {_token: token},
                    success: function (res) {
                        //console.log(res);
                        if(res=='success'){                              
                           alertify.success('Manager Access Removed'); 
                        }else{
                          alertify.error('Failed to remove manager access');	
                        }
                    }
                });

            }

        });
    });


});


</script>