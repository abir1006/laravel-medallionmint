@extends('layout')

@section('main')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title clearfix"><i class="glyphicon glyphicon-book"></i> Reports (<span class="report_counter">{{$location->report->count()}}</span>)
                @if($location->machines->count())
                    <button type="button" class="btn btn-xs btn-info pull-right" data-toggle="modal" data-target="#report_modal"> <i class="glyphicon glyphicon-plus"></i> New Report </button>
                @endif
            </h3>
        </div>



        @if(!$location->report->count())
        
            <p class="alert alert-warning">No report submitted yet</p>
            
        @else
        <?php $sl = 1; ?>
        <table class="table table-striped table-bordered reports first-last-auto">
            <tr><th>SL</th><th>Type</th><th>Month</th><th>Year</th><th>Read Date</th><th>Read Time</th>
                <th>Read By</th><th>Verified By</th><th>Created At</th><th>Action</th></tr>
            @foreach($location->report()->orderBy('created_at','desc')->get() as $report)
                <tr>
                    <td>{{$sl++}}</td>                   
                    <td>{{studly_case($report->type)}}</td>
                    <td>{{$report->month}}</td>
                    <td>{{$report->year}}</td>
                    <td>{{date('d F Y',strtotime($report->date))}}</td>
                    <td>{{date('h:i A',strtotime($report->time))}}</td>
                    <td>{{$report->read_by}}</td>
                    <td>{{$report->verified_by}}</td>
                    <td>{{$report->created_at->diffForHumans()}}</td> 
                    <td>
                        <a href="{{url('location/'.$location->id.'/reports/'.$report->id)}}" target="blank" class="btn btn-xs btn-default" title="view PDF at new tab"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                        <a href="{{url('location/'.$location->id.'/reports/'.$report->id)}}/download" class="btn btn-xs btn-default" title="Download PDF"><i class="glyphicon glyphicon-download"></i> Download</a>
                        <a href="#" class="btn btn-xs btn-default btn-expand pull-right" title="Expand/Collapse"><i class="glyphicon glyphicon-chevron-down"></i></a>
                    </td>                 
                </tr>

                <tr class="expandable">
                    <td colspan="10">
                        @foreach($report->machine_readings as $machine_reading)                        
                            @if($machine_reading->machine())
                            <div class="panel panel-info">        
                                <div class="panel-heading">
                                    <h3 class="panel-title clearfix">{{$machine_reading->machine()->name}} <span class="pull-right">Machine ID # {{$machine_reading->machine_id}}</span></h3>
                                </div>
                                <table class="table table-striped table-condensed">                                
                                    <tr><th>Meter</th><th>Previous</th><th>Current</th><th>Adj</th><th>Total</th><th>Display</th><th>Stock</th></tr>
                                    <tr><td><i class="glyphicon glyphicon-usd"></i> Meter 1 (Cash)</td><td>{{$machine_reading->meter_1_previous}}</td><td>{{$machine_reading->meter_1_current}}</td><td>{{$machine_reading->meter_1_adj}}</td><td>{{$machine_reading->meter_1_current-$machine_reading->meter_1_previous+$machine_reading->meter_1_adj}}</td><td></td><td></td></tr>
                                    <tr><td><i class="glyphicon glyphicon-credit-card"></i> Meter 2 (Credit Card)</td><td>{{$machine_reading->meter_2_previous}}</td><td>{{$machine_reading->meter_2_current}}</td><td>{{$machine_reading->meter_2_adj}}</td><td>{{$machine_reading->meter_2_current-$machine_reading->meter_2_previous+$machine_reading->meter_2_adj}}</td><td></td><td></td></tr>
                                    @for($i=1;$i<=$machine_reading->machine()->coins;$i++)
                                    <tr>
                                        <td><i class="glyphicon glyphicon-record"></i> Meter {{$i+2}} ({{$machine_reading->machine()->{'coin_'.$i}()->name }})</td>
                                        <td>{{$machine_reading->{'meter_'.($i+2).'_previous'} }}</td>
                                        <td>{{$machine_reading->{'meter_'.($i+2).'_current'} }}</td>
                                        <td>{{$machine_reading->{'meter_'.($i+2).'_adj'} }}</td>
                                        <td>{{ $machine_reading->{'meter_'.($i+2).'_current'} - $machine_reading->{'meter_'.($i+2).'_previous'} + $machine_reading->{'meter_'.($i+2).'_adj'} }}</td>
                                        <td>{{$machine_reading->{'meter_'.($i+2).'_display'} }}</td>
                                        <td>{{$machine_reading->{'meter_'.($i+2).'_stock'} }}</td>
                                    </tr>
                                    @endfor
                                    
                                </table>
                            </div>
                            @else
                            <p class="alert alert-danger">Oppps a machine (<strong>machine_id # {{$machine_reading->machine_id}}</strong>) has been deleted after generating report</p>
                            @endif
                        @endforeach
                    </td>
                </tr>

            @endforeach
        </table>
        @endif
    </div>


    <div class="modal fade" id="report_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="glyphicon glyphicon-book"></i> New Report</h4>
                </div>
                <div class="modal-body">
                    

                    @if($last_report_created_ago>25)
                        <p class="text-warning">Daily & Weekly report is disabled, coz last report submitted more than 25 days ago.</p>
                        
                        <a href="#" class="btn btn-sm btn-default disabled"><i class="glyphicon glyphicon-time"></i> Daily Report</a>
                        <a href="#" class="btn btn-sm btn-default disabled"><i class="glyphicon glyphicon-th"></i> Weekly Report</a>
                    @else
                        <a href="{{url('location/'.$location->id.'/reports/create')}}/daily" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-time"></i> Daily Report</a>
                        <a href="{{url('location/'.$location->id.'/reports/create')}}/weekly" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-th"></i> Weekly Report</a>
                    @endif

                    <a href="{{url('location/'.$location->id.'/reports/create')}}/monthly" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-calendar"></i> Monthly Report</a>
                    

                    <br><br>
                    @if($location->report->count())
                        <p class="text-muted">
                            <i class="glyphicon glyphicon-info-sign"></i> Your last report submitted 
                            <strong>{{$location->report()->orderBy('created_at','desc')->first()->created_at->diffForHumans()}}</strong>
                            ({{$location->report()->orderBy('created_at','desc')->first()->type}})
                        </p>
                    @else
                        <p class="text-info"><i class="glyphicon glyphicon-info-sign"></i> You have not submitted any report yet</p>
                    @endif

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>            
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    @include('reports.script')
@stop