<table border="1" cellpadding="3" style="border:1px solid #ddd;border-collapse:collapse;">

    <thead>

        <tr>
            <th>SL</th>
            <th>Location</th>
            <th>Machine</th>
            <th>Device</th>
            <th>Days</th>
            <th>Last Call-In</th>
            <!--
            <th>Days</th>
            <th>Last Cash Tran</th>
            <th>Days</th>
            <th>Last Credit Tran</th>
            -->

        </tr>
    </thead>

    <tbody>
    <?php $now = \Carbon\Carbon::now(); $sl=1;?>
    @foreach($all_device_health as $index=>$device_health)
        <tr>
            <td>{{$sl++}}</td>

            <td>{{$device_health->machine->location->name}}</td>

            <td>{{$device_health->machine->name}}</td>

            <td>{{$device_health->device}}</td>

            <td>
                @if(strtotime($device_health->date_of_last_call_in) < strtotime('-40 years') )
                    Never
                @else
                    {{$device_health->date_of_last_call_in->diff($now)->days}}
                @endif
            </td>

            <td>
                @if(strtotime($device_health->date_of_last_call_in) < strtotime('-40 years') )
                    Never
                @else
                    {{date('m-d-Y h:i A',strtotime($device_health->date_of_last_call_in))}}
                @endif
            </td>

            {{--<td>
                @if(strtotime($device_health->date_of_last_cash_tran) < strtotime('-40 years') )
                    Never
                @else
                    {{$device_health->date_of_last_cash_tran->diff($now)->days}}
                @endif
            </td>

            <td>

                @if(strtotime($device_health->date_of_last_cash_tran) < strtotime('-40 years') )
                    Never
                @else
                    {{date('m-d-Y h:i A',strtotime($device_health->date_of_last_cash_tran))}}
                @endif
            </td>

            <td>
                @if(strtotime($device_health->date_of_last_credit_tran) < strtotime('-40 years') )
                    Never
                @else
                    {{$device_health->date_of_last_credit_tran->diff($now)->days}}
                @endif
            </td>

            <td>
                @if(strtotime($device_health->date_of_last_credit_tran) < strtotime('-40 years') )
                    Never
                @else
                    {{date('m-d-Y h:i A',strtotime($device_health->date_of_last_credit_tran))}}
                @endif
            </td>--}}
        </tr>
    @endforeach
    </tbody>
</table>