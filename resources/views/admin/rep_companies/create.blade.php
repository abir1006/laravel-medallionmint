@extends('admin.layout')
@section('title','New Rep Company')

@section('main')
	<h1 class="page-header">Add New Rep Company</h1>
	@include('partial.form_error')

	{!! Form::open(['url'=>'admin/rep_companies','class'=>'row form-horizontal']) !!}

	@include('admin.rep_companies.form',['submit_btn_text'=>'Add Rep Company'])

	{!!Form::close()!!}


@stop

@section('script')
	@include('admin.rep_companies.script')
@stop