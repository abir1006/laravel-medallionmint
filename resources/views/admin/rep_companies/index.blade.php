@extends('admin.layout')
@section('title','Rep Companies')

@section('main')
	<h1 class="page-header">Rep Companies
		<a href="{{route('admin.rep_companies.create')}}" class="btn btn-sm btn-primary pull-right "> <i class="glyphicon glyphicon-plus"></i> Add New Rep Company</a>

	</h1>
	@if(!count($rep_companies))

		<p class="alert alert-warning">
			No Rep Company found! Please
			<a href="{{ url('admin/rep_companies/create')}}">create a Rep Company</a>
		</p>

	@else

		<table class="table table-striped table-bordered table_sortable">
			<thead>
			<tr><th>SL</th><th>Rep Company Name</th><th>Locations</th><th>Machines</th><th>Action</th></tr>
			</thead>
			@foreach($rep_companies as $index=>$rep_company)
				<tr>
					<td>{{1+$index++}}</td>
					<td>
						<a href="{{ URL::route('admin.rep_companies.show',$rep_company->id) }}">{{$rep_company->name}}</a>

					</td>
					<td>{{ $rep_company->locations->count() }}</td>
					<td>{{ $rep_company->machines->count() }}</td>

					<td>

						<a href="{{ URL::route('admin.rep_companies.edit',$rep_company->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
						<a title="delete" data-url="{{url('admin/rep_companies/'.$rep_company->id)}}" data-token="{{csrf_token()}}" class="admin_rep_company_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

					</td>
				</tr>
			@endforeach
		</table>

	@endif

@stop

@section('script')
	@include('admin.rep_companies.script')
@stop
