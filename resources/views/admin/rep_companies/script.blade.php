<script>
    $(document).ready(function(){

        $('.admin_rep_company_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this rep company? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Rep Company',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().remove();
                                });

                                alertify.success('Rep Company has been deleted');
                                $('.nav-sidebar .badge.rep_company').html(parseInt($('.nav-sidebar .badge.rep_company').html())-1);

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to delete rep company. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });



    });
</script>