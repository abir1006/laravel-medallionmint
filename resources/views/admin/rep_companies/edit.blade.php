@extends('admin.layout')
@section('title','Edit '.$rep_company->name)

@section('main')
	<a class="btn btn-sm btn-info pull-right" href="{{URL::route('admin.rep_companies.show',$rep_company->id)}}">
		<i class="glyphicon glyphicon-eye-open"></i> View Details
	</a>
	<h1 class="page-header">
		Edit {{ $rep_company->name }}
	</h1>
	@include('partial.form_error')

	{!! Form::model($rep_company,['method'=>'PATCH','url' => ['admin/rep_companies',$rep_company->id ],'files'=>'true', 'class'=>'row form-horizontal edit']) !!}

		@include('admin.rep_companies.form',['submit_btn_text'=>'Update Rep Company'])

	{!!Form::close()!!}

@stop

@section('script')
	@include('admin.rep_companies.script')
@stop