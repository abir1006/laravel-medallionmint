@extends('admin.layout')
@section('title',$rep_company->name)

@section('main')
	<h1 class="page-header">{{ $rep_company->name }}
		<a href="{{ URL::route('admin.rep_companies.edit',$rep_company->id) }}" class="btn btn-sm btn-info pull-right"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
	</h1>

    <div class="form-group">
        @include('partial.form_error')
    </div>

	<!-- Nav tabs -->
	<ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
		<li role="presentation" ><a href="#locations" aria-controls="locations" role="tab" data-toggle="tab">Locations ({{$rep_company->locations->count()}})</a></li>
		<li role="presentation" ><a href="#address" aria-controls="address" role="tab" data-toggle="tab">Address ({{$rep_company->addresses->count()}})</a></li>
	</ul>


	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="details">
			<table class="table table-striped">

				<tr>
					<td>Term</td>
					<td>
						@if($rep_company->term)
							{{$rep_company->term->name}}
						@endif
					</td>
				</tr>
				<tr>
					<td>Preferred Carrier</td>
					<td>
						@if($rep_company->carrier)
							{{$rep_company->carrier->name}}
						@endif
					</td>
				</tr>
				<tr>
					<td>Shipper #</td>
					<td>{{$rep_company->shipper_no}}</td>
				</tr>
				<tr>
					<td>Time Zone</td>
					<td>
						@if($rep_company->time_zone)
							{{$rep_company->time_zone->name}}
						@endif
					</td>
				</tr>
				<tr><td>Website</td><td>{{$rep_company->website}}</td></tr>
				<tr>
					<td>Notes</td>
					<td>{{$rep_company->notes}}</td>
				</tr>

				<tr><td>Status</td><td>{{$rep_company->active ? 'Active' :'InActive'}}</td></tr>
			</table>
		</div>

		<div role="tabpanel" class="tab-pane" id="locations">
			<table class="table table-bordered ">
				<thead>
					<tr>
						<th>SL</th>
						<th>Location Name</th>
					</tr>
				</thead>

				<tbody>

					@foreach($rep_company->locations as $index=>$location)
						<tr>
							<td>{{$index+1}}</td>
							<td><a href="/admin/locations/{{$location->id}}">{{$location->name}}</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>

		<div role="tabpanel" class="tab-pane" id="address">

            @include('admin.address.index',[
                'path'=>'admin/rep_companies/'.$rep_company->id.'/address#address',
                'addresses' => $rep_company->addresses

                ])

		</div>

	</div>

    @include('admin.address.modal')


@stop

@section('script')
	@include('admin.rep_companies.script')
	@include('admin.address.script')
@stop