<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>@yield('title')</title>

    @yield('stylesheet')

    <link href="/lib/bootstrap-3.3.2-dist/css/bootstrap.css" rel="stylesheet">
    <link href="/lib/bootstrap-3.3.2-dist/css/bootstrap-theme.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>

    <link href="/lib/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet">
    <link href="/lib/bootstrap-clockpicker/bootstrap-clockpicker.css" rel="stylesheet">
    <link href="/lib/alertify/alertify.min.css" rel="stylesheet">
    <link href="/lib/alertify/alertify.default.css" rel="stylesheet">


    <link href="/lib/chosen_v1.4.0/chosen.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/sidebar-nav.css">
    <link href="/css/style.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    {{--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.css">--}}
    <link rel="stylesheet" href="/lib/bootstrap-switch-3/bootstrap-switch.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="/images/logo_small.gif" alt="my.medallionmint.com"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">


            <ul class="nav navbar-nav navbar-right">

                <li {!!(Request::is('admin/profile') ? 'class="active"' : '')!!}>
                    <a href="/admin/profile">{{Auth::user()->name}}</a>
                </li>

                <li {!!(Request::is('admin/help') ? 'class="active"' : '')!!}>
                    <a href="/admin/help">Help</a>
                </li>
            </ul>


        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">

          <ul class="nav nav-sidebar">
            <li {!! (Request::is('admin/dashboard') ? 'class="active"' : '' ) !!}><a href="/admin/dashboard"><i class="glyphicon glyphicon-dashboard"></i>  Dashboard</a></li>

            <li {!! (Request::is('admin/users*') ? 'class="active"' : '' ) !!} >
              <a href="{{url('admin/users')}}"><i class="glyphicon glyphicon-user"></i>
                Users <span class="badge user pull-right">{{App\User::where('active','=','1')->count()}}</span>
              </a>
            </li>

            <li {!! (Request::is('admin/companies*') ? 'class="active"' : '' ) !!} >
              <a href="{{url('admin/companies')}}"><i class="glyphicon glyphicon-list"></i>
                Parent Companies <span class="badge company pull-right">{{App\Company::where('active','=','1')->count()}}</span>
              </a>
            </li>



            <li {!! (Request::is('admin/rep_companies*') ? 'class="active"' : '' ) !!} >
              <a href="{{url('admin/rep_companies')}}"><i class="glyphicon glyphicon-list"></i>
                Rep Companies <span class="badge rep_company pull-right">{{App\RepCompany::where('active','=','1')->count()}}</span>
              </a>
            </li>

            <li {!! (Request::is('admin/managing_companies*') ? 'class="active"' : '' ) !!} >
              <a href="{{url('admin/managing_companies')}}"><i class="glyphicon glyphicon-list"></i>
                Managing Companies <span class="badge managing_company pull-right">{{App\ManagingCompany::where('active','=','1')->count()}}</span>
              </a>
            </li>

            <li {!! (Request::is('admin/locations*') ? 'class="active"' : '' ) !!} >
              <a href="{{url('admin/locations')}}"><i class="glyphicon glyphicon-map-marker"></i>
                Locations <span class="badge location badge-info pull-right">{{App\Location::where('active','=','1')->count()}}</span>
              </a>
            </li>


            <li {!! (Request::is('admin/carriers*') ? 'class="active"' : '' ) !!} >
              <a href="{{url('admin/carriers')}}"><i class="glyphicon glyphicon-envelope"></i>
                Carriers <span class="badge carrier badge-info pull-right">{{App\Carrier::where('active','=','1')->count()}}</span>
              </a>
            </li>

            <li {!! (Request::is('admin/programs*') ? 'class="active"' : '' ) !!} >
              <a href="{{url('admin/programs')}}"><i class="glyphicon glyphicon-th-list"></i>
                Programs <span class="badge program badge-info pull-right">{{App\Program::where('active','=','1')->count()}}</span>
              </a>
            </li>

           <li {!! (Request::is('admin/machines*') ? 'class="active"' : '' ) !!} >
                <a href="{{url('admin/machines')}}"><i class="glyphicon glyphicon-film"></i>
                    Machines <span class="badge machines badge-info pull-right">{{App\Machine::all()->count()}}</span>
                </a>
              </li>

            <li {!! (Request::is('admin/machine_settings') ? 'class="active"' : '')!!} >
              <a href="{{url('admin/machine_settings')}}"><i class="glyphicon glyphicon-cog"></i> Machine Settings</a>
            </li>


              <li class="has-dropdown">
                  <a href="#"><i class="glyphicon glyphicon-wrench"></i> Machine Parts</a>
                  <ul>
                      <li><a href="{{url('admin/gizmos')}}">Gizmos</a></li>
                      <li><a href="{{url('admin/meter_boxes')}}">Meter Boxes</a></li>
                      <li><a href="{{url('admin/cc_modems')}}">CC Modems</a></li>
                      <li><a href="{{url('admin/cc_readers')}}">CC Readers</a></li>
                      <li><a href="{{url('admin/bill_validators')}}">Bill Validators</a></li>
                      <li><a href="{{url('admin/vaults')}}">Vaults</a></li>
                      <li><a href="{{url('admin/hoppers')}}">Hoppers</a></li>
                      <li><a href="{{url('admin/touch_screens')}}">Touch Screens</a></li>

                  </ul>
              </li>



              <li {!! (Request::is('admin/coins*') ? 'class="active"' : '' ) !!} >
                <a href="{{url('admin/coins')}}"><i class="glyphicon glyphicon-copyright-mark"></i>
                    Coins <span class="badge coins badge-info pull-right">{{App\Coin::all()->count()}}</span>
                </a>
              </li>


              <li {!! (Request::is('admin/dies*') ? 'class="active"' : '' ) !!} >
              <a href="{{url('admin/dies')}}"><i class="glyphicon glyphicon-record"></i>
                  Dies <span class="badge dies badge-info pull-right">{{App\Dies::all()->count()}}</span>
              </a>
              </li>

              <li {!! (Request::is('admin/device_health') ? 'class="active"' : '' ) !!} >
                <a href="{{url('admin/device_health')}}"><i class="glyphicon glyphicon-filter"></i>Device Health</a>
              </li>


            <li><a href="/auth/logout"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
          </ul>




        </div>
        <div class="col-sm-9 col-md-10 main">


            @yield('main')



        </div>
    </div>
</div>


<script src="/lib/jquery-2.1.1.min.js"></script>

<script src="/lib/bootstrap-3.3.2-dist/js/bootstrap.js"></script>
<script src="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.js"></script>
<script src="/lib/bootstrap-datepicker/bootstrap-datepicker.js"></script>

<script src="/lib/bootstrap-clockpicker/bootstrap-clockpicker.js"></script>
<script src="/js/bootbox-4.4.0.min.js"></script>
<script src="/lib/alertify/alertify.min.js"></script>
<script src="/js/holder.js"></script>

<!--
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
-->

<script src="/lib/Highcharts/js/highcharts.js"></script>

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js" type="text/javascript"></script>--}}
<script src="/lib/bootstrap-switch-3/bootstrap-switch.js" type="text/javascript"></script>

<script src="/lib/geocomplete/jquery.geocomplete.min.js"></script>

<script src="/lib/chosen_v1.4.0/chosen.jquery.min.js"></script>


<script src="/lib/jquery.validate.min.js"></script>

<script src="/lib/moment.js"></script>

<script src="/js/custom.js"></script>


@if(Session::has('flash_message'))
    <?php echo "<script>alertify.success('". session('flash_message'). "');</script>"; ?>
@endif

@yield('script')


</body>
</html>