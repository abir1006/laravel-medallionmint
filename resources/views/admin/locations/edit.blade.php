@extends('admin.layout')
@section('title', 'Edit '.$location->name)
@section('main')
   <h1 class="page-header">Edit {{$location->name}}</h1>

   
	{!! Form::model($location,['method'=>'PATCH','url' => ['admin/locations',$location->id ],'files'=>'true', 'class'=>'row form-horizontal edit']) !!}

   		@include('admin.locations.form',['submit_btn_text'=>'Update Location Info'])

	{!!Form::close()!!}

   
@stop

@section('script')
	@include('admin.locations.script')
@stop