@extends('admin.layout')
@section('title', 'New Location')
@section('main')
   <h1 class="page-header">Add New Location</h1>
   

	{!! Form::open(['url'=>'admin/locations','files'=>'true','class'=>'row form-horizontal']) !!}

   		@include('admin.locations.form',['submit_btn_text'=>'Add New Location'])

			
	{!!Form::close()!!}
	

@stop
	
@section('script')
	@include('admin.locations.script')
@stop