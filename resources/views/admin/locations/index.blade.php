@extends('admin.layout')
@section('title', 'All Locations')
@section('main')

	<h1 class="page-header">Locations
		<a href="{{URL::to('admin/locations/create')}}" class="btn btn-sm btn-primary pull-right">
			<i class="glyphicon glyphicon-plus"></i> Add New Location
		</a>
	</h1>

   @if(!count($locations))
           <p class="alert alert-warning">
               No locations found! Please
               <a href="{{ url('admin/locations/create')}}">create a location</a>
           </p>
   @else

   	<?php $sl = 1; ?>
   	<table class="table table-striped table-bordered table_sortable" id="locations_index">
        <thead>

            <tr>
				<th>SL</th>
				<th>Locations</th>
				<th>Parent Company</th>
				<th>Rep Company</th>
				<th>Managing Company</th>
				<th>Machines</th>
				<th>Action</th>
			</tr>
        </thead>
	   	@foreach($locations as $location)
	   	<tr>
	   		<td>{{$sl++}}</td>
	   		<td>
				<a href="{{ URL::route('admin.locations.show',$location->id) }}"><strong>{{$location->name}}</strong></a>
	   		</td>
        

	   		<td>
				@if($location->company)
	   				<a href="{{ URL::route('admin.companies.show',$location->company->id) }}">{{ $location->company->name }}</a>
				@endif
	   		</td>

			<td>
				@if($location->rep_company)
	   				<a href="{{ URL::route('admin.rep_companies.show',$location->rep_company->id) }}">{{ $location->rep_company->name }}</a>
				@endif
	   		</td>

			<td>
				@if($location->managing_company)
	   				<a href="{{ URL::route('admin.managing_companies.show',$location->managing_company->id) }}">{{ $location->managing_company->name }}</a>
				@endif
	   		</td>

			<td>{{$location->machines->count()}}</td>


		    <td>

	   			<a href="{{ URL::route('admin.locations.edit',$location->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
				<a title="delete" data-url="{{url('admin/locations/'.$location->id)}}" data-token="{{csrf_token()}}" class="admin_location_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a> 

	   		</td>
	   	</tr>
	   	@endforeach
   	</table>

   @endif


@stop


@section('script')
  @include('admin.locations.script')
@stop