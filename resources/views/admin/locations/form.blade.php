<div class="col-sm-12">
	@include('partial.form_error')
</div>


<div class="col-sm-6">
	<div class="form-group">
		{!!Form::label('name','Location Name',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7 @if ($errors->has('name')) has-error @endif">
			{!!Form::text('name',null,array('class'=>'form-control')) !!}
			@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('location_type_id','Location Type',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('location_type_id',array(null=>'Please Select')+$location_types,null,array('class'=>'form-control searchable_select')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('company_id','Parent Company',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('company_id',array(null=>'Please Select')+$companies,null,array('class'=>'form-control searchable_select')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('sales_type_id','Sales Type',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('sales_type_id',array(null=>'Please Select')+$sales_types,null,array('class'=>'form-control searchable_select')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('program_id','Program',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('program_id',array(null=>'Please Select')+$programs,null,array('class'=>'form-control searchable_select')) !!}
		</div>
	</div>


	<div class="form-group">
		{!!Form::label('split_id','Split',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('split_id',array(null=>'Please Select')+$splits,null,array('class'=>'form-control searchable_select')) !!}
		</div>
	</div>

	<div class="form-group">

		{!!Form::label('term_id','Terms',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('term_id',array(null=>'Please Select')+$terms,null,array('class'=>'form-control searchable_select')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('mm_rep_id','MM Rep',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('mm_rep_id',array(null=>'Please Select')+$mm_reps,null,array('class'=>'form-control searchable_select')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('rep_company_id','Rep Company',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7 @if ($errors->has('rep_company_id')) has-error @endif">
			{!!Form::select('rep_company_id',array(null=>'Please Select')+$rep_companies,null,array('class'=>'form-control searchable_select')) !!}
			@if ($errors->has('rep_company_id')) <p class="help-block">{{ $errors->first('rep_company_id') }}</p> @endif
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('managing_company_id','Managing Company',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('managing_company_id',array(null=>'Please Select')+$managing_companies,null,array('class'=>'form-control searchable_select')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('website','Website',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::text('website',null,array('class'=>'form-control')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('est_guests','Estimated Guests',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::text('est_guests',null,array('class'=>'form-control')) !!}
		</div>
	</div>

</div>

<div class="col-sm-6">
	<div class="form-group">
		{!!Form::label('has_tax_stamp','Tax Stamp',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			<div class="make-switch has_tax_stamp_switch" data-on="primary" data-off="info" data-on-label="Yes" data-off-label="No">
				{!!Form::checkbox('has_tax_stamp',1,false,array('class'=>'checkbox')) !!}
			</div>
		</div>
	</div>

	<div class="form-group sales_tax_rate">
		{!!Form::label('sales_tax_rate','Sales Tax Rate',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::text('sales_tax_rate',null,array('class'=>'form-control')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('carrier_id','Carrier',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('carrier_id',array(null=>'Please Select')+$carriers,null,array('class'=>'form-control searchable_select')) !!}
		</div>
	</div>

	<div class="form-group corporate_shipper_no">
		{!!Form::label('corporate_shipper_no','Corporate Shipper #',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::text('corporate_shipper_no',null,array('class'=>'form-control')) !!}
		</div>
	</div>

	<div class="form-group">

		{!!Form::label('has_custom_cc_percent','Has Custom CC Percentage',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			<div class="make-switch has_custom_cc_percent_switch" data-on="primary" data-off="info" data-on-label="Yes" data-off-label="No">
				{!!Form::checkbox('has_custom_cc_percent',1,false,array('class'=>'checkbox')) !!}
			</div>
		</div>
	</div>

	<div class="form-group custom_cc_percent">
		{!!Form::label('custom_cc_percent','Custom CC Percentage',array('class'=>'control-label col-xs-5'))!!}

		<div class="col-xs-7">
			{!!Form::text('custom_cc_percent',null,array('class'=>'form-control')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('has_seasons','Seasons',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-1">
			<div class="make-switch seasons_switch" data-on="primary" data-off="info" data-on-label="Yes" data-off-label="No">
				{!!Form::checkbox('has_seasons',1,false,array('class'=>'checkbox')) !!}
			</div>
		</div>
	</div>

	<div class="form-group has_seasons">
		{!!Form::label('duration','Duration',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-3">
			@if(isset($location))
				{!! Form::text('season_start_at', date('m/d/Y',strtotime($location->season_start_at)), ['class'=>'form-control datepicker']) !!}
			@else
				{!! Form::text('season_start_at', null, ['class'=>'form-control datepicker']) !!}
			@endif
		</div>
		<div class="col-xs-4">
			@if(isset($location))
				{!! Form::text('season_end_at', date('m/d/Y',strtotime($location->season_end_at)), ['class'=>'form-control datepicker']) !!}
			@else
				{!! Form::text('season_end_at', null, ['class'=>'form-control datepicker']) !!}
			@endif
		</div>
	</div>

	<div class="form-group">


		{!!Form::label('is_exclude','Exclude from reports',array('class'=>'control-label col-xs-5'))!!}

		<div class="col-xs-7">
			<div class="make-switch" data-on="primary" data-off="info" data-on-label="Yes" data-off-label="No">
				{!!Form::checkbox('is_exclude',1,false,array('class'=>'checkbox')) !!}
			</div>
		</div>

	</div>

	<div class="form-group">
		{!!Form::label('notes','Notes',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::textarea('notes',null,array('class'=>'form-control','rows'=>'3')) !!}
		</div>
	</div>


	<div class="form-group">
		{!!Form::label('active','Active',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			<div class="make-switch" data-on="success" data-off="danger" data-on-label="Yes" data-off-label="No">
				{!!Form::checkbox('active',1,true,array('class'=>'checkbox')) !!}
			</div>
		</div>
	</div>

	<div class="form-group">

		<div class="col-sm-offset-5 col-sm-7">
			@if(isset($location))
				{!!Form::label('photo','Location Photo ('.$location->photos->count().')')!!}
				@if($location->photo())
					{!! Html::image('/upload/location_photo/200-'.$location->photo()->name,null,array('class'=>'thumbnail')) !!}
				@else
					{!! Html::image('/images/no_image_90x90.png',null,['class'=>'thumbnail'])!!}
				@endif

				<p><a href="/admin/locations/{{$location->id}}#photos">Manage Photos</a></p>

			@endif
		</div>

	</div>

	<div class="form-group">
		<div class="col-sm-offset-5 col-sm-7">

			{!! Form::submit($submit_btn_text,['class'=>'btn btn-primary']) !!}

			@if(Request::is('admin/locations/*/edit'))
				<a href="{{ URL::route('admin.locations.show', $location->id) }}" class="btn btn-default"><i class="glyphicon glyphicon-remove-circle"></i> Cancel</a>
			@elseif(Request::is('admin/locations/create'))
				<a href="{{ URL::route('admin.locations.index') }}" class="btn btn-default"><i class="glyphicon glyphicon-remove-circle"></i> Cancel</a>
			@endif
		</div>

	</div>

</div>

