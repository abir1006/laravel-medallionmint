<!-- Modal -->
<div class="add_setting modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  {!! Form::open(['url'=>'admin/machine_settings','class'=>'add_setting_form']) !!}
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>

      <div class="modal-body"></div>
               
        

    </div>
  </div>
  {!! Form::close()!!}
</div>

<table class="table table-bordered table-striped add_setting_step machine_types">    
    <thead> <tr><th>Machine Type</th><th></th></tr> </thead>
    <tbody>
        <tr>                         
            <td><input name="type" class="form-control" placeholder="Enter Machine Type" required></td>                 
        </tr>                                        
    </tbody>    
</table>

<table class="table  table-bordered table-striped add_setting_step machine_colors">
    <thead> <tr><th>Type</th><th>SW Code</th><th>Color</th><th></th></tr> </thead>
    <tbody>
        <tr>
            <td>
                <select name="type" class="form-control" required>
                    <option value="">Please Select Type</option>
                    <option value="Clear Coat">Clear Coat</option>
                    <option value="Paint">Paint</option>
                    <option value="Stain">Stain</option>
                </select>
            </td>
            <td><input name="sw_code" class="form-control" placeholder="SW Code" required> </td> 
            <td><input name="color" class="form-control" placeholder="Color" required></td>            
        </tr>        
    </tbody>    
</table>


<table class="table table-striped table-bordered add_setting_step machine_keys">
    <thead><tr><th>Machine Keys</th><th></th> </tr></thead>

    <tbody>
        <tr>
            <td><input name="machine_key" class="form-control" placeholder="Machine Key" required></td>            
        </tr>                              
    </tbody>
    
</table>


<table class="table table-striped table-bordered add_setting_step prices">
    <thead>
        <tr>
          <th>Prices</th>
          <th colspan="2" style="text-align:center;">Level 1</th>
          <th colspan="2" style="text-align:center;">Level 2</th>         
          <th></th></tr>

    </thead>

    <tbody>
        <tr>                            
            <td><input name="prices" class="form-control" placeholder="Prices" required style="width:140px;"></td>
            <td><input name="level_1_credit" class="form-control" placeholder="$" required></td>
            <td><input name="level_1_coin" class="form-control" placeholder="#coin" required></td>
            <td><input name="level_2_credit" class="form-control" placeholder="$" required></td>
            <td><input name="level_2_coin" class="form-control" placeholder="#coin" required></td>                 
        </tr>                        
    </tbody>    
</table>


<table class="table table-striped table-bordered add_setting_step motors">
    <thead>
        <tr><th>Company</th><th>Voltage</th><th></th></tr>
    </thead>

    <tbody>
        <tr>
            <td><input name="company" class="form-control" placeholder="Company" required></td>
            <td><input name="voltage" class="form-control" placeholder="Voltage" required></td>           
        </tr>                        
    </tbody>
    
</table>


<table class="table table-striped table-bordered add_setting_step button_lights">
    <thead>
    <tr><th>Company</th><th>Type</th><th></th></tr>
    </thead>

    <tbody>
    <tr>
        <td><input name="company" class="form-control" placeholder="Company" required></td>
        <td><input name="type" class="form-control" placeholder="Type" required></td>
    </tr>
    </tbody>

</table>

<table class="table table-striped table-bordered add_setting_step touch_screen_models">
    <thead>
    <tr><th>Company</th><th>Type</th><th></th></tr>
    </thead>

    <tbody>
    <tr>
        <td><input name="company" class="form-control" placeholder="Company" required></td>
        <td><input name="type" class="form-control" placeholder="Type" required></td>
    </tr>
    </tbody>

</table>

<table class="table table-striped table-bordered add_setting_step touch_screen_fws">
    <thead>
    <tr><th>FW</th><th></th></tr>
    </thead>

    <tbody>
    <tr>
        <td><input name="fw" class="form-control" placeholder="fw" required></td>
    </tr>
    </tbody>

</table>

<table class="table table-striped table-bordered add_setting_step gizmo_models">
    <thead>
        <tr><th>Company</th><th>Model</th><th></th></tr>
    </thead>

    <tbody>
        <tr>                              
            <td><input name="company" class="form-control" placeholder="Company" required></td>
            <td><input name="model" class="form-control" placeholder="Model" required></td>
        </tr>
    </tbody>
    
</table>

<table class="table table-striped table-bordered add_setting_step meter_box_models">
    <thead>
        <tr><th>Company</th><th>Model</th><th></th></tr>
    </thead>

    <tbody>
        <tr>                            
            <td><input name="company" class="form-control" placeholder="Company" required></td>
            <td><input name="model" class="form-control" placeholder="Model" required></td>            
        </tr>                        
    </tbody>
    
</table>

<table class="table table-striped table-bordered add_setting_step gizmo_fws">
    <thead>
        <tr><th>Gizmo FWS</th><th></th></tr>
    </thead>

    <tbody>
        <tr>                           
            <td><input name="gizmo_fws" class="form-control" placeholder="Gizmo FWS" required></td>                            
        </tr>                        
    </tbody>
    
</table>

 <table class="table table-striped table-bordered add_setting_step cc_companies">
    <thead>
        <tr><th>Credit Card Company</th><th></th></tr>
    </thead>

    <tbody>
        <tr>
            <td><input name="cc_company" class="form-control" placeholder="Credit Card Company" required></td>            
        </tr>                        
    </tbody>
    
</table>


<table class="table table-striped table-bordered add_setting_step cc_modem_models">
    <thead>
        <tr><th>Company</th><th>Model</th><th></th></tr>
    </thead>

    <tbody>
        <tr>
            <td><input name="company" class="form-control" placeholder="Company" required></td>
            <td><input name="model" class="form-control" placeholder="Model" required></td>            
        </tr>                        
    </tbody>
    
</table>

<table class="table table-striped table-bordered add_setting_step cc_reader_models">
    <thead>
        <tr><th>Company</th><th>Model</th><th></th></tr>
    </thead>

    <tbody>

        <tr>
            <td><input name="company" class="form-control" placeholder="Company" required></td>
            <td><input name="model" class="form-control" placeholder="Model" required></td>           
        </tr>                        
    </tbody>
    
</table>

 <table class="table table-striped table-bordered add_setting_step cc_antennas">
    <thead>
        <tr><th>Name</th><th>Company</th><th>Model</th><th></th></tr>
    </thead>

    <tbody>
        <tr>                           
            <td><input name="cc_antenna_name" class="form-control" placeholder="Name" required></td>
            <td><input name="cc_antenna_company" class="form-control" placeholder="Company" required></td>
            <td><input name="cc_antenna_model" class="form-control" placeholder="Model" required></td>
        </tr>
    </tbody>
    
</table>


<table class="table table-striped table-bordered add_setting_step bill_validator_models">
    <thead>
        <tr><th>Company</th><th>Model</th><th>Type</th><th></th></tr>
    </thead>

    <tbody>

        <tr>                         
            <td><input name="company" class="form-control" placeholder="Company" required></td>
            <td><input name="model" class="form-control" placeholder="Model" required></td>
            <td>
                <select name="type" class="form-control" required>
                    <option value="">Please Select Type</option>
                    <option value="MDB">MDB</option>
                    <option value="Pulse">Pulse</option>
                </select>
            </td>           
        </tr> 
    </tbody>
    
</table>

<table class="table table-striped table-bordered add_setting_step vault_models">
    <thead>
        <tr><th>Company</th><th>Model</th><th>Lockable</th><th></th></tr>
    </thead>

    <tbody>

        <tr>                           
            <td><input name="company" class="form-control" placeholder="Company" required></td>
            <td><input name="model" class="form-control" placeholder="Model" required></td>
            <td>
                <select name="lockable" class="form-control" required>
                    <option value="">Please Select</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </td>
        </tr>                       
    </tbody>
    
</table>

<table class="table table-striped table-bordered add_setting_step vault_keys">
    <thead>
        <tr><th>Vault Keys</th><th></th></tr>
    </thead>

    <tbody>
        <tr>                             
            <td><input name="vault_key" class="form-control" placeholder="Vault Key" required></td> 
        </tr>                
    </tbody>
    
</table>


<table class="table table-striped table-bordered add_setting_step hopper_models">
    <thead>
        <tr><th>Company</th><th>Model</th><th>Size</th><th>Power</th><th></th></tr>
    </thead>

    <tbody>

        <tr>                            
            <td><input name="company" class="form-control" placeholder="Company" required></td>
            <td><input name="model" class="form-control" placeholder="Model" required></td>
            <td>
                <select name="size" class="form-control" required>
                    <option value="">Please Select</option>
                    <option value="28mm">28mm</option>
                    <option value="39mm">39mm</option>
                </select>
            </td>
             <td>
                <select name="power" class="form-control" required>
                    <option value="">Please Select</option>
                    <option value="24 V">24 V</option>
                    <option value="110 V">110 V</option>
                </select>
            </td>            
        </tr>                

        
    </tbody>
    
</table>