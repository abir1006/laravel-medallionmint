<script type="text/javascript">

	$(document).ready(function(){

		Dropzone.options.machinePhotoUploadForm = {
            paramName: "photo", // The name that will be used to transfer the file
            maxFilesize: 1, // MB
            acceptedFiles:'image/*',
            success: function(file,res){
                
                //console.log(res);

                $('.machine_photos').append(
                    '<div class="photo thumbnail">'
                        +'<img src="/upload/machine_photo/'+res.thumb+'">'                
                        +'<a title="Delete Photo" class="btn btn-xs btn-danger pull-right photo_del_btn" data-url="/admin/machines/'+res.machine_id+'/delPhoto/'+res.photo_id+'" data-token="{{csrf_token()}}"><i class="glyphicon glyphicon-trash"></i></a>'
                        +'<a title="Mark as default photo" class="btn btn-xs btn-info pull-right photo_set_default_btn" data-url="/admin/machines/'+res.machine_id+'/setDefaultPhoto/'+res.photo_id+'" data-token="{{csrf_token()}}"><i class="glyphicon glyphicon-check"></i></a>'
                    +'</div>'
                );

            },            
            // this error section is optional, to display server side response msg
            error: function(file, response) {
                if($.type(response) === "string")
                    var message = response; //dropzone sends it's own error messages in string
                else
                    var message = response.photo;

                file.previewElement.classList.add("dz-error");

                _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                _results = [];
                
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i];
                    _results.push(node.textContent = message);
                }

                return _results;
          
            }   
        };

        $('.machine_photos').on('click','.photo_del_btn',function(e){

            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this photo ? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Photo',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){
                            target.closest('.photo').css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                //$('.nav-sidebar .badge.users').html(parseInt($('.nav-sidebar .badge.users').html())-1);

                                $(target).closest('.photo').fadeOut(500,function(){
                                    $(target).closest('.photo').remove();
                                });

                                alertify.success('Photo has been deleted');

                            }else{

                                $(target).closest('.photo').css('opacity','1');
                                alertify.error(res);
                            }


                        },
                        error:function(request,status,error){
                            //request.responseText
                            alertify.alert(error);
                            $(target).closest('.photo').css('opacity','1');
                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });

        $('.machine_photos').on('click','.photo_set_default_btn',function(e){

            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');          

            $.ajax({

                url: url,
                type: 'POST',
                data: {_token: token},
                success: function (res) {

                    if(res=='success'){
                        target.closest('.photo').addClass('default').siblings().removeClass('default');
                    }else{
                        alertify.error('Failed to set default photo');
                    }


                },
                error:function(request,status,error){
                    //request.responseText
                    alertify.alert(error);
                }
            });


              
        });
        
	});
</script>