<select name="{{$name}}" id="{{$name}}" class="form-control {{$setting}}">


    <option value="">Please Select</option>
    @if( isset($machine_settings[$setting]) )
        @foreach($machine_settings[$setting] as $item)             
           
            @if(old($name))
                @if(old($name)==$item['id'])
                    <option value="{{$item['id']}}" selected>
                @else
                    <option value="{{$item['id']}}">
                @endif
            @elseif(isset($machine))
                @if($machine->$name == $item['id'])
                    <option value="{{$item['id']}}" selected>
                @else
                    <option value="{{$item['id']}}">
                @endif
            @else
                <option value="{{$item['id']}}">

            @endif
           
            
                <?php $sl=0; ?>
                @foreach(json_decode($item['data']) as $key=>$value)
                    {{($sl++>0?' | ':'').$value}}
                @endforeach
            </option>
            
        @endforeach
    @endif
</select>