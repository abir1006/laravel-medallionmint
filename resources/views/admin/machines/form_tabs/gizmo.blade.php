<div class="row">
    
    <div class="col-sm-5">

        <div class="row form-group @if ($errors->has('gizmo_id')) has-error @endif">

            <div class="col-sm-4">
                {!!Form::label('gizmo_id','Gizmo',array('class'=>'form-control-static'))!!}
            </div>

            <div class="col-sm-6">
                {!!Form::select('gizmo_id',array(null=>'Please Select')+$gizmos,null,array('class'=>'form-control searchable_select')) !!}
                @if ($errors->has('gizmo_id')) <p class="help-block">{{ $errors->first('gizmo_id') }}</p> @endif
            </div>

        </div>

        <div class="row form-group" data-setting="gizmo_fws">

            <div class="col-sm-4">
                {!!Form::label('gizmo_fws','Gizmo FW',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'gizmo_fws','name'=>'gizmo_fws'])
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>

        </div>

        <div class="row form-group @if ($errors->has('meter_box_id')) has-error @endif">

            <div class="col-sm-4">
                {!!Form::label('meter_box_id','Meter Box',array('class'=>'form-control-static'))!!}
            </div>

            <div class="col-sm-6">
                {!!Form::select('meter_box_id',array(null=>'Please Select')+$meter_boxes,null,array('class'=>'form-control searchable_select')) !!}
                @if ($errors->has('meter_box_id')) <p class="help-block">{{ $errors->first('meter_box_id') }}</p> @endif
            </div>

        </div>

        

    </div>    
   

    <div class="col-sm-3">
        <img class="thumbnail" data-src="holder.js/100%x240" alt="">
    </div>
</div>