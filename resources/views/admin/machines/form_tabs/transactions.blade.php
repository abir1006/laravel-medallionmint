<h3>Transactions</h3>

<table class="table table-striped table-bordered table_sortable">

    <thead>

        <tr>

            <th>SL</th>
            <th>Coin</th>
            <th>Reference</th>
            <th>Type</th>
            <th>Card Number</th>
            <th>Amount</th>
            <th>Time</th>
        </tr>
    </thead>

    <tbody>
        @foreach($machine->transactions as $index=>$transaction)
            <tr>
                <td>{{$index+1}}</td>
                <td>
                    @if($transaction->coin)
                        {{(int) $transaction->item_number.' '.$transaction->coin->name}}
                    @else
                        {{$transaction->item_number}}
                    @endif
                </td>

                <td>{{$transaction->transaction_ref}}</td>
                <td>
                    @if($transaction->transaction_type == 'C')
                        <i class="fa fa-money text-primary" aria-hidden="true" title="Cash"></i>
                    @elseif($transaction->transaction_type == 'R')
                        <i class="fa fa-credit-card text-danger" aria-hidden="true" title="Credit Card "></i>
                    @elseif($transaction->transaction_type == 'P')
                        <i class="fa fa-user-md text-muted" aria-hidden="true" title="Test Pass"></i>
                    @endif
                </td>
                <td>{{$transaction->card_num}}</td>
                <td>{{$transaction->total_amount}}</td>
                <td>{{date('m-d-Y h:i A',strtotime($transaction->transaction_time))}}</td>
            </tr>

        @endforeach
    </tbody>
</table>

<p class="text-muted"><i class="glyphicon glyphicon-info-sign"></i> Displaying most recent 100 transactions</p>