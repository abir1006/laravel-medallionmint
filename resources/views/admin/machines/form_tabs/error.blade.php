@if($machine->deviceAlerts->where('alert_type','error')->count())
    <h3>Error</h3>
    <table class="table table-striped table-bordered first-last-auto">

        <tr>
            <th>SL</th>
            <th>Error</th>
            <th>Detected At</th>
            <th>Reported At</th>
        </tr>
        @foreach($machine->deviceAlerts->where('alert_type','error') as $index=>$error)
            <tr>
                <td>{{$index+1}}</td>
                <td>{{$error->alert}}</td>
                <td>{{date('m-d-Y h:i A',strtotime($error->detected_at))}}</td>
                <td>{{date('m-d-Y h:i A',strtotime($error->reported_at))}}</td>
            </tr>
        @endforeach

    </table>
@else
    <p class="alert alert-success">No device error found!</p>
@endif