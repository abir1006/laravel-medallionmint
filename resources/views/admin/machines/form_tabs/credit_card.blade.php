<div class="row form-group">
	<div class="col-sm-2">
		{!!Form::label('has_cc','Has Credit Card',array('class'=>'form-control-static'))!!}
	</div>
	<div class="col-sm-3">
        <div class="form-group">
            <div class="make-switch has_cc_switch" data-on="primary" data-off="info" data-on-label="Yes" data-off-label="No">
                {!!Form::checkbox('has_cc',1,false,array('class'=>'checkbox')) !!}
            </div>
        </div>
	</div>
</div>

<div class="cc_details">

    <div class="row form-group" data-setting="cc_companies">
        
        <div class="col-sm-2">
            {!!Form::label('cc_companies','CC Company',array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-3">
            @include('admin.machines.machine_setting_fetch',['setting'=>'cc_companies','name'=>'cc_companies'])             
        </div>
        <div class="col-sm-1 form-control-static">
            <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
        </div>
        
    </div>

    <div class="row form-group" data-setting="cc_antennas">
        
        <div class="col-sm-2">
            {!!Form::label('cc_antennas','CC Antenna',array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-3">
            @include('admin.machines.machine_setting_fetch',['setting'=>'cc_antennas','name'=>'cc_antennas'])             
        </div>
        <div class="col-sm-1 form-control-static">
            <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
        </div>
        
    </div>

    <div class="row form-group @if ($errors->has('cc_modem_id')) has-error @endif">

        <div class="col-sm-2">
            {!!Form::label('cc_modem_id','CC Modem',array('class'=>'form-control-static'))!!}
        </div>

        <div class="col-sm-3">
            {!!Form::select('cc_modem_id',array(null=>'Please Select')+$cc_modems,null,array('class'=>'form-control searchable_select')) !!}
            @if ($errors->has('cc_modem_id')) <p class="help-block">{{ $errors->first('cc_modem_id') }}</p> @endif
        </div>

    </div>

    <div class="row form-group @if ($errors->has('cc_reader_id')) has-error @endif">

        <div class="col-sm-2">
            {!!Form::label('cc_reader_id','CC Reader',array('class'=>'form-control-static'))!!}
        </div>

        <div class="col-sm-3">
            {!!Form::select('cc_reader_id',array(null=>'Please Select')+$cc_readers,null,array('class'=>'form-control searchable_select')) !!}
            @if ($errors->has('cc_reader_id')) <p class="help-block">{{ $errors->first('cc_reader_id') }}</p> @endif
        </div>

    </div>


</div> <!-- /.cc_details -->