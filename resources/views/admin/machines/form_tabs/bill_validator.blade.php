<div class="row">
    
    <div class="col-sm-5">


        <div class="row form-group @if ($errors->has('bill_validator_id')) has-error @endif">

            <div class="col-sm-4">
                {!!Form::label('bill_validator_id','Bill Validator',array('class'=>'form-control-static'))!!}
            </div>

            <div class="col-sm-6">
                {!!Form::select('bill_validator_id',array(null=>'Please Select')+$bill_validators,null,array('class'=>'form-control searchable_select')) !!}
                @if ($errors->has('bill_validator_id')) <p class="help-block">{{ $errors->first('bill_validator_id') }}</p> @endif
            </div>

        </div>

        <div class="row form-group @if ($errors->has('vault_id')) has-error @endif">

            <div class="col-sm-4">
                {!!Form::label('vault_id','Vault',array('class'=>'form-control-static'))!!}
            </div>

            <div class="col-sm-6">
                {!!Form::select('vault_id',array(null=>'Please Select')+$vaults,null,array('class'=>'form-control searchable_select')) !!}
                @if ($errors->has('vault_id')) <p class="help-block">{{ $errors->first('vault_id') }}</p> @endif
            </div>

        </div>


        <div class="row form-group" data-setting="vault_keys">
            
            <div class="col-sm-4">
                {!!Form::label('vault_keys','Vault Key',array('class'=>'form-control-static'))!!}
            </div>
            <div class="col-sm-6">
                @include('admin.machines.machine_setting_fetch',['setting'=>'vault_keys','name'=>'vault_keys'])             
            </div>
            <div class="col-sm-2 form-control-static">
                <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
            </div>
            
        </div>


    </div><!-- col-sm-5 -->
</div> <!-- /row -->