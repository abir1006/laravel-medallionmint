@for($i=1;$i<=8;$i++)


    <div class="row form-group hopper_models_wrapper hopper_models_wrapper_{{$i}} @if ($errors->has('hopper_'.$i.'_id')) has-error @endif">

        <div class="col-sm-2">
            {!!Form::label('hopper_'.$i.'_id','Hopper '.$i,array('class'=>'form-control-static'))!!}
        </div>

        <div class="col-sm-3">
            {!!Form::select('hopper_'.$i.'_id',array(null=>'Please Select')+$hoppers,null,array('class'=>'form-control searchable_select')) !!}
            @if ($errors->has('hopper_'.$i.'_id')) <p class="help-block">{{ $errors->first('hopper_'.$i.'_id') }}</p> @endif
        </div>

    </div>
@endfor


{{--
@for($i=1;$i<=8;$i++)
    <div class="row form-group hopper_models_wrapper hopper_models_wrapper_{{$i}}" data-setting="hopper_models">

        <div class="col-sm-2">
            {!!Form::label('hopper_models_'.$i,'Hopper Model '.$i,array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-3">
            @include('admin.machines.machine_setting_fetch',['setting'=>'hopper_models','name'=>'hopper_models_'.$i])
        </div>

        <div class="col-sm-1 form-control-static">
            <a href="#" class="btn btn-xs btn-info btn_add_setting"><i class="glyphicon glyphicon-plus"></i></a>
        </div>

        <div class="col-sm-2">
            {!!Form::label('hopper_serial_'.$i,'Hopper Serial '.$i,array('class'=>'form-control-static'))!!}
        </div>
        <div class="col-sm-2">
            {!!Form::text('hopper_serial_'.$i,null,array('class'=>'form-control')) !!}
        </div>

    </div>
@endfor--}}
