<h3>Machine Health</h3>

    <div class="row">
        @if($machine->deviceHealth)

            <div class="col-sm-4">
                <table class="table table-striped table-condensed table-bordered first-last-auto">

                    <tr><td>Asset #</td><td>{{$machine->deviceHealth->asset}}</td></tr>
                    <tr><td>Status</td><td>{{$machine->deviceHealth->status}}</td></tr>
                    <tr><td>Signal Strength</td><td>{{$machine->deviceHealth->signal_strength}}</td></tr>
                    <tr><td>FirmWare Version</td><td>{{$machine->deviceHealth->firmware_version}}</td></tr>
                    <tr><td>Communication Method</td><td>{{$machine->deviceHealth->communication_method}}</td></tr>

                </table>

            </div>


            <div class="col-sm-4">
                <table class="table table-striped table-condensed table-bordered first-last-auto">

                    <tr>
                        <td><i class="glyphicon glyphicon-transfer"></i> Last Call In</td>
                        <td>
                            @if(strtotime($machine->deviceHealth->date_of_last_call_in) < strtotime('-10 years') )
                                <span class="text-muted">No call in yet!</span>
                            @else
                                {{$machine->deviceHealth->date_of_last_call_in->diffForHumans()}}<br>
                                <span class="text-muted">{{date('m-d-Y h:i A',strtotime($machine->deviceHealth->date_of_last_call_in))}}</span>
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td><i class="glyphicon glyphicon-credit-card"></i> Last Credit Transaction</td>
                        <td>
                            @if(strtotime($machine->deviceHealth->date_of_last_credit_tran) < strtotime('-10 years') )
                                <span class="text-muted">No credit transaction yet!</span>
                            @else
                                {{$machine->deviceHealth->date_of_last_credit_tran->diffForHumans()}}<br>
                                <span class="text-muted">{{date('m-d-Y h:i A',strtotime($machine->deviceHealth->date_of_last_credit_tran))}}</span>
                            @endif
                        </td>
                    </tr>

                    <tr>
                        <td><i class="glyphicon glyphicon-usd"></i> Last Cash Transaction</td>
                        <td>
                            @if(strtotime($machine->deviceHealth->date_of_last_cash_tran) < strtotime('-10 years') )
                                <span class="text-muted">No cash transaction yet!</span>
                            @else
                                {{$machine->deviceHealth->date_of_last_cash_tran->diffForHumans()}} <br>
                                <span class="text-muted">{{date('m-d-Y h:i A',strtotime($machine->deviceHealth->date_of_last_cash_tran))}}</span>

                            @endif


                        </td>
                    </tr>

                    <tr>
                        <td>Earliest Logged</td>
                        <td>
                            @if($machine->first_transaction)

                                {{$machine->first_transaction->transaction_time->diffForHumans()}} <br>
                                <span class="text-muted">{{date('m-d-Y h:i A',strtotime($machine->first_transaction->transaction_time))}}</span>

                            @else
                                <span class="text-muted">No Transaction Yet Today!</span>
                            @endif


                        </td>
                    </tr>

                    <tr>
                        <td>Latest Logged</td>
                        <td>
                            @if($machine->last_transaction)

                                {{$machine->last_transaction->transaction_time->diffForHumans()}} <br>
                                <span class="text-muted">{{date('m-d-Y h:i A',strtotime($machine->last_transaction->transaction_time))}}</span>

                            @else
                                <span class="text-muted">No Transaction Yet Today!</span>
                            @endif


                        </td>
                    </tr>

                </table>

            </div>

            <div class="col-sm-4">

                <table class="table table-striped table-condensed table-bordered first-last-auto">
                    <tr>
                        <th>#</th>
                        <th>Coin</th>
                        <th>Last Transaction At</th>
                    </tr>
                    @for($i=1;$i<=$machine->coins;$i++)
                        <tr>
                            <td>{{$i}}</td>
                            <td>
                                @if($machine->{'coin_'.$i})
                                    {{$machine->{'coin_'.$i}->name}}
                                @endif
                            </td>
                            <td>
                                @if(strtotime($machine->deviceHealth->{'hopper_'.$i.'_last_transaction_date'} ) > strtotime('-40 years') )

                                    {{date('m-d-Y h:i A',strtotime($machine->deviceHealth->{'hopper_'.$i.'_last_transaction_date'}))}}&nbsp;

                                @endif

                            </td>
                        </tr>
                    @endfor
                </table>
            </div>

        @else

            <div class="col-sm-12">
                <p class="alert alert-warning">No Device Health Data Found for this machine !!</p>
            </div>
        @endif



    </div>


