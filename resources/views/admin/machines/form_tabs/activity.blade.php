@if($machine->deviceAlerts->where('alert_type','normal')->count())
    <h3>Activity</h3>
    <table class="table table-striped table-bordered first-last-auto">

        <tr>
            <th>SL</th>
            <th>Activity</th>
            <th>Detected At</th>
            <th>Reported At</th>
        </tr>

        @foreach($machine->deviceAlerts->where('alert_type','normal') as $index=>$activity)
            <tr>
                <td>{{$index+1}}</td>
                <td>{{$activity->alert}}</td>
                <td>{{date('m-d-Y h:i A',strtotime($activity->detected_at))}}</td>
                <td>{{date('m-d-Y h:i A',strtotime($activity->reported_at))}}</td>
            </tr>
        @endforeach

    </table>
@else
    <p class="alert alert-info">No device activity found!</p>
@endif