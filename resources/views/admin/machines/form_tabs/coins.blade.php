<div class="row form-group">
	<div class="col-sm-8">
		<div class="col-sm-4">
			{!!Form::label('coins','Select Number of Coins')!!}
		</div>
		<div class="col-sm-4">
			{!!Form::selectRange('coins',1,8,null,array('class'=>'form-control'))!!}
		</div>
	</div>

	<div class="col-sm-4 ">
		<p class="alert alert-warning location_select_warning">
			<i class="glyphicon glyphicon-warning-sign"></i> 
			Please  <strong>Select Location</strong> at <strong>Details</strong> tab first,
			you can choose coin only from your selected location
		</p>

		<p class="alert alert-warning location_has_coins_warning">
			<i class="glyphicon glyphicon-warning-sign"></i> 
			Your selected location doesn'nt have any coins yet, Please add coins at this location then try again
			<br><a href="#" class="no_coin_refresh_link">Reload (i have added some coins)</a>
		</p>
	</div>
</div>


	@for($i=1;$i<=8;$i++)
	<div class="row form-group">
		<div class="col-sm-8 clearfix coin_wrapper coin_wrapper_{{$i}}">
			<div class="col-sm-4">
				{!!Form::label('coin_'.$i.'_id','Coin '.$i)!!}
			</div>
			<div class="col-sm-4">
				{!!Form::select('coin_'.$i.'_id',array(null=>'Please Select'),null,array('class'=>'coin form-control'))!!}
			</div>

			<!-- <div class="coin_thumb pull-left front"></div>
			<div class="coin_thumb pull-left back"></div> -->

		</div>
	</div>
	@endfor