<div class="row form-group">
    <div class="col-sm-2 col-sm-offset-3"><label>Starting Meter</label></div>
    <div class="col-sm-2"><label>Current Meter</label></div>
    <div class="col-sm-2"><label>Calculated Meter</label></div>
    <div class="col-sm-3"><label>Calculated Inventory</label></div>
</div>

<div class="row form-group">

    <div class="col-sm-3">
        <label class="form-control-static" >Bill Validator Meter</label>
    </div>

    <div class="col-sm-2">
        {!!Form::text('starting_meter_1',null,array('class'=>'form-control')) !!}
    </div>

    <div class="col-sm-2">
        {!!Form::text('current_meter_1',null,array('class'=>'form-control')) !!}
    </div>

    <div class="col-sm-2">
        {!!Form::text('calculated_meter_1',null,array('class'=>'form-control')) !!}
    </div>

    <div class="col-sm-3">   </div>

</div>

<div class="row form-group">

    <div class="col-sm-3">
        <label class="form-control-static" >Credit Card Reader Meter</label>
    </div>

    <div class="col-sm-2">
        {!!Form::text('starting_meter_2',null,array('class'=>'form-control')) !!}
    </div>

    <div class="col-sm-2">
        {!!Form::text('current_meter_2',null,array('class'=>'form-control')) !!}
    </div>

    <div class="col-sm-2">
        {!!Form::text('calculated_meter_2',null,array('class'=>'form-control')) !!}
    </div>

    <div class="col-sm-3">   </div>

</div>

@for($i=3;$i<=10;$i++)
    <div class="row form-group meter_wrapper meter_wrapper_{{$i}}">

        <div class="col-sm-3">

            <label class="form-control-static" >Meter {{$i}}</label>
        </div>

        <div class="col-sm-2">
            {!!Form::text('starting_meter_'.$i,null,array('class'=>'form-control')) !!}
        </div>

        <div class="col-sm-2">
            {!!Form::text('current_meter_'.$i,null,array('class'=>'form-control')) !!}
        </div>

        <div class="col-sm-2">
            {!!Form::text('calculated_meter_'.$i,null,array('class'=>'form-control')) !!}
        </div>

        <div class="col-sm-3">
            {!!Form::text('calculated_inventory_'.($i-2),null,array('class'=>'form-control')) !!}
        </div>
                
    </div>
@endfor