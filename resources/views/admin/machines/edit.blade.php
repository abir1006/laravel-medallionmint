@extends('admin.layout')

@section('title','Edit '.$machine->name )

@section('main')
    <h1 class="page-header">Edit {{$machine->name}}, {{$machine->location->name}}</h1>

    @include('partial.form_error')

    {!! Form::model($machine,['method'=>'PATCH','url' => ['admin/machines',$machine->id ],'class'=>'machine_form']) !!}
        

        @include('admin.machines.form_edit')
    
        <div class="form-group">
            {!! Form::submit('Update Machine',['class'=>'btn btn-primary']) !!}
        </div>
        

    {!!Form::close()!!}

    @include('admin.machines.machine_setting_add')
    
@stop

@section('script')
   @include('admin.machines.script')
@stop
