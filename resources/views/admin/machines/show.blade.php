@extends('admin.layout')
@section('title', $machine->name)

@section('stylesheet')
    <link rel="stylesheet" type="text/css" href="/lib/dropzone/dropzone.css">
@stop

@section('main')
    <h1 class="page-header">{{$machine->name}}, {{$machine->location->name}}
        <a href="{{ URL::route('admin.machines.edit',$machine->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>

    </h1>

    <div role="tabpanel">
    
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>            
            <li role="presentation"><a href="#photos" aria-controls="photos" role="tab" data-toggle="tab">Photos</a></li>
            
        </ul>

        <div class="tab-content">
            
            <div role="tabpanel" class="tab-pane fade in active" id="details">
                
            </div>
            
            <div role="tabpanel" class="tab-pane fade" id="photos">
                <div class="clearfix machine_photos">
                @foreach($machine->photos as $photo)
                    <div class="photo thumbnail {{($machine->default_photo_id == $photo->id) ? 'default' : ''}}">
                        <img src="/upload/machine_photo/200-{{$photo->name}}">                
                        <a title="Delete Photo" class="btn btn-xs btn-danger pull-right photo_del_btn" data-url="{{url('admin/machines/'.$machine->id.'/delPhoto/'.$photo->id)}}" data-token="{{csrf_token()}}"><i class="glyphicon glyphicon-trash"></i></a>
                        <a title="Mark as default photo" class="btn btn-xs btn-info pull-right photo_set_default_btn" data-url="{{url('admin/machines/'.$machine->id.'/setDefaultPhoto/'.$photo->id)}}" data-token="{{csrf_token()}}"><i class="glyphicon glyphicon-check"></i></a>
                    </div>
                @endforeach
                </div>

                <form id="machinePhotoUploadForm" class="dropzone" action="/admin/machines/{{$machine->id}}/addPhoto" method="POST">
                    {{csrf_field()}}
                </form>
            </div>             

        </div>
        
    </div>


    
@stop

@section('script')
    <script type="text/javascript" src="/lib/dropzone/dropzone.js"></script>
   @include('admin.machines.script_photo')
@stop
