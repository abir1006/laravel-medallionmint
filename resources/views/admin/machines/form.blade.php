<div role="tabpanel">
    
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
        <li role="presentation"><a href="#coins" aria-controls="coins" role="tab" data-toggle="tab">Coins</a></li>
        <li role="presentation"><a href="#gizmo" aria-controls="gizmo" role="tab" data-toggle="tab">Gizmo</a></li>
        <li role="presentation"><a href="#credit_card" aria-controls="credit_card" role="tab" data-toggle="tab">Credit Card</a></li>
        <li role="presentation"><a href="#bill_validator" aria-controls="bill_validator" role="tab" data-toggle="tab">Bill Validator</a></li>
        <li role="presentation"><a href="#hoppers" aria-controls="hoppers" role="tab" data-toggle="tab">Hoppers</a></li>
        <li role="presentation"><a href="#meters" aria-controls="meters" role="tab" data-toggle="tab">Meters</a></li>
        <li role="presentation"><a href="#shipped" aria-controls="shipped" role="tab" data-toggle="tab">Shipped</a></li>

    </ul>

    <div class="tab-content">

        <div role="tabpanel" class="tab-pane fade in active" id="details">
            @include('admin.machines.form_tabs.details')
        </div>
        
        <div role="tabpanel" class="tab-pane fade" id="coins">
            @include('admin.machines.form_tabs.coins')
        </div>

        <div role="tabpanel" class="tab-pane fade" id="gizmo">
            @include('admin.machines.form_tabs.gizmo')
        </div>

        <div role="tabpanel" class="tab-pane fade" id="credit_card">
            @include('admin.machines.form_tabs.credit_card')
        </div>

        <div role="tabpanel" class="tab-pane fade" id="bill_validator">
            @include('admin.machines.form_tabs.bill_validator')
        </div>

        <div role="tabpanel" class="tab-pane fade" id="hoppers">
            @include('admin.machines.form_tabs.hoppers')
        </div>

        <div role="tabpanel" class="tab-pane fade" id="meters">
            @include('admin.machines.form_tabs.meters')
        </div>

        <div role="tabpanel" class="tab-pane fade" id="shipped">
            @include('admin.machines.form_tabs.shipped')
        </div>

    </div>
    
</div>

