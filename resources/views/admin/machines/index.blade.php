@extends('admin.layout')
@section('title', 'Machines')

@section('main')
   <h1 class="page-header">Machines
	   <a href="{{URL::to('admin/machines/create')}}" class="btn btn-sm btn-primary pull-right">
		   <i class="glyphicon glyphicon-plus"></i> Add New Machine
	   </a>
   </h1>
   @if(!count($machines))
       
     <p class="alert alert-warning">
         No machines found! 
         <a href="{{ url('admin/machines/create')}}">Add new machine</a>
     </p>
       
   @else

    {{--<p class="alert alert-danger">{{$machines->where('cc_modem_serial','')->count() }} Machines doesn't have CC Modem Serial</p>--}}

   	<table class="table table-striped table-bordered table_sortable">
        <thead>
            <tr>
                <th>SL</th>
                <th>Machines</th>
                <th></th>
                <th>Location</th>
                <th>Device Serial</th>
                <th>Action</th>
            </tr>
        </thead>
	   	@foreach($machines as $index=>$machine)
        <tr>
            <td>{{$index+1}}</td>


            <td>
                {{--
                @if($machine->photo())
                    {!!Html::image('/upload/machine_photo/75-'.$machine->photo()->name,null,['class'=>'thumbnail machine_thumb pull-left'])!!}
                @else
                    {!!Html::image('/images/no_image_75x50.png',null,['class'=>'thumbnail machine_thumb pull-left']) !!}
                @endif--}}

                <a href="{{ URL::route('admin.machines.show',$machine->id) }}">{{ $machine->name }}</a>

            </td>

            <td>
                @if($machine->active)
                    <i class="glyphicon glyphicon-check text-success"></i>
                @else
                    <i class="glyphicon glyphicon-unchecked text-muted"></i>
                @endif

            </td>

            <td>
                <a href="{{ URL::route('admin.locations.show',$machine->location->id) }}">{{ $machine->location->name }}</a>
            </td>

            <td>{{$machine->cc_modem ? $machine->cc_modem->serial : ''}}</td>

            <td>
                <a href="{{ URL::route('admin.machines.edit',$machine->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                <a title="delete" data-url="{{url('admin/machines/'.$machine->id)}}" data-token="{{csrf_token()}}" class="admin_machine_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>
            </td>
        </tr>
	   	@endforeach
   	</table>

   @endif

@stop

@section('script')
   @include('admin.machines.script')
@stop
