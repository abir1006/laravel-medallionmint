<script type="text/javascript">
	$(document).ready(function(){


		/************ has_cc **************/	
		if( $('#has_cc').prop('checked')==false){
//			if ($('[name="has_seasons"]').is(':checked')){
			$('.cc_details').hide();
		}

		$('.has_cc_switch').on('switch-change', function () {
			$('.cc_details').slideToggle();
		});
		/*****************************************/

		function change_coin_number(coins){
			$('.coin_wrapper,.hopper_models_wrapper,.meter_wrapper,.shipped_meter_wrapper').hide();


			for(var i =1;i<=coins;i++){
				$('.coin_wrapper_'+i).show();
				$('.hopper_models_wrapper_'+i).show();
				$('.meter_wrapper_'+(i+2) ).show();
				$('.shipped_meter_wrapper_'+i).show();
			}
		}

		function set_coins(location_id){
			
			if(location_id){
				$('.location_select_warning').hide();
			}else{
				$('.location_select_warning').show();

				return;
			}
			
			$.getJSON("{{url('admin/get_coins')}}"+'/'+location_id, null, function(data) {			
			    
			    if(data.length){			    	
			       	$('.location_has_coins_warning').hide();
			    }else{
			       	alertify.alert('Your selected location doesn\'nt have any coins yet, Please add coins at this location then try again');
			       	$('.location_has_coins_warning').show();
			    }

			    $(".coin option").remove();
			    
			    $(".coin").append( $("<option value>Please Select</option>") );
			    $.each(data, function(index, item) { 			        
			        $(".coin").append( $("<option ></option>").text(item.name).val(item.id)  );
			    });

			    @for($i=1;$i<=8;$i++)
			    	
			    	@if(old('coin_'.$i))
			    		$('#coin_'+{{$i}}+'_id').val( {{ old('coin_'.$i.'_id') }} );
			    	@elseif(isset($machine))
			    		$('#coin_'+{{$i}}+'_id').val( {{ $machine->{'coin_'.$i.'_id'} }} );
			    	@endif
			    				    	
			    @endfor

			});
		}


		
		change_coin_number($('#coins :selected').val());		
		set_coins($('#location_id').val());
		
		$('#coins').change(function(){					
			change_coin_number($(':selected',this).val());			
		});

		$('#location_id').change(function(){
			var location_id = $('#location_id').val();			
			set_coins(location_id);
		});

		$('.no_coin_refresh_link').click(function(e){
			e.preventDefault();
			set_coins($('#location_id').val());
		});


		/*****************************************************************************************/
		var working_setting_item;

		$('.btn_add_setting').click(function(e){
			e.preventDefault();
			var setting = $(this).closest('.row').data('setting');
			working_setting_item = $(this).closest('.row').find('select');

			$('.modal.add_setting .modal-title').html('Add new '+setting.replace('_',' '));
			
			var content = $('.add_setting_step.'+setting).clone().show().removeClass('add_setting_step');

			$(content).find('tbody tr').append('<td>'+
                '<input type="hidden" name="setting" value="'+setting+'">'+
                '<button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-plus"></i> Add New </button>'+
            '</td>');

			$('.modal.add_setting .modal-body').html(content);

			$('.modal.add_setting').modal('show');

		});

		$('.add_setting_form').submit(function(e){
			e.preventDefault();

			$('.btn_add_setting').attr('disabled','disabled');

			var url = $(this).attr('action');
            var data = $(this).serializeArray();
            
            $.ajax({
                url: url,
                type: 'POST',
                data: data,                
                success:function(res){
                	var new_opt_text = '';
                    var fields = $.parseJSON(res.data);
                    var sl = 0;                   
                    $.each(fields,function(i){   
                    	new_opt_text+= (sl++>0?' | ':'')+fields[i]                    	
                    }); 

                    if( $(working_setting_item).hasClass('hopper_models')){
                    	$('select.hopper_models').append('<option value="'+res.id+'">'+new_opt_text+'</option>');
                    	$(working_setting_item).val(res.id);
                    }else{
                    	$(working_setting_item).append('<option value="'+res.id+'" selected>'+new_opt_text+'</option>');
                    }
					
                    alertify.success('New machine setting item added');

                    $('.btn_add_setting').removeAttr('disabled');
                }
            });
			
			$('.modal.add_setting').modal('hide');
            
		});


		/**
		 * delete machine
		 */

		$('.admin_machine_del_btn').click(function(e){
			e.preventDefault();

			var target = $(this);
			var url = $(this).data('url');
			var token = $(this).data('token');




			alertify.confirm('Are you sure you want to delete this machine? This action cannot be undone.').setting({
				'title':'<span class="glyphicon glyphicon-trash"></span> Delete Machine',
				'labels':{ok:'DELETE'},
				'onok': function(){

					$.ajax({

						url: url,
						type: 'POST',
						data: {_method: 'delete', _token: token},

						beforeSend:function(){

							$(target).addClass('disabled');
							$(target).parent().parent().css('opacity','.5');

						},
						success: function (res) {

							if(res=='success'){

								$(target).parent().parent().fadeOut(500,function(){
									$(target).parent().parent().slideUp();
								});

								alertify.success('Machine has been deleted');
								$('.nav-sidebar .badge.machine').html(parseInt($('.nav-sidebar .badge.machine').html())-1);

							}else{

								$(target).removeClass('disabled');
								$(target).parent().parent().css('opacity','1');
								alertify.error('Failed to delete machine. <br>'+res);
							}


						}
					});


				},
				'oncancel':function(){


				}
			});
		});
			
		
	});
</script>