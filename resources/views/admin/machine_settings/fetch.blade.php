<?php 
    
    

    $settings = array_where($MachineSettings, function($key,$value) use($setting){                                                   

            return ($value['setting'] == $setting ? true : false);
        });                           

    $sl = 1;
?>                   

@foreach($settings as $item)
    <tr data-id="{{$item['id']}}">
        <td>{{$sl++}}</td>
        @foreach(json_decode($item['data']) as $key=>$value)
            <td>{{$value}}</td>
        @endforeach
        <td>
            <a class="setting_edit_btn btn btn-xs btn-info" href="#"><i class="glyphicon glyphicon-edit"></i> Edit</a> &nbsp;            
            <a class="setting_del_btn btn btn-xs btn-warning" href="#" data-url="{{url('admin/machine_settings/'.$item['id'])}}" data-token="{{csrf_token()}}"><i class="glyphicon glyphicon-trash"></i> </a> 

        </td>
    </tr>
@endforeach           