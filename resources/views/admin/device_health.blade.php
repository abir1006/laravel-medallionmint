@extends('admin.layout')

@section('title','Device Health')

@section('main')

    <h3 class="page-header">Device Health <small class="text-muted" title="m-d-Y">{{date('m-d-Y, h:i A')}}</small></h3>

    @if($all_device_health)

        <table class="table table-bordered table-striped device-health-table">

            <thead>

            <tr>
                <th>SL</th>
                <th>Location</th>
                <th>Machine</th>
                <th>Device</th>
                <th>Days</th>
                <th>Last Call-In</th>
                <th>Days</th>
                <th>Last Cash Tran</th>
                <th>Days</th>
                <th>Last Credit Tran</th>

            </tr>
            </thead>

            <tbody>
            <?php $now = \Carbon\Carbon::now(); ?>
            @foreach($all_device_health as $index=>$device_health)
                <tr>
                    <td>{{++$index}}</td>

                    @if($device_health->machine_id)

                        <td><a href="/admin/locations/{{$device_health->machine->location->id}}" >{{$device_health->machine->location->name}}</a></td>

                        <td data-machinestatus="{{$device_health->machine->active ? 'active':'inactive'}}" data-search="{{$device_health->machine->name}}">

                            <a href="/admin/machines/{{$device_health->machine->id}}/edit" >
                                <span class="{{$device_health->machine->active ? '':'text-muted'}}">{{$device_health->machine->name}}</span>
                            </a>

                        </td>

                    @else
                        <td>Not Found</td>
                        <td data-machinestatus="not-found">Not Found</td>
                    @endif

                    <td>{{$device_health->device}}</td>

                    <td>
                        @if(strtotime($device_health->date_of_last_call_in) < strtotime('-40 years') )
                            Never
                        @else
                            {{$device_health->date_of_last_call_in->diff($now)->days}}
                        @endif
                    </td>
                    <td data-order="{{$device_health->date_of_last_call_in}}" data-search="{{$device_health->date_of_last_call_in}}" title="{{date('m-d-Y h:i A',strtotime($device_health->date_of_last_call_in))}}">
                        @if(strtotime($device_health->date_of_last_call_in) < strtotime('-40 years') )
                            Never
                        @else
                            {{date('m-d-Y h:i A',strtotime($device_health->date_of_last_call_in))}}
                        @endif
                    </td>

                    <td>
                        @if(strtotime($device_health->date_of_last_cash_tran) < strtotime('-40 years') )
                            Never
                        @else
                            {{$device_health->date_of_last_cash_tran->diff($now)->days}}
                        @endif
                    </td>
                    <td data-order="{{$device_health->date_of_last_cash_tran}}" data-search="{{$device_health->date_of_last_cash_tran}}" title="{{date('m-d-Y h:i A',strtotime($device_health->date_of_last_cash_tran))}}">

                        @if(strtotime($device_health->date_of_last_cash_tran) < strtotime('-40 years') )
                            Never
                        @else
                            {{date('m-d-Y h:i A',strtotime($device_health->date_of_last_cash_tran))}}
                        @endif
                    </td>

                    <td>
                        @if(strtotime($device_health->date_of_last_credit_tran) < strtotime('-40 years') )
                            Never
                        @else
                            {{$device_health->date_of_last_credit_tran->diff($now)->days}}
                        @endif
                    </td>
                    <td data-order="{{$device_health->date_of_last_credit_tran}}" data-search="{{$device_health->date_of_last_credit_tran}}" title="{{date('m-d-Y h:i A',strtotime($device_health->date_of_last_credit_tran))}}">
                        @if(strtotime($device_health->date_of_last_credit_tran) < strtotime('-40 years') )
                            Never
                        @else
                            {{date('m-d-Y h:i A',strtotime($device_health->date_of_last_credit_tran))}}
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    @else

        <p class="alert alert-warning">No Device Health Data Found!</p>
    @endif
@stop

@section('script')
    <script type="text/javascript">

        $(document).ready(function(){


            var device_health_table = $('table.device-health-table').DataTable({
                aLengthMenu: [
                    [10,25, 50, 100, 200, -1],
                    [10,25, 50, 100, 200, "All"]
                ],
                iDisplayLength: -1,
                bAutoWidth: false,
                dom: "<'row'<'col-sm-2'l><'device-health-filter-wrapper col-sm-7'> <'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            });

            $(".device-health-filter-wrapper").html('Filter By '
                    +'<select class="machine-status form-control">' +
                    '<option value="all">All Machine</option>' +
                    '<option value="active" selected>Active Machine</option>' +
                    '<option value="inactive">In Active Machine</option>' +
                    '<option value="not-found">Not Found</option>' +
                    '</select> '+


                    '<select class="filter-by form-control">'
                    +'<option value="all">Please Select</option>'
                    +'<option value="5" selected>Last Call-In</option>'
                    +'<option value="7">Last Cash Transaction</option>'
                    +'<option value="9">Last Credit Transaction</option>'
                    +'</select> '

                    +'<select class="range form-control">'
                    +'<option value="not-within">Not With in</option>'
                    +'<option value="within">With in</option>'
                    +'</select> '

                    +'<select class="hours-diff form-control">'
                    +'<option value="all">Please Select</option>'
                    +'<option value="6">6 Hours</option>'
                    +'<option value="12">12 Hours</option>'
                    +'<option value="24">1 Day</option>'
                    +'<option value="48">2 Days</option>'
                    +'<option value="72" selected>3 Days</option>'
                    +'<option value="168">7 Days</option>'
                    +'</select>'
            );


            var machineStatus = $('.device-health-filter-wrapper select.machine-status').val();
            var filterBy = $('.device-health-filter-wrapper select.filter-by').val();
            var hoursDiff = $('.device-health-filter-wrapper select.hours-diff').val();
            var range = $('.device-health-filter-wrapper select.range').val();


            $.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {

                var dataMachineStatus = device_health_table
                        .row(dataIndex)         //get the row to evaluate
                        .nodes()                //extract the HTML - node() does not support to$
                        .to$()                  //get as jQuery object
                        .find('td[data-machinestatus]') //find column with data-machinestatus
                        .data('machinestatus');         //get the value of data-machinestatus


                if(machineStatus !== 'all'){
                    /*   if(machineStatus !== data[2]){
                     return false;
                     }   */

                    if(machineStatus !== dataMachineStatus){
                        return false;
                    }
                }

                if(filterBy === 'all' || hoursDiff === 'all' ){
                    return true;
                }

                var diff = moment('{{date('Y-m-d H:i:s')}}').diff(moment(data[filterBy]),'hours');


                if(range === 'within') {
                    if (diff <= hoursDiff) {

                        return true;
                    }

                }else if(range === 'not-within'){
                    if (diff > hoursDiff) {
                        return true;
                    }
                }


                return false;
            });

            $('.device-health-filter-wrapper select.machine-status,.device-health-filter-wrapper select.filter-by,.device-health-filter-wrapper select.hours-diff,.device-health-filter-wrapper select.range').on('change',function(){

                machineStatus = $('.device-health-filter-wrapper select.machine-status').val();

                filterBy = $('.device-health-filter-wrapper select.filter-by').val();
                hoursDiff = $('.device-health-filter-wrapper select.hours-diff').val();
                range = $('.device-health-filter-wrapper select.range').val();



                device_health_table.draw();
            });

            device_health_table.draw();

        });
    </script>
@stop