@extends('admin.layout')
@section('title','Gizmos')

@section('main')
    <h1 class="page-header">Gizmos
        <a href="{{route('admin.gizmos.create')}}" class="btn btn-sm btn-primary pull-right "> <i class="glyphicon glyphicon-plus"></i> Add New Gizmo</a>

    </h1>
    @if(!count($gizmos))

        <p class="alert alert-warning">
            No Gizmo found! Please
            <a href="{{ url('admin/gizmos/create')}}">create a new gizmo</a>
        </p>

    @else

        <table class="table table-striped table-bordered table_sortable">
            <thead>
            <tr>
                <th>SL</th>
                @foreach(json_decode($gizmos[0]->model->data) as $key=>$value)
                    <th>{{ucfirst($key)}}</th>
                @endforeach
                <th>Serial</th>
                <th>Action</th>
            </tr>
            </thead>

            @foreach($gizmos as $index=>$gizmo)
                <tr>
                    <td>{{1+$index}}</td>

                    @foreach(json_decode($gizmo->model->data) as $key=>$value)
                        <td>{{$value}}</td>
                    @endforeach


                    <td>{{ $gizmo->serial }}</td>

                    <td>

                        <a href="{{ URL::route('admin.gizmos.edit',$gizmo->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/gizmos/'.$gizmo->id)}}" data-token="{{csrf_token()}}" class="common_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>
                </tr>
            @endforeach
        </table>

    @endif

@stop