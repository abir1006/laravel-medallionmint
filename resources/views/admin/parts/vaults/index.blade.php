@extends('admin.layout')
@section('title','Vaults')

@section('main')
    <h1 class="page-header">Vaults
        <a href="{{route('admin.vaults.create')}}" class="btn btn-sm btn-primary pull-right "> <i class="glyphicon glyphicon-plus"></i> Add New Vault</a>

    </h1>
    @if(!count($vaults))

        <p class="alert alert-warning">
            No Vault found! Please
            <a href="{{ url('admin/vaults/create')}}">create a vault</a>
        </p>

    @else

        <table class="table table-striped table-bordered table_sortable">
            <thead>
            <tr>
                <th>SL</th>
                @foreach(json_decode($vaults[0]->model->data) as $key=>$value)
                    <th>{{ucfirst($key)}}</th>
                @endforeach
                <th>Serial</th>
                <th>Action</th>
            </tr>
            </thead>

            @foreach($vaults as $index=>$vault)
                <tr>
                    <td>{{1+$index}}</td>

                    @foreach(json_decode($vault->model->data) as $key=>$value)
                        <td>{{$value}}</td>
                    @endforeach


                    <td>{{ $vault->serial }}</td>

                    <td>

                        <a href="{{ URL::route('admin.vaults.edit',$vault->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/vaults/'.$vault->id)}}" data-token="{{csrf_token()}}" class="common_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>
                </tr>
            @endforeach
        </table>

    @endif

@stop