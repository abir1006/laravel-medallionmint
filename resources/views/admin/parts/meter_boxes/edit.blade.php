@extends('admin.layout')
@section('title','Edit Meter Box')

@section('main')


    <h1 class="page-header">Edit Meter Box

        <span class="pull-right">

            <a href="{{route('admin.meter_boxes.create')}}" class="btn btn-sm btn-primary" >
                <i class="glyphicon glyphicon-plus"></i> New
            </a>

            <a href="{{route('admin.meter_boxes.index')}}" class="btn btn-sm btn-primary" >
                <i class="glyphicon glyphicon-list"></i> All Meter Boxes
            </a>
        </span>
    </h1>


    @include('partial.form_error')

    <div class="row">

        {!! Form::model($meter_box,['method'=>'PATCH','url' => ['admin/meter_boxes',$meter_box->id ], 'class'=>'col-sm-5 machine-parts-form']) !!}


        <div class="row form-group">
            <div class="col-sm-5">
                {!! Form::label('model_id','Model',array('class'=>'form-control-static')) !!}
            </div>

            <div class="col-sm-7">

                <select name="model_id" id="model_id" class="form-control">
                    <option value="">Please Select</option>
                    @foreach($models as $index=>$model)
                        <option value="{{$model->id}}" {{$meter_box->model_id == $model->id ? 'selected' : ''}}>
                            <?php $sl=0;?>
                            @foreach(json_decode($model->data) as $key=>$value)
                                {{($sl++>0 ?' | ':'').$value}}
                            @endforeach
                        </option>
                    @endforeach


                </select>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-sm-5">
                {!! Form::label('serial','Serial',array('class'=>'form-control-static')) !!}
            </div>

            <div class="col-sm-7">
                {!! Form::text('serial',null,array('class'=>'form-control','placeholder'=>'Serial') ) !!}
            </div>
        </div>

        <div class="col-sm-offset-5 col-sm-7">
            {!! Form::submit('Update Meter Box',array('class'=>'btn btn-primary form-control'))  !!}
        </div>



        {!! Form::close() !!}

    </div>

@stop

@section('script')
    @include('admin.parts.script')
@stop
