@extends('admin.layout')
@section('title','Meter Boxes')

@section('main')
    <h1 class="page-header">Meter Boxes
        <a href="{{route('admin.meter_boxes.create')}}" class="btn btn-sm btn-primary pull-right "> <i class="glyphicon glyphicon-plus"></i> Add New Meter Box</a>

    </h1>
    @if(!count($meter_boxes))

        <p class="alert alert-warning">
            No Meter Box found! Please
            <a href="{{ url('admin/meter_boxes/create')}}">create a meter box</a>
        </p>

    @else

        <table class="table table-striped table-bordered table_sortable">
            <thead>
            <tr>
                <th>SL</th>
                @foreach(json_decode($meter_boxes[0]->model->data) as $key=>$value)
                    <th>{{ucfirst($key)}}</th>
                @endforeach
                <th>Serial</th>
                <th>Action</th>
            </tr>
            </thead>

            @foreach($meter_boxes as $index=>$meter_box)
                <tr>
                    <td>{{1+$index}}</td>

                    @foreach(json_decode($meter_box->model->data) as $key=>$value)
                        <td>{{$value}}</td>
                    @endforeach


                    <td>{{ $meter_box->serial }}</td>

                    <td>

                        <a href="{{ URL::route('admin.meter_boxes.edit',$meter_box->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/meter_boxes/'.$meter_box->id)}}" data-token="{{csrf_token()}}" class="common_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>
                </tr>
            @endforeach
        </table>

    @endif

@stop