@extends('admin.layout')
@section('title','CC Readers')

@section('main')
    <h1 class="page-header">CC Readers
        <a href="{{route('admin.cc_readers.create')}}" class="btn btn-sm btn-primary pull-right "> <i class="glyphicon glyphicon-plus"></i> Add New CC Reader</a>

    </h1>
    @if(!count($cc_readers))

        <p class="alert alert-warning">
            No CC Reader found! Please
            <a href="{{ url('admin/cc_readers/create')}}">create a cc reader</a>
        </p>

    @else

        <table class="table table-striped table-bordered table_sortable">
            <thead>
            <tr>
                <th>SL</th>
                @foreach(json_decode($cc_readers[0]->model->data) as $key=>$value)
                    <th>{{ucfirst($key)}}</th>
                @endforeach
                <th>Serial</th>
                <th>Action</th>
            </tr>
            </thead>

            @foreach($cc_readers as $index=>$cc_reader)
                <tr>
                    <td>{{1+$index}}</td>

                    @foreach(json_decode($cc_reader->model->data) as $key=>$value)
                        <td>{{$value}}</td>
                    @endforeach


                    <td>{{ $cc_reader->serial }}</td>

                    <td>

                        <a href="{{ URL::route('admin.cc_readers.edit',$cc_reader->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/cc_readers/'.$cc_reader->id)}}" data-token="{{csrf_token()}}" class="common_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>
                </tr>
            @endforeach
        </table>

    @endif

@stop