@extends('admin.layout')
@section('title','CC Modems')

@section('main')
    <h1 class="page-header">CC Modems
        <a href="{{route('admin.cc_modems.create')}}" class="btn btn-sm btn-primary pull-right "> <i class="glyphicon glyphicon-plus"></i> Add New CC Modems</a>

    </h1>
    @if(!count($cc_modems))

        <p class="alert alert-warning">
            No CC Modem found! Please
            <a href="{{ url('admin/cc_modems/create')}}">create a CC Modem</a>
        </p>

    @else

        <table class="table table-striped table-bordered table_sortable">
            <thead>
                <tr>
                    <th>SL</th>
                    @foreach(json_decode($cc_modems[0]->model->data) as $key=>$value)
                        <th>{{ucfirst($key)}}</th>
                    @endforeach
                    <th>Serial</th>
                    <th>Action</th>
                </tr>
            </thead>

            @foreach($cc_modems as $index=>$cc_modem)
                <tr>
                    <td>{{1+$index}}</td>

                    @foreach(json_decode($cc_modem->model->data) as $key=>$value)
                        <td>{{$value}}</td>
                    @endforeach


                    <td>{{ $cc_modem->serial }}</td>

                    <td>

                        <a href="{{ URL::route('admin.cc_modems.edit',$cc_modem->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/cc_modems/'.$cc_modem->id)}}" data-token="{{csrf_token()}}" class="common_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>
                </tr>
            @endforeach
        </table>

    @endif

@stop

@section('script')
    @include('admin.rep_companies.script')
@stop
