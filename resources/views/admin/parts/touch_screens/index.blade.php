@extends('admin.layout')
@section('title','Touch Screens')

@section('main')
    <h1 class="page-header">Touch Screens
        <a href="{{route('admin.touch_screens.create')}}" class="btn btn-sm btn-primary pull-right "> <i class="glyphicon glyphicon-plus"></i> Add New Touch Screen</a>

    </h1>
    @if(!count($touch_screens))

        <p class="alert alert-warning">
            No Touch Screen found! Please
            <a href="{{ url('admin/touch_screens/create')}}">create a Touch Screen</a>
        </p>

    @else

        <table class="table table-striped table-bordered table_sortable">
            <thead>
            <tr>
                <th>SL</th>
                @foreach(json_decode($touch_screens[0]->model->data) as $key=>$value)
                    <th>{{ucfirst($key)}}</th>
                @endforeach
                <th>Serial</th>
                <th>Action</th>
            </tr>
            </thead>

            @foreach($touch_screens as $index=>$touch_screen)
                <tr>
                    <td>{{1+$index}}</td>

                    @foreach(json_decode($touch_screen->model->data) as $key=>$value)
                        <td>{{$value}}</td>
                    @endforeach


                    <td>{{ $touch_screen->serial }}</td>

                    <td>

                        <a href="{{ URL::route('admin.touch_screens.edit',$touch_screen->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/touch_screens/'.$touch_screen->id)}}" data-token="{{csrf_token()}}" class="common_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>
                </tr>
            @endforeach
        </table>

    @endif

@stop