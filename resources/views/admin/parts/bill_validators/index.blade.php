@extends('admin.layout')
@section('title','Bill Validators')

@section('main')
    <h1 class="page-header">Bill Validators
        <a href="{{route('admin.bill_validators.create')}}" class="btn btn-sm btn-primary pull-right "> <i class="glyphicon glyphicon-plus"></i> Add New Bill Validator</a>

    </h1>
    @if(!count($bill_validators))

        <p class="alert alert-warning">
            No Bill Validator found! Please
            <a href="{{ url('admin/bill_validators/create')}}">create a bill validator</a>
        </p>

    @else

        <table class="table table-striped table-bordered table_sortable">
            <thead>
                <tr>
                    <th>SL</th>
                    @foreach(json_decode($bill_validators[0]->model->data) as $key=>$value)
                        <th>{{ucfirst($key)}}</th>
                    @endforeach
                    <th>Serial</th>
                    <th>Action</th>
                </tr>
            </thead>

            @foreach($bill_validators as $index=>$bill_validator)
                <tr>
                    <td>{{1+$index}}</td>

                    @foreach(json_decode($bill_validator->model->data) as $key=>$value)
                        <td>{{$value}}</td>
                    @endforeach


                    <td>{{ $bill_validator->serial }}</td>

                    <td>

                        <a href="{{ URL::route('admin.bill_validators.edit',$bill_validator->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/bill_validators/'.$bill_validator->id)}}" data-token="{{csrf_token()}}" class="common_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>
                </tr>
            @endforeach
        </table>

    @endif

@stop