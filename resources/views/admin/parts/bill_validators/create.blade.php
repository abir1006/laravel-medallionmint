@extends('admin.layout')
@section('title','Create Bill Validator')

@section('main')
    <h1 class="page-header">Create Bill Validator
        <a href="{{route('admin.bill_validators.index')}}" class="btn btn-sm btn-primary pull-right">
            <i class="glyphicon glyphicon-list"></i> All Bill Validators
        </a>
    </h1>

    @include('partial.form_error')

    <div class="row">

        {!! Form::open(['url'=>'admin/bill_validators','class'=>'col-sm-5 machine-parts-form']) !!}

        <div class="row form-group">
            <div class="col-sm-5">
                {!! Form::label('model_id','Model',array('class'=>'form-control-static')) !!}
            </div>

            <div class="col-sm-7">

                <select name="model_id" id="model_id" class="form-control">
                    <option value="">Please Select</option>
                    @foreach($models as $index=>$model)
                        <option value="{{$model->id}}">
                            <?php $sl=0;?>
                            @foreach(json_decode($model->data) as $key=>$value)
                                {{($sl++>0 ?' | ':'').$value}}
                            @endforeach
                        </option>
                    @endforeach


                </select>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-sm-5">
                {!! Form::label('serial','Serial',array('class'=>'form-control-static')) !!}
            </div>

            <div class="col-sm-7">
                {!! Form::text('serial',null,array('class'=>'form-control','placeholder'=>'Serial') ) !!}
            </div>
        </div>

        <div class="col-sm-offset-5 col-sm-7">
            {!! Form::submit('Create Bill Validator',array('class'=>'btn btn-primary form-control'))  !!}
        </div>



        {!! Form::close() !!}

    </div>

@stop

@section('script')
    @include('admin.parts.script')
@stop
