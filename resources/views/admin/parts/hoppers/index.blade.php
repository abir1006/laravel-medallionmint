@extends('admin.layout')
@section('title','Hoppers')

@section('main')
    <h1 class="page-header">Hoppers
        <a href="{{route('admin.hoppers.create')}}" class="btn btn-sm btn-primary pull-right "> <i class="glyphicon glyphicon-plus"></i> Add New Hopper</a>

    </h1>
    @if(!count($hoppers))

        <p class="alert alert-warning">
            No Hopper found! Please
            <a href="{{ url('admin/hoppers/create')}}">create a new hopper</a>
        </p>

    @else

        <table class="table table-striped table-bordered table_sortable">
            <thead>
            <tr>
                <th>SL</th>
                @foreach(json_decode($hoppers[0]->model->data) as $key=>$value)
                    <th>{{ucfirst($key)}}</th>
                @endforeach
                <th>Serial</th>
                <th>Action</th>
            </tr>
            </thead>

            @foreach($hoppers as $index=>$hopper)
                <tr>
                    <td>{{1+$index}}</td>

                    @foreach(json_decode($hopper->model->data) as $key=>$value)
                        <td>{{$value}}</td>
                    @endforeach


                    <td>{{ $hopper->serial }}</td>

                    <td>

                        <a href="{{ URL::route('admin.hoppers.edit',$hopper->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
                        <a title="delete" data-url="{{url('admin/hoppers/'.$hopper->id)}}" data-token="{{csrf_token()}}" class="common_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

                    </td>
                </tr>
            @endforeach
        </table>

    @endif

@stop