<script>
    $(function () {

        $(document).ready(function () {

            // Build the chart
            $('#antiqued_pie_container').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    height: 300
                },
                title: {
                    text: 'Antiqued Sales'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    type: 'pie',
                    name: 'Browser share',
                    data: [
                        ['Brass',   45.0],
                        ['Copper',  26.8],
                        ['Nickel Silver',    8.5]
                    ]
                }]
            });
        });

    });
</script>