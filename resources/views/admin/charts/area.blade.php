<script>
    $(function () {
        $('#area_chart_container').highcharts({
            chart: {
                type: 'area',
                spacingBottom: 30,
                height: 300
            },
            title: {
                text: 'Total Revenue'
            },
            subtitle: {
                text: '* Missing months are not shown',
                floating: true,
                align: 'right',
                verticalAlign: 'bottom',
                y: 15
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 150,
                y: 0,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            xAxis: {
                categories: [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                ]
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + '</b><br/>' +
                            this.x + ': ' + this.y;
                }
            },
            plotOptions: {
                area: {
                    fillOpacity: 0.5
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Cash',
                data: [7, 1, 4, 4, 5, 2, 3, 7, 5, 5, 4, 3]
            }, {
                name: 'Credit Cards',
                data: [1, 0, 3, null, 3, 9, 2, 1, 4, 6, 7, 4]
            }]
        });
    });
</script>