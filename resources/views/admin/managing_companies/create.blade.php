@extends('admin.layout')
@section('title','New Managing Company')

@section('main')
	<h1 class="page-header">Add New Managing Company</h1>
	@include('partial.form_error')

	{!! Form::open(['url'=>'admin/managing_companies','class'=>'row form-horizontal']) !!}

		@include('admin.managing_companies.form',['submit_btn_text'=>'Add Managing Company'])

	{!!Form::close()!!}


@stop

@section('script')
	@include('admin.managing_companies.script')
@stop