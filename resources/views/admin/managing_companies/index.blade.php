@extends('admin.layout')
@section('title','Managing Companies')

@section('main')
	<h1 class="page-header">
		Managing Companies
		<a href="{{URL::to('admin/managing_companies/create')}}" class="btn btn-sm btn-primary pull-right">
			<i class="glyphicon glyphicon-plus"></i> Add New Managing Company
		</a>
	</h1>

	@if(!count($managing_companies))

		<p class="alert alert-warning">
			No managing company found! Please
			<a href="{{ url('admin/managing_companies/create')}}">create a managing company</a>
		</p>

	@else
		<table class="table table-striped table-bordered table_sortable">
			<thead>
			<tr>
				<th>SL</th>
				<th>Managing Company Name</th>
				<th>Locations</th>
				<th>Machines</th>
				<th>Action</th>
			</tr>
			</thead>
			@foreach($managing_companies as $index => $managing_company)
				<tr>
					<td>{{$index+1}}</td>
					<td>
						<a href="{{ URL::route('admin.managing_companies.show',$managing_company->id) }}">
							{{$managing_company->name}}
						</a>
					</td>
					<td>{{ $managing_company->locations->count() }}</td>
					<td>{{ $managing_company->Machines()->count()}}</td>
					<td>

						<a href="{{ URL::route('admin.managing_companies.edit',$managing_company->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
						<a title="delete" data-url="{{url('admin/managing_companies/'.$managing_company->id)}}" data-token="{{csrf_token()}}" class="admin_managing_company_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>

					</td>
				</tr>
			@endforeach
		</table>

	@endif

@stop

@section('script')
	@include('admin.managing_companies.script')
@endsection
