@extends('admin.layout')
@section('title','Edit '.$managing_company->name)

@section('main')
	<a href="{{URL::route('admin.managing_companies.show',$managing_company->id)}}" class="btn btn-info btn-sm pull-right">
		<i class="glyphicon glyphicon-eye-open"></i> View Details
	</a>
	<h1 class="page-header">Edit {{ $managing_company->name }}
	</h1>
	@include('partial.form_error')



	{!! Form::model($managing_company,['method'=>'PATCH','url' => ['admin/managing_companies',$managing_company->id ],'files'=>'true', 'class'=>'row form-horizontal']) !!}

		@include('admin.managing_companies.form',['submit_btn_text'=>'Update'])

	{!!Form::close()!!}


@stop

@section('script')
	@include('admin.managing_companies.script')
@stop