@extends('admin.layout')

@section('title','Hoppers Report')

@section('main')
    <h3 class="page-header">Hoppers Report</h3>

    <table class="table table-striped table-bordered hoppers-report-table">

        <thead>
            <tr>
                <th>SL</th>
                <th>Location</th>
                <th>Machine</th>
                <th>Hopper#/Coin</th>
                <th></th>
                <th>Last Call In</th>
                <th></th>
                <th>Last Trans</th>
            </tr>
        </thead>

        <?php
            $sl = 1;
            $now = \Carbon\Carbon::now();
        ?>

        <tbody>
            @foreach($machines as $machine)
                @if($machine->deviceHealth)
                    @for($i=1;$i<=$machine->coins;$i++)
                        <tr>
                            <td>{{$sl++}}</td>
                            <td><a href="{{Route('admin.locations.show',$machine->location->id)}}">{{$machine->location->name}}</a></td>
                            <td><a href="{{Route('admin.machines.edit',$machine->id)}}">{{$machine->name}}</a></td>
                            <td>
                                @if($machine->{'coin_'.$i})
                                    {{$i.' '.$machine->{'coin_'.$i}->name }}

                                @endif
                            </td>

                            <td data-search="{{\Carbon\Carbon::parse($machine->deviceHealth->date_of_last_call_in)->diff($now)->days}}">
                                {{\Carbon\Carbon::parse($machine->deviceHealth->date_of_last_call_in)->diff($now)->days}}

                            </td>

                            <td data-search="{{$machine->deviceHealth->date_of_last_call_in}}">
                                {{date('m-d-y h:i A',strtotime($machine->deviceHealth->date_of_last_call_in))}}

                            </td>

                            <td data-search="{{\Carbon\Carbon::parse($machine->deviceHealth->{'hopper_'.$i.'_last_transaction_date'})->diff($now)->days}}">

                                @if($machine->deviceHealth->{'hopper_'.$i.'_last_transaction_date'} > 0)
                                    {{\Carbon\Carbon::parse($machine->deviceHealth->{'hopper_'.$i.'_last_transaction_date'})->diff($now)->days}}
                                @else
                                    Never
                                @endif

                            </td>

                            <td data-search="{{$machine->deviceHealth->{'hopper_'.$i.'_last_transaction_date'} }}">

                                @if($machine->deviceHealth->{'hopper_'.$i.'_last_transaction_date'} > 0)
                                    {{ date('m-d-Y h:i A',strtotime($machine->deviceHealth->{'hopper_'.$i.'_last_transaction_date'} ) )  }}
                                @else
                                    Never
                                @endif

                            </td>


                        </tr>
                    @endfor
                @endif
            @endforeach
        </tbody>
    </table>
@stop


@section('script')
    <script type="text/javascript">

        $(document).ready(function(){


            var hoppers_report_table = $('table.hoppers-report-table').DataTable({
                aLengthMenu: [
                    [10,25, 50, 100, 200, -1],
                    [10,25, 50, 100, 200, "All"]
                ],
                iDisplayLength: -1,
                bAutoWidth: false,
                dom: "<'row'<'col-sm-2'l><'custom-filter-wrapper col-sm-7'> <'col-sm-3'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            });


            $(".custom-filter-wrapper").html('Filter By '



                    +'<select class="filter-by form-control">'
                    +'<option value="all">All</option>'
                    +'<option value="4">Last Call-In</option>'
                    +'<option value="6" selected>Last Transaction</option>'
                    +'</select> '

                    +'<select class="range form-control">'
                    +'<option value="not-within">Not With in</option>'
                    +'<option value="within">With in</option>'
                    +'</select> '

                    +'<select class="days-diff form-control">'
                    +'<option value="all">All</option>'
                    +'<option value="1">1 Day</option>'
                    +'<option value="2">2 Days</option>'
                    +'<option value="3" selected>3 Days</option>'
                    +'<option value="7">7 Days</option>'
                    +'<option value="10">10 Days</option>'
                    +'<option value="14">14 Days</option>'
                    +'<option value="25">25 Days</option>'
                    +'<option value="30">30 Days</option>'

                    +'</select>'

                    +'&nbsp;&nbsp;<div class="checkbox"><label><input type="checkbox" class="exclude-never" checked> Exclude Never</label></div>'
            );



            var filterBy = $('.custom-filter-wrapper select.filter-by').val();
            var daysDiff = $('.custom-filter-wrapper select.days-diff').val();
            var range = $('.custom-filter-wrapper select.range').val();
            var exclude_never = $('.custom-filter-wrapper input.exclude-never').is(':checked');


            $.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {

                if(exclude_never && (data[6] > 3650) ){

                    return false;

                }

                if(filterBy === 'all' || daysDiff === 'all' ){
                    return true;
                }

                if(range === 'within') {
                    if (data[filterBy] <= parseInt(daysDiff) ) {
                        return true;
                    }

                }else if(range === 'not-within'){
                    if (data[filterBy] > parseInt(daysDiff) ) {
                        return true;
                    }
                }


                return false;
            });

            $('.custom-filter-wrapper select.filter-by,.custom-filter-wrapper select.days-diff,.custom-filter-wrapper select.range, .custom-filter-wrapper input.exclude-never').on('change',function(){


                filterBy = $('.custom-filter-wrapper select.filter-by').val();
                daysDiff = $('.custom-filter-wrapper select.days-diff').val();
                range = $('.custom-filter-wrapper select.range').val();
                exclude_never = $('.custom-filter-wrapper input.exclude-never').is(':checked');

                hoppers_report_table.draw();
            });

            hoppers_report_table.draw();


        });
    </script>
@stop