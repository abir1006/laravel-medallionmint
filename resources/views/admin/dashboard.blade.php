@extends('admin.layout')
@section('title','Admin Dashboard')
@section('main')
  <h1 class="page-header">Admin Dashboard</h1>

  <div class="row">       
    <div class="col-sm-8" id="area_chart_container"> </div>           
    <div class="col-sm-4">
      <div class="panel panel-default">
        <!-- Default panel contents -->
        

        <!-- List group -->
        <ul class="list-group" style="height:300px;overflow:hidden;">
            <li class="list-group-item active">User Name<span class="pull-right text-default">Last Activity</span></li>
          @foreach($online_users as $online_user)
            <li class="list-group-item">
              {{$online_user->user->name}} 
              <span class="pull-right text-muted">
                {{\Carbon\Carbon::parse(date('d F Y h:i:s A',$online_user->last_activity))->diffForHumans()}}
              </span>
            </li>
          @endforeach
        </ul>

      </div>
    </div>

  </div>

  <div class="row">
    <div class="col-sm-5">
      <div class="panel panel-default">
          <!-- Default panel contents -->
          <div class="panel-heading">Map with all location markers</div>
          <div class="panel-body"><img src="/images/usa_map.jpg" alt="" style="width:100%"/></div>
      </div>
    </div>
    <div class="col-sm-2" id="shiny_pie_container"></div>
    <div class="col-sm-2" id="antiqued_pie_container"></div>
    <div class="col-sm-3" id="all_coins_pie_container"></div>
  </div>

@stop


@section('script')
  @include('admin.charts.area')
  @include('admin.charts.shiny_pie')
  @include('admin.charts.antiqued_pie')
  @include('admin.charts.all_coins_pie')
@stop