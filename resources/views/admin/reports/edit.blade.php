@extends('admin.layout')

@section('main')
    <h3 class="page-header">         
        <span class="text-info"> <i class="glyphicon glyphicon-edit"></i> Edit {{studly_case($report->type)}} Report</span> 
        <small class="text-muted">{{$location->name}}</small>
    </h3>
    
    @if($machines->count())
        {!! Form::model($report,['method'=>'PATCH','url' => ['admin/locations/'.$location->id.'/reports',$report->id ], 'class'=>'post_meters edit']) !!}
           

            
            <input type="hidden" name="no_of_machines" value="{{$machines->count()}}">

            <div role="tabpanel" class="">

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#date_time" aria-controls="date_time" role="tab" data-toggle="tab">Date Time</a></li>
                    <?php $sl=1;?>
                    @foreach($machines as $machine)
                        <li role="presentation"><a href="#machine_{{$sl}}" aria-controls="machine_{{$sl}}" role="tab" data-toggle="tab">{{$machine->name}}</a></li>                
                        <?php $sl++;?>
                    @endforeach
                    
                    <li role="presentation"><a href="#confirm" aria-controls="confirm" role="tab" data-toggle="tab">Confirm</a></li>
                </ul>
                
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="date_time">
                        <div class="form-group row text-muted">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2">Current ({{$report->type}})</div>
                            <div class="col-sm-2">
                                @if($prev_report)
                                    Previous ({{$prev_report->type}})
                                @else
                                    @if($report->type=='monthly')
                                        First Monthly
                                    @else
                                        First Report
                                    @endif
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-2">{!!Form::label('month','Report for Month',array('class'=>'form-control-static'))!!}</div>
                            <div class="col-sm-2">{!!Form::select('month',array(
                                'January'=>'January',
                                'February'=>'February',
                                'March'=>'March',
                                'April'=>'April',
                                'May'=>'May',
                                'June'=>'June',
                                'July'=>'July',
                                'August'=>'August',
                                'September'=>'September',
                                'October'=>'October',
                                'November'=>'November',
                                'December'=>'December'

                                ),null,array('class'=>'form-control')) !!}</div>
                            
                            <div class="col-sm-2 form-control-static">@if($prev_report){{$prev_report->month}}@endif</div>

                        </div>

                        <div class="form-group  row">
                            <div class="col-sm-2">{!!Form::label('year','Report for Year',array('class'=>'form-control-static'))!!}</div>
                            <div class="col-sm-2">{!!Form::selectRange('year',date('Y'),2010,null,array('class'=>'form-control')) !!}</div>
                            <div class="col-sm-2 form-control-static">@if($prev_report){{$prev_report->year}}@endif</div>
                        </div>

                        <div class="form-group  row">
                            <div class="col-sm-2">{!!Form::label('date','Date Read',array('class'=>'form-control-static'))!!}</div>
                            <div class="col-sm-2">{!!Form::text('date',date('m/d/Y',strtotime($report->date)),array('class'=>'form-control datepicker')) !!}</div>
                            <div class="col-sm-2 form-control-static">@if($prev_report){{date('d F Y',strtotime($prev_report->date))}}@endif</div>
                        </div>

                        
                        <div class="form-group  row">
                            <div class="col-sm-2">{!!Form::label('time','Time Read',array('class'=>'form-control-static'))!!}</div>                       
                            <div class="col-sm-2">{!!Form::text('time',date('H:i',strtotime($report->time)),array('class'=>'form-control clockpicker')) !!}</div>
                            <div class="col-sm-2 form-control-static">@if($prev_report){{date('h:i A',strtotime($prev_report->time))}}@endif</div>
                        </div>

                        

                        <div class="form-group  row">
                            <div class="col-sm-2">{!!Form::label('read_by','Read By',array('class'=>'form-control-static'))!!}</div>
                            <div class="col-sm-2">{!!Form::text('read_by',null,array('class'=>'form-control')) !!}</div>
                            <div class="col-sm-2 form-control-static">@if($prev_report){{$prev_report->read_by}}@endif</div>
                        </div>

                        <div class="form-group  row">
                            <div class="col-sm-2">{!!Form::label('verified_by','Verified By',array('class'=>'form-control-static'))!!}</div>
                            <div class="col-sm-2">{!!Form::text('verified_by',null,array('class'=>'form-control')) !!}</div>
                            <div class="col-sm-2 form-control-static">@if($prev_report){{$prev_report->verified_by}}@endif</div>
                        </div>

                        

                        <div class="form-group  row">
                            
                            <div class="col-sm-2 col-sm-offset-2">
                                <a href="#" class="date_time_next_btn btn btn-sm btn-info">Next Step <i class="glyphicon glyphicon-chevron-right"></i></a>
                            </div>
                            
                        </div>

                    </div> <!-- End of Date Time-->

                    <?php $sl = 0;?>
                    
                    @foreach($machines as $machine)
                    
                    <?php $sl++;?>
                    
                    <input type="hidden" name="machine_{{$sl}}_id" value="{{$machine->id}}">
                    <input type="hidden" name="machine_{{$sl}}_no_of_coins" value="{{$machine->coins}}">

                    <div role="tabpanel" class="tab-pane fade machine" id="machine_{{$sl}}">
                        <div class="row">
                            <div class="col-sm-2"><img width="100%" data-src="holder.js/100%x380"></div>
                            <div class="col-sm-10">
                                <div class="form-group row text-muted">
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-1">Previous</div>
                                    <div class="col-sm-2">Current</div>
                                    <div class="col-sm-1">Adjmnt</div>
                                    <div class="col-sm-1">Total</div>
                                    <div class="col-sm-2">Calculated Inventory</div>
                                    <div class="col-sm-2">Physical Inventory</div>
                                </div>

                                <div class="form-group row meter credit_meter">
                                    <div class="col-sm-1"><img class="cc_cash" src="/images/cash.jpg"></div>
                                    <div class="col-sm-2">{!!Form::label('meter_name','Meter 1',array('class'=>'form-control-static'))!!}</div>
                                    

                                    @if($prev_report)
                                        <div class="col-sm-1 text-muted form-control-static previous">{{$prev_report->machine_readings->where('machine_id',$machine->id)->first()->meter_1_current}}</div>
                                        <input type="hidden" name="machine_{{$sl}}_meter_1_previous" value="{{$prev_report->machine_readings->where('machine_id',$machine->id)->first()->meter_1_current}}">
                                    @else
                                        <div class="col-sm-1 text-muted form-control-static previous">{{$machine->starting_meter_1}}</div>
                                        <input type="hidden" name="machine_{{$sl}}_meter_1_previous" value="{{$machine->starting_meter_1}}">
                                    @endif
                                    
                                    <div class="col-sm-2 ">{!!Form::text('machine_'.$sl.'_meter_1_current',$report->machine_readings->where('machine_id',$machine->id)->first()->meter_1_current,array('class'=>'form-control current'))!!}</div>
                                    <div class="col-sm-1">{!!Form::text('machine_'.$sl.'_meter_1_adj',$report->machine_readings->where('machine_id',$machine->id)->first()->meter_1_adj,array('class'=>'form-control adj'))!!}</div>
                                    <div class="col-sm-1 form-control-static total"></div>
                                </div>

                                <div class="form-group row meter credit_meter">
                                    <div class="col-sm-1"><img class="cc_cash" src="/images/cc.gif"></div>
                                    <div class="col-sm-2">{!!Form::label('meter_name','Meter 2',array('class'=>'form-control-static'))!!}</div>
                                    
                                    @if($prev_report)
                                        <div class="col-sm-1 text-muted form-control-static previous">{{$prev_report->machine_readings->where('machine_id',$machine->id)->first()->meter_2_current}}</div>
                                        <input type="hidden" name="machine_{{$sl}}_meter_2_previous" value="{{$prev_report->machine_readings->where('machine_id',$machine->id)->first()->meter_2_current}}">
                                    @else
                                        <div class="col-sm-1 text-muted form-control-static previous">{{$machine->starting_meter_2}}</div>
                                        <input type="hidden" name="machine_{{$sl}}_meter_2_previous" value="{{$machine->starting_meter_2}}">
                                    @endif
                                   
                                    <div class="col-sm-2">{!!Form::text('machine_'.$sl.'_meter_2_current',$report->machine_readings->where('machine_id',$machine->id)->first()->meter_2_current,array('class'=>'form-control current'))!!}</div>
                                    <div class="col-sm-1">{!!Form::text('machine_'.$sl.'_meter_2_adj',$report->machine_readings->where('machine_id',$machine->id)->first()->meter_2_adj,array('class'=>'form-control adj'))!!}</div>
                                    <div class="col-sm-1 form-control-static total"></div>
                                    <div class="col-sm-1 form-control-static text-muted">Display</div>
                                    <div class="col-sm-1 form-control-static text-muted">Stock</div>
                                    <div class="col-sm-1 form-control-static text-muted">Display</div>
                                    <div class="col-sm-1 form-control-static text-muted">Stock</div>
                                </div>

                                @for($i=1;$i<=$machine->coins;$i++)
                                <div class="form-group row meter coin_meter">
                                    <div class="col-sm-1 coin-thumb-wrapper">
                                        
                                        <div class="coin-thumb {{($machine->{'coin_'.$i}()->metal == 'Nickel Silver'?'nickel_silver':$machine->{'coin_'.$i}()->metal)}}">                                    
                                            {!! Html::image('upload/100-'.$machine->{'coin_'.$i}()->front_dies()->photo, null, array('class' => 'front')) !!}
                                            {!! Html::image('upload/100-'.$machine->{'coin_'.$i}()->back_dies()->photo, null, array('class' => 'back')) !!}
                                        </div>
                                        
                                    </div>
                                    <div class="col-sm-2">
                                        {!!Form::label('meter_name','Meter '.($i+2),array('class'=>'form-control-static'))!!}
                                        <br>{{$machine->{'coin_'.$i}()->name}}
                                    </div>
                                    
                                    @if($prev_report)
                                        <div class="col-sm-1 text-muted form-control-static previous">{{ $prev_report->machine_readings->where('machine_id',$machine->id)->first()->{'meter_'.($i+2).'_current'} }}</div>
                                        <input type="hidden" name="machine_{{$sl}}_meter_{{($i+2)}}_previous" value="{{ $prev_report->machine_readings->where('machine_id',$machine->id)->first()->{'meter_'.($i+2).'_current'} }}">
                                    @else
                                        <div class="col-sm-1 text-muted form-control-static previous">{{ $machine->{'starting_meter_'.($i+2)} }}</div>
                                        <input type="hidden" name="machine_{{$sl}}_meter_{{($i+2)}}_previous" value="{{ $machine->{'starting_meter_'.($i+2)} }}">                                    
                                    @endif                                    
                                    
                                    <div class="col-sm-2">{!!Form::text('machine_'.$sl.'_meter_'.($i+2).'_current',$report->machine_readings->where('machine_id',$machine->id)->first()->{'meter_'.($i+2).'_current'},array('class'=>'form-control current'))!!}</div>
                                    <div class="col-sm-1">{!!Form::text('machine_'.$sl.'_meter_'.($i+2).'_adj',$report->machine_readings->where('machine_id',$machine->id)->first()->{'meter_'.($i+2).'_adj'},array('class'=>'form-control adj'))!!}</div>
                                    <div class="col-sm-1 form-control-static total"></div>
                                    <div class="col-sm-1 form-control-static">{{$machine->{'coin_'.$i}()->top_inventory}}</div>
                                    <div class="col-sm-1 form-control-static">{{$machine->{'coin_'.$i}()->inventory}}</div>                                    
                                    <div class="col-sm-1">{!!Form::text('machine_'.$sl.'_meter_'.($i+2).'_display',$report->machine_readings->where('machine_id',$machine->id)->first()->{'meter_'.($i+2).'_display'},array('class'=>'form-control'))!!}</div>
                                    <div class="col-sm-1">{!!Form::text('machine_'.$sl.'_meter_'.($i+2).'_stock',$report->machine_readings->where('machine_id',$machine->id)->first()->{'meter_'.($i+2).'_stock'},array('class'=>'form-control'))!!}</div>
                                </div>
                                @endfor

                                
                                <div class="row ">
                            
                                    <div class="col-sm-4 btn-container">
                                        <a href="#" class="prev_btn btn btn-sm btn-info" data-tab-number="{{$sl}}"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a>
                                        <a href="#" class="next_btn btn btn-sm btn-info" data-tab-number="{{$sl}}">Next Step <i class="glyphicon glyphicon-chevron-right"></i></a>
                                    </div>

                                    <div class="col-sm-4">
                                        <h4 class="average_wrapper text-muted">Average per coin: $<span class="average">0</span></h4>
                                    </div>

                                    <div class="col-sm-4 text-muted">
                                        <i class="glyphicon glyphicon-info-sign"></i> Machine Price : <strong>{{$machine->price()}}</strong><br>
                                        Expected average per coin : <strong>$<span class="price_min">{{$machine->price_min()}}</span></strong> to <strong>$<span class="price_max">{{$machine->price_max()}}</span></strong>
                                    </div>

                                    
                                </div>
                            </div>
                        </div>

                    </div> <!-- End of dummy machine 1 -->

                    @endforeach

                    
                    
                    <div role="tabpanel" class="tab-pane fade " id="confirm">
                        <h3>Confirm Tab</h3>
                        <p>Lorem Ipsum Dolor sit amet is a dummy text .... </p>

                        <a href="#" class="prev_btn btn btn-sm btn-info" data-tab-number="{{$machines->count()+1}}"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a>
                        <button class="submit_btn btn btn-sm btn-primary"><i class="glyphicon glyphicon-send"></i> Update Report</button>


                    </div>
                </div>

            </div>
        {!!Form::close()!!}
    @else
        <div class="alert alert-warning">No machines found under this location !!</div>
    @endif

@stop


@section('script')
    @include('admin.reports.script')
@stop