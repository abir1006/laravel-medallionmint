<script type="text/javascript">
$(document).ready(function(){	

	function validate_date_time(){
		var has_error = false;

		var date = $('.post_meters input[name="date"]');
		var time = $('.post_meters input[name="time"]');
		var read_by = $('.post_meters input[name="read_by"]');
		var verified_by = $('.post_meters input[name="verified_by"]');

		date.removeClass('error').parent().find('span.error').remove();
		time.removeClass('error').parent().find('span.error').remove();
		read_by.removeClass('error').parent().find('span.error').remove();
		verified_by.removeClass('error').parent().find('span.error').remove();

		if(date.val() ==''){
			date.addClass('error').after('<span class="error">This is Required</span>');
			has_error = true;
		}

		if(time.val() ==''){
			time.addClass('error').after('<span class="error">This is Required</span>');
			has_error = true;
		}

		if(read_by.val() ==''){
			read_by.addClass('error').after('<span class="error">This is Required</span>');
			has_error = true;
		}

		if(verified_by.val() ==''){
			verified_by.addClass('error').after('<span class="error">This is Required</span>');
			has_error = true;
		}		

		return has_error;

	}
	
	
	$('input[name="read_by"],input[name="verified_by"]').blur(function(){
		var that = $(this);
		that.removeClass('error').parent().find('span.error').remove();
		if(that.val() ==''){
			that.addClass('error').after('<span class="error">This is Required</span>');
			has_error = true;
		}

	});

	/*this chking isn't important*/
	$('input[name="read_by"],input[name="verified_by"]').keyup(function(){
		
		if(!$(this).hasClass('error')){return;}

		var that = $(this);
		that.removeClass('error').parent().find('span.error').remove();
		if(that.val() ==''){
			that.addClass('error').after('<span class="error">This is Required</span>');
			has_error = true;
		}

	});

	$('input[name="date"],input[name="time"]').change(function(){
		var that = $(this);
		that.removeClass('error').parent().find('span.error').remove();
		if(that.val() ==''){
			that.addClass('error').after('<span class="error">This is Required</span>');
			has_error = true;
		}

	});

	$('.date_time_next_btn').click(function(e){
		e.preventDefault();
		var has_error = validate_date_time();

		if(!has_error){
			$('.post_meters ul.nav.nav-tabs li:eq(1) a').tab('show');
			
		}

	});

	

	
	$("input.current,input.adj").keydown(function (e) {

		//console.log(e.keyCode,e.shiftKey);
		//backspace=8,delete=46,tab=9,enter=13,escape=27,dot=110,fullstop=190	, F5 = 116, minus(keypad) = 109, minus(189)

        // Allow: backspace, delete, tab, escape, enter, F5, minus
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 116,109,189]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    function validate(previous,current,adj,total){
    	current.removeClass('error').parent().find('span.error').remove();
    	adj.removeClass('error').parent().find('span.error').remove();

    	var previous_val = parseInt(previous.text()) || 0;
		var current_val = parseInt(current.val()) || 0;
		var adj_val = parseInt(adj.val()) || 0;
		var total_val = current_val - previous_val + adj_val ;

		total.text(total_val).trigger('change');

		var has_error = false;

		if(current.val() ==''){

			current.addClass('error').after('<span class="error">This is Required</span>');
			has_error = true;

		}else if(current_val < previous_val){
			current.addClass('error').after('<span class="error">less than previous</span>');			
			has_error = true;

		}else{
			
			if( total_val<0 ){
				adj.addClass('error').after('<span class="error">total negative</span>');
				has_error = true;
			}
		}

		return has_error;
    }
	
	

	/*this chking isn't important*/
	$('input.current,input.adj').keyup(function(){			

		if(!$(this).hasClass('error')){return;}

		var previous = $(this).closest('.row').find('.previous');
		var current = $(this).closest('.row').find('.current');
		var adj = $(this).closest('.row').find('.adj');
		var total = $(this).closest('.row').find('.total');
	
		validate(previous,current,adj,total);
		
		
	});

	$('input.current,input.adj').blur(function(){	
		
		var previous = $(this).closest('.row').find('.previous');
		var current = $(this).closest('.row').find('.current');
		var adj = $(this).closest('.row').find('.adj');
		var total = $(this).closest('.row').find('.total');
	
		validate(previous,current,adj,total);
		
		
	});	

	

	
	$('.post_meters .next_btn').click(function(e){
		e.preventDefault();
		

		var has_error = false;

		var meters = $(this).closest('.tab-pane').find('.row.meter');
		
		$.each(meters,function(i){
			var previous = $(this).find('.previous');
			var current = $(this).find('.current');
			var adj = $(this).find('.adj');
			var total = $(this).find('.total');		

			if( validate(previous,current,adj,total)  ){			

				has_error = true;
			}

			
		});

		
		if(!has_error){
			var next_tab_number = $(this).data('tab-number') + 1 ;
			$('.post_meters ul.nav.nav-tabs li:eq('+next_tab_number+') a').tab('show');
		}
		
		
		
	});

	$('.post_meters .prev_btn').click(function(e){
		e.preventDefault();

		var prev_tab_number = $(this).data('tab-number') - 1 ;
		$('.post_meters ul.nav.nav-tabs li:eq('+prev_tab_number+') a').tab('show');
	});

	/****************avg price chking...*********************/
	$(".total").on('change',function(){		

		var machine = $(this).closest('.machine');
		var credit_meters = machine.find('.credit_meter');
		var coin_meters = machine.find('.coin_meter');
		var average = machine.find('.average');
		var price_min = parseFloat(machine.find('.price_min').text());
		var price_max = parseFloat(machine.find('.price_max').text());


		var total_credits = 0;
		var total_coins = 0;

		$.each(credit_meters,function(i){			
			total_credits += parseInt($(this).find('.total').text()) || 0; 
		});

		$.each(coin_meters,function(i){			
			total_coins += parseInt($(this).find('.total').text()) || 0; 
		});

		var average_price = (total_credits / total_coins).toFixed(2) ;

		average.text(average_price);

		console.log(price_min,price_max,average_price);
		//correct
		if( (average_price >= price_min ) && (average_price <= price_max) ){
			average.closest('h4').addClass('text-primary').removeClass('text-danger');
		}else{
			average.closest('h4').addClass('text-danger').removeClass('text-primary');
		}

		
	});

	/*********************************************/


	$('form.post_meters').submit(function(e){		
		e.preventDefault();
		
		var has_error = validate_date_time();

		
		var meters = $('.post_meters .row.meter');
		
		$.each(meters,function(i){
			var previous = $(this).find('.previous');
			var current = $(this).find('.current');
			var adj = $(this).find('.adj');
			var total = $(this).find('.total');		

			if( validate(previous,current,adj,total)  ){			

				has_error = true;
			}

			
		});
		
		
		if(has_error){			
			alertify.alert('Validation Error Found !!, please fix and try again');			
		}else{			
			

			$('form.post_meters')[0].submit();
			
		}

	});

	// this is actually not for validation purpose
	// calculate total and average value for edit form
	if($('form.post_meters').hasClass('edit')){
		var meters = $('.post_meters .row.meter');
		
		$.each(meters,function(i){
			var previous = $(this).find('.previous');
			var current = $(this).find('.current');
			var adj = $(this).find('.adj');
			var total = $(this).find('.total');		

			validate(previous,current,adj,total);
			
		});
	}
	
	
});

</script>