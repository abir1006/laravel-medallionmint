@extends('admin.layout')

@section('title','Add New Parent Company')

@section('main')
   <h1 class="page-header">Add New Parent Company</h1>
   @include('partial.form_error')

   {!! Form::open(['url'=>'admin/companies','class'=>'row form-horizontal']) !!}

      @include('admin.companies.form',['submit_btn_text'=>'Add New Parent Company'])

   {!!Form::close()!!}


@stop

@section('script')
   @include('admin.companies.script')
@stop