<div class="col-sm-6">

	<div class="form-group">
		{!!Form::label('name','Parent Company Name',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7 @if ($errors->has('name')) has-error @endif">
			{!!Form::text('name',null,array('class'=>'form-control')) !!}
			@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('term_id','Terms',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('term_id',array(null=>'Please Select')+$terms,null,array('class'=>'form-control searchable_select')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('carrier_id','Preferred Carrier',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('carrier_id',array(null=>'Please Select')+$carriers,null,array('class'=>'form-control')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('shipper_no','Shipper Number',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::text('shipper_no',null,array('class'=>'form-control')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('time_zone_id','Time Zone',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::select('time_zone_id',array(null=>'Please Select')+$time_zones,null,array('class'=>'form-control')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('website','Website',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::text('website',null,array('class'=>'form-control')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('notes','Notes',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			{!!Form::textarea('notes',null,array('class'=>'form-control', 'rows' => 3)) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('active','Active',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			<div class="make-switch" data-on="success" data-off="danger" data-on-label="Yes" data-off-label="No">
				{!!Form::checkbox('active',1,true,array('class'=>'checkbox')) !!}
			</div>
		</div>
	</div>

	<div class="form-group">
		<div class="col-xs-offset-5 col-xs-7">

			{!! Form::submit($submit_btn_text,['class'=>'btn btn-primary']) !!}

			@if(Request::is('admin/companies/*/edit'))
				<a href="{{ URL::route('admin.companies.show', $company->id) }}" class="btn btn-default"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
			@elseif(Request::is('admin/companies/create'))
				<a href="{{ URL::route('admin.companies.index') }}" class="btn btn-default"><i class="glyphicon glyphicon-remove"></i> Cancel</a>
			@endif


		</div>

	</div>

</div>




