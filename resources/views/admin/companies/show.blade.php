@extends('admin.layout')
@section('title',$company->name)

@section('main')
    <h1 class="page-header">{{ $company->name }}
        <a href="{{ URL::route('admin.companies.edit',$company->id) }}" class="btn btn-sm btn-info pull-right"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
    </h1>

    <div class="form-group">
        @include('partial.form_error')
    </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a></li>
        <li role="presentation" ><a href="#locations" aria-controls="locations" role="tab" data-toggle="tab">Locations ({{$company->locations->count()}})</a></li>
        <li role="presentation" ><a href="#address" aria-controls="address" role="tab" data-toggle="tab">Address ({{$company->addresses->count()}})</a></li>
    </ul>


    <div class="tab-content">

        <div role="tabpanel" class="tab-pane active" id="details">
            <table class="table table-striped">

                <tr>
                    <td>Term</td>
                    <td>
                        @if($company->term)
                            {{$company->term->name}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Preferred Carrier</td>
                    <td>
                        @if($company->carrier)
                            {{$company->carrier->name}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Shipper #</td>
                    <td>{{$company->shipper_no}}</td>
                </tr>
                <tr>
                    <td>Time Zone</td>
                    <td>
                        @if($company->time_zone)
                            {{$company->time_zone->name}}
                        @endif
                    </td>
                </tr>
                <tr><td>Website</td><td>{{$company->website}}</td></tr>
                <tr>
                    <td>Notes</td>
                    <td>{{$company->notes}}</td>
                </tr>

                <tr><td>Status</td><td>{{$company->active ? 'Active' :'InActive'}}</td></tr>
            </table>
        </div>

        <div role="tabpanel" class="tab-pane" id="locations">
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>SL</th>
                    <th>Location Name</th>
                </tr>
                </thead>

                <tbody>

                @foreach($company->locations as $index=>$location)
                    <tr>
                        <td>{{$index+1}}</td>
                        <td><a href="/admin/locations/{{$location->id}}">{{$location->name}}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div role="tabpanel" class="tab-pane" id="address">

            @include('admin.address.index',[
                'path'=>'admin/companies/'.$company->id.'/address#address',
                'addresses' => $company->addresses

                ])

        </div>

    </div>

    @include('admin.address.modal')


@stop

@section('script')
    @include('admin.companies.script')
    @include('admin.address.script')
@stop