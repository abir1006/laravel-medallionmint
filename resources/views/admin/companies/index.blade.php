@extends('admin.layout')
@section('title','Parent Companies')

@section('main')

   <h1 class="page-header">Parent Companies
	   <a href="{{URL::to('admin/companies/create')}}" class="btn btn-sm btn-primary pull-right">
		   <i class="glyphicon glyphicon-plus"></i> Add New Parent Company
	   </a>
   </h1>

   @if(!count($companies))

           <p class="alert alert-warning">
               No parent companies found! Please
               <a href="{{ url('admin/companies/create')}}">create a parent company</a>
           </p>
   @else

   	<?php $sl = 1; ?>
   	<table class="table table-striped table-bordered table_sortable" id="parent_companies_index">
        <thead>
            <tr><th>SL</th><th>Parent Company Name</th><th>Locations</th><th>Machines</th><th>Action</th></tr>
        </thead>
	   	@foreach($companies as $index=>$company)
	   	<tr>
	   		<td>{{1+$index++}}</td>
	   		<td>
	   			<a href="{{ URL::route('admin.companies.show',$company->id) }}">{{$company->name}}</a>
	   			
	   		</td>	
	   		<td>{{ $company->locations->count() }}</td>
	   		<td>{{ $company->machines->count() }}</td>

	   		<td>
	   			
	   			<a href="{{ URL::route('admin.companies.edit',$company->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;&nbsp;
				<a title="delete" data-url="{{url('admin/companies/'.$company->id)}}" data-token="{{csrf_token()}}" class="admin_company_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a> 

	   		</td>
	   	</tr>
	   	@endforeach
   	</table>

   @endif

@stop

@section('script')
  @include('admin.companies.script')
@stop
