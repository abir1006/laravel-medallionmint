@extends('admin.layout')
@section('title','Edit '.$company->name)

@section('main')
   <a class="btn btn-sm btn-info pull-right" href="{{URL::route('admin.companies.show',$company->id)}}">
      <i class="glyphicon glyphicon-eye-open"></i> View Details
   </a>
   <h1 class="page-header">
      Edit {{ $company->name }}
   </h1>
      @include('partial.form_error')

   {!! Form::model($company,['method'=>'PATCH','url' => ['admin/companies',$company->id ],'files'=>'true', 'class'=>'row form-horizontal edit']) !!}

      @include('admin.companies.form',['submit_btn_text'=>'Update Parent Company'])

   {!!Form::close()!!}

@stop


@section('script')
   @include('admin.companies.script')
@stop