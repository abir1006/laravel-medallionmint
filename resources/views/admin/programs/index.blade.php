@extends('admin.layout')

@section('main')
	<h1 class="page-header">Programs
		<a href="{{URL::to('admin/programs/create')}}" class="btn btn-sm btn-primary pull-right">
			<i class="glyphicon glyphicon-plus"></i> Add New Program
		</a>
	</h1>
	@if(!count($oPrograms))
		<div class="col-sm-4">
			<p class="alert alert-warning">
				No Program found! Please
				<a href="{{ url('admin/programs/create')}}">create a program</a>
			</p>
		</div>
	@else
		<?php $sl = 1; ?>
		<table class="table table-striped table-bordered table_sortable" id="programs_index">
			<thead>
			<tr><th>SL</th><th>Program Name</th><th>Value</th><th>Locations</th><th>Action</th></tr>
			</thead>
			@foreach($oPrograms as $oProgram)
				<tr>
					<td>{{$sl++}}</td>
					<td>
						{{$oProgram->name}}
					</td>
					<td>
						{{$oProgram->value}}
					</td>
					<td>0</td>
					<td>
						<a href="{{ URL::route('admin.programs.edit',$oProgram->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;
						<a title="delete" data-url="{{url('admin/programs/'.$oProgram->id)}}" data-token="{{csrf_token()}}" class="admin_program_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>
					</td>
				</tr>
			@endforeach
		</table>

	@endif


@stop

@section('script')
	@include('admin.programs.script')
@endsection