@extends('admin.layout')

@section('main')
	<h1 class="page-header">Edit {{ $oProgram->name }}</h1>
	@include('partial.form_error')

	{!! Form::model($oProgram,['method'=>'PATCH','url' => ['admin/programs',$oProgram->id ],'files'=>'true', 'class'=>'row form-horizontal edit']) !!}

	@include('admin.programs.form',['submit_btn_text'=>'Update Program'])

	{!!Form::close()!!}

	@include('admin.programs.script')
@stop