@extends('admin.layout')

@section('main')
	<h1 class="page-header">Add New Program</h1>
	@include('partial.form_error')

	{!! Form::open(['url'=>'admin/programs','class'=>'row form-horizontal']) !!}

	@include('admin.programs.form',['submit_btn_text'=>'Add New Program'])

	{!!Form::close()!!}

	@include('admin.programs.script')
@stop