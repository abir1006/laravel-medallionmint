<div class="modal fade" id="address-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            {!! Form::open(['class'=>'address-form form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Address</h4>
            </div>

            <div class="modal-body">

                <div class="form-group row">

                    {!!Form::label('type','Type',array('class'=>'col-sm-4 control-label'))!!}

                    <div class="col-sm-8">
                        {!!Form::select('type',[''=>'Please Select','Billing'=>'Billing','Shipping'=>'Shipping','Other'=>'Other'],null,array('class'=>'form-control')) !!}
                    </div>

                </div>

                <div class="form-group row">

                    {!!Form::label('contact','Contact Name',array('class'=>'col-sm-4 control-label'))!!}

                    <div class="col-sm-8">
                        {!!Form::text('contact',null,array('class'=>'form-control')) !!}
                    </div>

                </div>

                <div class="form-group row">

                    {!!Form::label('address_line_1','Address Line 1',array('class'=>'col-sm-4 control-label'))!!}

                    <div class="col-sm-8">
                        {!!Form::text('address_line_1',null,array('class'=>'form-control')) !!}
                    </div>

                </div>

                <div class="form-group row">

                    {!!Form::label('address_line_2','Address Line 2',array('class'=>'col-sm-4 control-label'))!!}

                    <div class="col-sm-8">
                        {!!Form::text('address_line_2',null,array('class'=>'form-control')) !!}
                    </div>
                </div>

                <div class="form-group row">

                    {!!Form::label('city','City',array('class'=>'col-sm-4 control-label'))!!}

                    <div class="col-sm-8">
                        {!!Form::text('city',null,array('class'=>'form-control')) !!}
                    </div>
                </div>

                <div class="form-group row">

                    {!!Form::label('state','State',array('class'=>'col-sm-4 control-label'))!!}

                    <div class="col-sm-8">
                        {!!Form::text('state',null,array('class'=>'form-control')) !!}
                    </div>
                </div>

                <div class="form-group row">

                    {!!Form::label('zip','ZIP',array('class'=>'col-sm-4 control-label'))!!}

                    <div class="col-sm-8">
                        {!!Form::text('zip',null,array('class'=>'form-control')) !!}
                    </div>
                </div>

                <div class="form-group row">

                    {!!Form::label('country','Country',array('class'=>'col-sm-4 control-label'))!!}

                    <div class="col-sm-8">
                        {!!Form::text('country',null,array('class'=>'form-control')) !!}
                    </div>
                </div>

                <div class="form-group row">

                    {!!Form::label('phone','Phone',array('class'=>'col-sm-4 control-label'))!!}

                    <div class="col-sm-8">
                        {!!Form::text('phone',null,array('class'=>'form-control')) !!}
                    </div>
                </div>

                <div class="form-group row">

                    {!!Form::label('fax','Fax',array('class'=>'col-sm-4 control-label'))!!}

                    <div class="col-sm-8">
                        {!!Form::text('fax',null,array('class'=>'form-control')) !!}
                    </div>
                </div>

                <div class="form-group row">
                    {!!Form::label('time_zone_id','Time Zone',array('class'=>'col-sm-4 control-label'))!!}
                    <div class="col-sm-8">
                        {!!Form::select('time_zone_id',array(null=>'Please Select')+$time_zones,null,array('class'=>'form-control')) !!}
                    </div>
                </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
            </div>


            {!! Form::close() !!}

        </div>
    </div>
</div>