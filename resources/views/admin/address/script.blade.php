<script type="text/javascript">

    $(document).ready(function(){


        var validator = $('form.address-form').validate({
            rules:{
                type:'required',
                address_line_1:'required',
                city:'required',
                state:'required',
                zip:'required',
                country:'required',
                phone:'required'
            }
        });

    /*    $('#address-modal').on('hide.bs.modal',function(){

            $('.address-form')[0].reset();
            validator.resetForm();
        });*/

        $('.address-add-btn').click(function(e){
            e.preventDefault();

            $('.address-form')[0].reset();
            validator.resetForm();


            var url = $(this).data('url');
            $('.address-form').attr('action',url);


            $('.address-form input[name=_method]').remove();


        });

        $('.address-edit-btn').click(function(e){
            e.preventDefault();

            $('.address-form')[0].reset();
            validator.resetForm();

            var url = $(this).data('url');


            $('.address-form').attr('action',url);

            $('.address-form').append('<input type="hidden" name="_method" value="PATCH">');


            $.getJSON( url, function( data ) {

                $.each( data, function( key, val ) {
                    $('.address-form').find('#'+key).val(val);
                });

            });

        });


        $('.address-del-btn').click(function(e){
            e.preventDefault();


            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this address ? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Address',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).closest('.address').css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $(target).closest('.address').fadeOut(500,function(){
                                    $(target).closest('.address').remove();
                                });

                                alertify.success('Address has been deleted');

                            }else{

                                $(target).removeClass('disabled');
                                $(target).closest('.address').css('opacity','1');
                                alertify.error('Failed to delete address. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });


    });
</script>