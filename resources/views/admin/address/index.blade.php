<p>
    <button type="button" class="btn btn-info btn-sm address-add-btn"
            data-toggle="modal"
            data-target="#address-modal"
            data-url="{{url($path)}}">
        <i class="glyphicon glyphicon-plus"></i> Add New Address
    </button>
</p>

<div class="row">

    @foreach($addresses as $index=>$address)
        <div class="col-sm-4 address">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-striped first-last-auto">
                        <tr><td>Type</td><td>{{$address->type}}</td></tr>
                        <tr><td>Contact</td><td>{{$address->contact}}</td></tr>
                        <tr><td>Address Line 1</td><td>{{$address->address_line_1}}</td></tr>
                        <tr><td>Address Line 2</td><td>{{$address->address_line_2}}</td></tr>
                        <tr><td>City</td><td>{{$address->city}}</td></tr>
                        <tr><td>State</td><td>{{$address->state}}</td></tr>
                        <tr><td>Zip</td><td>{{$address->zip}}</td></tr>
                        <tr><td>Country</td><td>{{$address->country}}</td></tr>
                        <tr><td>Phone</td><td>{{$address->phone}}</td></tr>
                        <tr><td>Fax</td><td>{{$address->fax}}</td></tr>
                        <tr><td>Time Zone</td><td>{{$address->time_zone?$address->time_zone->name.' ( '.$address->time_zone->value.' hours )' : ''}}</td></tr>
                    </table>

                    <a href="#" class="btn btn-xs btn-default address-edit-btn" data-toggle="modal" data-target="#address-modal" data-url="{{url('admin/address/'.$address->id)}}#address" >
                        <i class="glyphicon glyphicon-edit"></i> Edit
                    </a>

                    <a title="delete" data-url="{{url('admin/address/'.$address->id)}}#address" data-token="{{csrf_token()}}" class="address-del-btn btn btn-xs btn-warning" href="#">
                        <i class="glyphicon glyphicon-trash"></i> Delete
                    </a>

                </div>
            </div>

        </div>

    @endforeach
</div>