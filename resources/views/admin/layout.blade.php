<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/favicon.ico">

  <title>@yield('title')</title>

  @yield('stylesheet')

  <link href="/lib/bootstrap-3.3.2-dist/css/bootstrap.css" rel="stylesheet">
  <link href="/lib/bootstrap-3.3.2-dist/css/bootstrap-theme.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.css"/>

  <link href="/lib/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet">
  <link href="/lib/bootstrap-clockpicker/bootstrap-clockpicker.css" rel="stylesheet">
  <link href="/lib/alertify/alertify.min.css" rel="stylesheet">
  <link href="/lib/alertify/alertify.default.css" rel="stylesheet">


  <link href="/lib/chosen_v1.4.0/chosen.css" rel="stylesheet">
  <link rel="stylesheet" href="/css/sidebar-nav.css">
  <link href="/css/style.css" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

  {{--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap3/bootstrap-switch.css">--}}
  <link rel="stylesheet" href="/lib/bootstrap-switch-3/bootstrap-switch.css">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>


  {{--
  http://bootsnipp.com/snippets/featured/responsive-navigation-menu
  --}}

  <div class="nav-side-menu">



    <div class="brand"><img src="/images/logo_transparent.png" alt=""></div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list viewport">

      <ul id="menu-content" class="menu-content collapse in">

        <li {!!(Request::is('admin/profile') ? 'class="active"' : '')!!}>
          <a href="/admin/profile"><i class="fa fa-user fa-lg"></i> {{Auth::user()->name}}</a>
        </li>

        <li {!! (Request::is('admin/dashboard') ? 'class="active"' : '' ) !!}>
          <a href="/admin/dashboard"><i class="glyphicon glyphicon-dashboard"></i>  Dashboard</a>
        </li>

        <li {!! (Request::is('admin/users*') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/users')}}"><i class="glyphicon glyphicon-user"></i>
            Users <span class="badge user">{{App\User::where('active','=','1')->count()}}</span>
          </a>
        </li>


        <li  data-toggle="collapse" data-target="#companies" class="collapsed">
          <a href="#"><i class="fa fa-home fa-lg"></i> Companies <span class="arrow"></span></a>
        </li>

        <ul class="sub-menu collapse" id="companies">

          <li {!! (Request::is('admin/companies*') ? 'class="active"' : '' ) !!} >
            <a href="{{url('admin/companies')}}">
              Parent Companies <span class="badge company">{{App\Company::where('active','=','1')->count()}}</span>
            </a>
          </li>

          <li {!! (Request::is('admin/rep_companies*') ? 'class="active"' : '' ) !!} >
            <a href="{{url('admin/rep_companies')}}">
              Rep Companies <span class="badge rep_company">{{App\RepCompany::where('active','=','1')->count()}}</span>
            </a>
          </li>

          <li {!! (Request::is('admin/managing_companies*') ? 'class="active"' : '' ) !!} >
            <a href="{{url('admin/managing_companies')}}">
              Managing Companies <span class="badge managing_company">{{App\ManagingCompany::where('active','=','1')->count()}}</span>
            </a>
          </li>

        </ul>



        <li {!! (Request::is('admin/locations*') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/locations')}}"><i class="glyphicon glyphicon-map-marker"></i>
            Locations <span class="badge location">{{App\Location::where('active','=','1')->count()}}</span>
          </a>
        </li>


        <li {!! (Request::is('admin/carriers*') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/carriers')}}"><i class="glyphicon glyphicon-envelope"></i>
            Carriers <span class="badge carrier">{{App\Carrier::where('active','=','1')->count()}}</span>
          </a>
        </li>

        <li {!! (Request::is('admin/programs*') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/programs')}}"><i class="glyphicon glyphicon-th-list"></i>
            Programs <span class="badge program">{{App\Program::where('active','=','1')->count()}}</span>
          </a>
        </li>

        <li {!! (Request::is('admin/machines*') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/machines')}}"><i class="glyphicon glyphicon-film"></i>
            Machines <span class="badge machines">{{App\Machine::all()->count()}}</span>
          </a>
        </li>

        <li {!! (Request::is('admin/machine_settings') ? 'class="active"' : '')!!} >
          <a href="{{url('admin/machine_settings')}}"><i class="glyphicon glyphicon-cog"></i> Machine Settings</a>
        </li>


        <li  data-toggle="collapse" data-target="#parts" class="collapsed">
          <a href="#"><i class="fa fa-gift fa-lg"></i> Machine Parts <span class="arrow"></span></a>
        </li>

        <ul class="sub-menu collapse" id="parts">
          <li {!! (Request::is('admin/gizmos*') ? 'class="active"' : '')!!}><a href="{{url('admin/gizmos')}}">Gizmos</a></li>
          <li {!! (Request::is('admin/meter_boxes*') ? 'class="active"' : '')!!}><a href="{{url('admin/meter_boxes')}}">Meter Boxes</a></li>
          <li {!! (Request::is('admin/cc_modems*') ? 'class="active"' : '')!!}><a href="{{url('admin/cc_modems')}}">CC Modems</a></li>
          <li {!! (Request::is('admin/cc_readers*') ? 'class="active"' : '')!!}><a href="{{url('admin/cc_readers')}}">CC Readers</a></li>
          <li {!! (Request::is('admin/bill_validators*') ? 'class="active"' : '')!!}><a href="{{url('admin/bill_validators')}}">Bill Validators</a></li>
          <li {!! (Request::is('admin/vaults*') ? 'class="active"' : '')!!}><a href="{{url('admin/vaults')}}">Vaults</a></li>
          <li {!! (Request::is('admin/hoppers/*') ? 'class="active"' : '')!!}><a href="{{url('admin/hoppers')}}">Hoppers</a></li>
          <li {!! (Request::is('admin/touch_screens*') ? 'class="active"' : '')!!}><a href="{{url('admin/touch_screens')}}">Touch Screens</a></li>
        </ul>


        <li {!! (Request::is('admin/coins*') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/coins')}}"><i class="glyphicon glyphicon-copyright-mark"></i>
            Coins <span class="badge coins badge-info pull-right">{{App\Coin::all()->count()}}</span>
          </a>
        </li>


        <li {!! (Request::is('admin/dies*') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/dies')}}"><i class="glyphicon glyphicon-record"></i>
            Dies <span class="badge dies badge-info pull-right">{{App\Dies::all()->count()}}</span>
          </a>
        </li>

        <li {!! (Request::is('admin/device_health') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/device_health')}}"><i class="glyphicon glyphicon-filter"></i>Device Health</a>
        </li>

        <li {!! (Request::is('admin/transactions') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/transactions')}}"><i class="glyphicon glyphicon-usd"></i>Transactions</a>
        </li>

        <li {!! (Request::is('admin/device_alerts') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/device_alerts')}}"><i class="glyphicon glyphicon-exclamation-sign"></i>Device Alerts</a>
        </li>

        <li {!! (Request::is('admin/hoppers_report') ? 'class="active"' : '' ) !!} >
          <a href="{{url('admin/hoppers_report')}}"><i class="glyphicon glyphicon-transfer"></i>Hoppers Report</a>
        </li>


        <li  data-toggle="collapse" data-target="#test" class="collapsed">
          <a href="#"><i class="fa fa-user-md fa-lg"></i> Test <span class="arrow"></span></a>
        </li>

        <ul class="sub-menu collapse" id="test">
          <li><a href="{{url('admin/duplicate_device_serial')}}" target="_blank">Duplicate Device Serial</a></li>
        </ul>

        <li><a href="/auth/logout"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>


      </ul>

    </div>

  </div>

  <div class="main">

    @yield('main')
  </div>




<script src="/lib/jquery-2.1.1.min.js"></script>

<script src="/lib/bootstrap-3.3.2-dist/js/bootstrap.js"></script>
<script src="https://cdn.datatables.net/v/bs/dt-1.10.12/datatables.min.js"></script>
<script src="/lib/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<script src="/lib/bootstrap-clockpicker/bootstrap-clockpicker.js"></script>
<script src="/js/bootbox-4.4.0.min.js"></script>
<script src="/lib/alertify/alertify.min.js"></script>
<script src="/js/holder.js"></script>


<!--
<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
-->

<script src="/lib/Highcharts/js/highcharts.js"></script>

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js" type="text/javascript"></script>--}}
<script src="/lib/bootstrap-switch-3/bootstrap-switch.js" type="text/javascript"></script>
<script src="/lib/geocomplete/jquery.geocomplete.min.js"></script>
<script src="/lib/chosen_v1.4.0/chosen.jquery.min.js"></script>
<script src="/lib/jquery.validate.min.js"></script>
<script src="/lib/moment.js"></script>

<script src="/js/custom.js"></script>


@if(Session::has('flash_message'))
  <?php echo "<script>alertify.success('". session('flash_message'). "');</script>"; ?>
@endif

@yield('script')


</body>
</html>