<script type="text/javascript">

    $(document).ready(function(){

        var validator = $('form.contract-form').validate({
            rules:{
                terms:{
                    required:true,
                    digits:true
                },
                start_date:{
                    required:true,
                    date:true
                },
                end_date:{
                    required:true,
                    date:true
                }
            }
        });

        // hide.bs.modal fires multiple times .. (conflict with address modal)

        $('.contract-add-btn').click(function(e){
            e.preventDefault();

            $('.contract-form')[0].reset();
            validator.resetForm();


            var url = $(this).data('url');
            $('.contract-form').attr('action',url);

            $('.address-form input[name=_method]').remove();

        });

        $('.contract-edit-btn').click(function(e){
            e.preventDefault();

            $('.contract-form')[0].reset();
            validator.resetForm();

            var url = $(this).data('url');


            $('.contract-form').attr('action',url);

            $('.contract-form').append('<input type="hidden" name="_method" value="PATCH">');


            $.getJSON( url, function( data ) {

                $.each( data, function( key, val ) {
                    if(key!= 'auto_renew'){
                        $('.contract-form').find('#'+key).val(val);
                    }
                });

                /*ohhh Yessss finally its fixed */

                if(data.auto_renew === '1'){

                    $('.contract-form').find('#auto_renew').prop('checked',true).parent().addClass('switch-on').removeClass('switch-off');

                }else{
                    $('.contract-form').find('#auto_renew').prop('checked',false).parent().addClass('switch-off').removeClass('switch-on');

                }

            });

        });

    });

</script>