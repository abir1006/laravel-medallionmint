<div class="modal fade" id="contract-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            {!! Form::open(['class'=>'contract-form form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Contract</h4>
            </div>

            <div class="modal-body">

                <div class="form-group row">

                    {!!Form::label('terms','Terms',array('class'=>'col-sm-4 control-label'))!!}


                    <div class="col-sm-8">
                        {!!Form::text('terms',null,array('class'=>'form-control')) !!}
                    </div>

                </div>

                <div class="form-group row">

                    {!!Form::label('start_date','Start Date',array('class'=>'col-sm-4 control-label'))!!}


                    <div class="col-sm-8">
                        {!!Form::text('start_date',null,array('class'=>'form-control datepicker')) !!}
                    </div>

                </div>

                <div class="form-group row">

                    {!!Form::label('end_date','End Date',array('class'=>'col-sm-4 control-label'))!!}


                    <div class="col-sm-8">
                        {!!Form::text('end_date',null,array('class'=>'form-control datepicker')) !!}
                    </div>

                </div>

                <div class="form-group row">

                    {!!Form::label('auto_renew','Auto Renew',array('class'=>'col-sm-4 control-label'))!!}


                    <div class="col-sm-8">
                        <div class="make-switch" data-on="primary" data-off="info" data-on-label="Yes" data-off-label="No">
                            {!!Form::checkbox('auto_renew',1,false,array('class'=>'checkbox')) !!}
                        </div>
                    </div>

                </div>


            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Submit',['class'=>'btn btn-primary']) !!}
            </div>


            {!! Form::close() !!}

        </div>
    </div>
</div>