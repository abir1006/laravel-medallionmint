@extends('admin.layout')

@section('title','Transactions')

@section('main')

    <h3 class="page-header">
        Transactions <small class="text-muted" title="m-d-Y">{{date('m-d-Y, h:i A')}}</small>
    </h3>

    <h4>Total for today {{date('m-d-Y')}}:
        <strong>{{$today_summary['total_transactions']}}</strong> Transactions,
        <strong>${{$today_summary['total_cc_amount']}}</strong> CC <span class="text-muted">({{$today_summary['cc_percent']}}%)</span>,
        <strong>${{$today_summary['total_cash_amount']}}</strong> Cash <span class="text-muted">({{$today_summary['cash_percent']}}%)</span>,
        <strong>{{$today_summary['total_coins']}}</strong> Coins <span class="text-muted">({{$today_summary['per_trans']}}/per trans)</span>,
    </h4>



    <table class="table table-bordered table-striped">

        <thead>
            <tr>
                <th>SL</th>
                <th>Reference</th>
                <th>Location</th>
                <th>Machine</th>
                <th>Coin</th>
                <th>Device</th>
                <th>Type</th>
                <th>Card Number</th>
                <th>Amount</th>
                <th>Time</th>
            </tr>
        </thead>

        <tbody>

            <?php $sl = $transactions->firstItem();?>

            @foreach($transactions as $index=>$transaction)

                <tr>
                    <td>{{$sl++}}</td>
                    <td>{{$transaction->transaction_ref}}</td>
                    <td>
                        @if(isset($transaction->machine->location))
                            <a href="{{URL::route('admin.locations.edit',$transaction->machine->location->id)}}">
                                {{$transaction->machine->location->name}}
                            </a>

                        @else
                            Not Found
                        @endif
                    </td>
                    <td>
                        @if(isset($transaction->machine))
                            <a href="{{URL::route('admin.machines.edit',$transaction->machine->id)}}">
                                {{$transaction->machine->name}}
                            </a>

                        @else
                            Not Found
                        @endif
                    </td>
                    <td>
                        @if($transaction->coin)
                            {{(int) $transaction->item_number.' '.$transaction->coin->name}}
                        @else
                            {{$transaction->item_number}}
                        @endif
                    </td>

                    <td>{{$transaction->device_serial}}</td>
                    <td>
                        @if($transaction->transaction_type == 'C')
                            <i class="fa fa-money text-primary" aria-hidden="true" title="Cash"></i>
                        @elseif($transaction->transaction_type == 'R')
                            <i class="fa fa-credit-card text-danger" aria-hidden="true" title="Credit Card "></i>
                        @elseif($transaction->transaction_type == 'P')
                            <i class="fa fa-user-md text-muted" aria-hidden="true" title="Test Pass"></i>
                        @endif
                    </td>
                    <td>{{$transaction->card_num}}</td>
                    <td>{{$transaction->total_amount}}</td>
                    <td>{{date('m-d-Y h:i A',strtotime($transaction->transaction_time))}}</td>
                </tr>

            @endforeach
        </tbody>
    </table>

    {!! $transactions->render() !!}


@stop

@section('script')

@stop