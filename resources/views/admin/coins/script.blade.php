<script type="text/javascript">
    $(document).ready(function() {


	    /**
         * delete coin from coins.show
         */

        $('.admin_coin_del_btn').click(function (e) {
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this Coin ? This action cannot be undone.').setting({
                'title': '<span class="glyphicon glyphicon-trash"></span> Delete Coin',
                'labels': {ok: 'DELETE'},
                'onok': function () {

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend: function () {

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity', '.5');

                        },
                        success: function (res) {

                            if (res == 'success') {

                                $('.nav-sidebar .badge.coins').html(parseInt($('.nav-sidebar .badge.coins').html()) - 1);

                                $(target).parent().parent().fadeOut(500, function () {
                                    $(target).parent().parent().remove();
                                });

                                alertify.success('Coin has been deleted');

                            } else {

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity', '1');
                                alertify.error('Failed to delete Coin. <br>' + res);
                            }


                        }
                    });


                },
                'oncancel': function () {


                }
            });
        });

        $('#metal').change(function () {
            var selected_metal = $(this).val();
            $('form .dies').removeClass('brass copper nickel_silver');

            if (selected_metal == 'Brass') {
                $('form .dies').addClass('brass');
            } else if (selected_metal == 'Copper') {
                $('form .dies').addClass('copper');
            } else if (selected_metal == 'Nickel Silver') {
                $('form .dies').addClass('nickel_silver');
            }

        });


        window.update_front_dies_list = function (location_id) {

            if ($('select#front_dies_type').val() == 'Stock Dies') {
                location_id = 0;
            }

            $.ajax({
                url: '/admin/get_dies/' + location_id,
                type: 'GET',
                beforeSend: function () {
                    $('select#front_dies').addClass('loading');
                },
                success: function (res) {
                    $('select#front_dies').removeClass('loading');
                    $('select#front_dies option').remove();
                    $('select#front_dies').append("<option value=''>Please Select</option>");

                    $.each(res, function (index, dies) {

                        $('<option/>', {
                            text: dies.name,
                            value: dies.id,
                            data: {
                                imagesrc: dies.photo
                            }
                        }).appendTo('select#front_dies');


                    });


                    var old_front_dies = '<?php echo \Input::old('front_dies');?>';
                    $('select#front_dies').val(old_front_dies);

                    if ($('select#front_dies').find(':selected').val() != '') {
                        $('.dies.front').html('<img alt=" " src="/upload/100-' + $('select#front_dies').find(':selected').data('imagesrc') + '">');
                    }


                }
            });
        }

        window.update_back_dies_list = function (location_id) {

            if ($('select#back_dies_type').val() == 'Stock Dies') {
                location_id = 0;
            }

            $.ajax({
                url: '/admin/get_dies/' + location_id,
                type: 'GET',
                beforeSend: function () {
                    $('select#back_dies').addClass('loading');
                },
                success: function (res) {
                    $('select#back_dies').removeClass('loading');
                    $('select#back_dies option').remove();
                    $('select#back_dies').append("<option value=''>Please Select</option>");


                    $.each(res, function (index, dies) {

                        $('<option/>', {
                            text: dies.name,
                            value: dies.id,
                            data: {
                                imagesrc: dies.photo

                            }
                        }).appendTo('select#back_dies');


                    });

                    var old_back_dies = '<?php echo \Input::old('back_dies');?>';
                    $('select#back_dies').val(old_back_dies);

                    if ($('select#back_dies').find(':selected').val() != '') {
                        $('.dies.back').html('<img alt=" " src="/upload/100-' + $('select#back_dies').find(':selected').data('imagesrc') + '">');
                    }

                }
            });


        }


        $('select#location_id').change(function () {
            var location_id = $('select#location_id').val();
            if (location_id != '') {
                update_front_dies_list(location_id);
                update_back_dies_list(location_id);
            }
        });

        $('select#front_dies_type').change(function () {
            var location_id = $('select#location_id').val();
            if (location_id != '') {
                update_front_dies_list(location_id);
            }
        });

        $('select#back_dies_type').change(function () {
            var location_id = $('select#location_id').val();
            if (location_id != '') {
                update_back_dies_list(location_id);
            }
        });

        $('select#front_dies').change(function () {

            if ($(this).find(':selected').val() != '') {
                $('.dies.front').html('<img src="/upload/100-' + $(this).find(':selected').data('imagesrc') + '">');
            }

        });

        $('select#back_dies').change(function () {

            if ($(this).find(':selected').val() != '') {
                $('.dies.back').html('<img src="/upload/100-' + $(this).find(':selected').data('imagesrc') + '">');
            }

        });
    });

</script>