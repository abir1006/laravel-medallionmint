@extends('admin.layout')

@section('main')
    <h1 class="page-header">Add New Coin</h1>
    <div class="row">
        {!! Form::open(['url'=>'admin/coins','class'=>'col-sm-12 col-lg-6']) !!}
        @include('admin.coins.form',['submit_btn_text'=>'Add New Coin'])
        {!! Form::close() !!}
    </div>

@stop

@section('script')
	@include('admin.coins.script')
@stop