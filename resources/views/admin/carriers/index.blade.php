@extends('admin.layout')

@section('main')
	<h1 class="page-header">Carriers
		<a href="{{URL::to('admin/carriers/create')}}" class="btn btn-sm btn-primary pull-right">
			<i class="glyphicon glyphicon-plus"></i> Add New Carrier
		</a>
	</h1>
	@if(!count($oCarriers))
		<div class="col-sm-4">
			<p class="alert alert-warning">
				No Carriers found! Please
				<a href="{{ url('admin/carriers/create')}}">create a carrier</a>
			</p>
		</div>
	@else
		<?php $sl = 1; ?>
		<table class="table table-striped table-bordered table_sortable" id="carriers_index">
			<thead>
			<tr><th>SL</th><th>Carrier Name</th><th>Type</th><th>Track Url</th><th>Rate Per Box</th><th>Action</th></tr>
			</thead>
			@foreach($oCarriers as $oCarrier)
				<tr>
					<td>{{$sl++}}</td>
					<td>{{$oCarrier->name}}</td>
					<td>{{$oCarrier->type}}</td>
					<td>{{$oCarrier->track_url}}</td>
					<td>{{$oCarrier->rate_per_box}}</td>
					<td>
						<a href="{{ URL::route('admin.carriers.edit',$oCarrier->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;
						<a title="delete" data-url="{{url('admin/carriers/'.$oCarrier->id)}}" data-token="{{csrf_token()}}" class="admin_carrier_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>
					</td>
				</tr>
			@endforeach
		</table>

	@endif


@stop

@section('script')
	@include('admin.carriers.script')
@endsection