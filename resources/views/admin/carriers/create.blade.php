@extends('admin.layout')

@section('main')
	<h1 class="page-header">Add New Carrier</h1>
	@include('partial.form_error')

	{!! Form::open(['url'=>'admin/carriers','class'=>'row form-horizontal']) !!}

	@include('admin.carriers.form',['submit_btn_text'=>'Add New Carrier'])

	{!!Form::close()!!}

	@include('admin.carriers.script')
@stop