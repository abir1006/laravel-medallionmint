<script>
    $(document).ready(function() {


	    /***
	     *Delete Carrier button
	     * */

	    $(document).on('click', '.admin_carrier_del_btn', function (e) {

		    e.preventDefault();

		    var target = $(this);
		    var url = $(this).data('url');
		    var token = $(this).data('token');

		    alertify.confirm('Are you sure you want to delete this carrier? This action cannot be undone.').setting({
			    'title': '<span class="glyphicon glyphicon-trash"></span> Delete Carrier',
			    'labels': {ok: 'DELETE'},
			    'onok': function () {

				    $.ajax({

					    url: url,
					    type: 'POST',
					    data: {_method: 'delete', _token: token},

					    beforeSend: function () {

						    $(target).addClass('disabled');
						    $(target).parent().parent().css('opacity', '.5');

					    },
					    success: function (res) {

						    if (res == 'success') {

							    $(target).parent().parent().fadeOut(500, function () {
								    $(target).parent().parent().remove();
							    });

							    alertify.success('Carrier has been deleted');
							    $('.nav-sidebar .badge.location').html(parseInt($('.nav-sidebar .badge.location').html()) - 1);

						    } else {

							    $(target).removeClass('disabled');
							    $(target).parent().parent().css('opacity', '1');
							    alertify.error('Failed to find carrier. <br>' + res);
						    }

					    }
				    });

			    },
			    'oncancel': function () {

			    }
		    });
	    });

    });
</script>