<div class="row">
	<div class="form-group">
		{!!Form::label('name','Carrier Name',array('class'=>'control-label col-xs-2'))!!}
		<div class="col-xs-4 @if ($errors->has('name')) has-error @endif">
			{!!Form::text('name',null,array('class'=>'form-control')) !!}
			@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('track_url','Track Url',array('class'=>'control-label col-xs-2'))!!}
		<div class="col-xs-4">
			{!!Form::text('track_url',null,array('class'=>'form-control')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('type','Type',array('class'=>'control-label col-xs-2'))!!}
		<div class="col-xs-4 @if ($errors->has('type')) has-error @endif">
			{!!Form::text('type',null,array('class'=>'form-control ')) !!}
		@if ($errors->has('type')) <p class="help-block">{{ $errors->first('type') }}</p> @endif
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('rate_per_box','Rate Per Box',array('class'=>'control-label col-xs-2'))!!}
		<div class="col-xs-4">
			{!!Form::text('rate_per_box',null,array('class'=>'form-control')) !!}
		</div>
	</div>

	<div class="form-group">
		{!!Form::label('active','Active',array('class'=>'control-label col-xs-5'))!!}
		<div class="col-xs-7">
			<div class="make-switch" data-on="primary" data-off="info" data-on-label="Yes" data-off-label="No">
				{!!Form::checkbox('active',1,true,array('class'=>'checkbox')) !!}
			</div>
		</div>
	</div>
</div>

<div class="form-group">
	<a href="{{ URL::route('admin.carriers.index') }}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancel</a>

	{!! Form::submit($submit_btn_text,['class'=>'btn btn-primary']) !!}

</div>