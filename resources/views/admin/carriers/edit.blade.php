@extends('admin.layout')

@section('main')
	<h1 class="page-header">Edit {{ $oCarrier->name }}</h1>
	@include('partial.form_error')

	{!! Form::model($oCarrier,['method'=>'PATCH','url' => ['admin/carriers',$oCarrier->id ],'files'=>'true', 'class'=>'row form-horizontal edit']) !!}

	@include('admin.carriers.form',['submit_btn_text'=>'Update Carrier'])

	{!!Form::close()!!}

	@include('admin.carriers.script')
@stop