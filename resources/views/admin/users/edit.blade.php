@extends('admin.layout')
@section('title','Edit '.$user->name)

@section('main')
    <h1 class="page-header">Edit {{$user->name}}</h1>
    
    @include('partial.form_error')

    <div class="row">
        {!! Form::model($user,['method'=>'PATCH','url' => ['admin/users',$user->id ], 'class'=>'col-sm-5']) !!}

        	        	
        	<div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('name','Full Name',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::text('name',null,array('class'=>'form-control','placeholder'=>'Full Name') ) !!}
	        	</div>
	        </div>

	        <div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('email','Email Address',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email') ) !!}
	        	</div>
	        </div>
        	
        	<div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('password','New Password',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::password('password',array('class'=>'form-control','placeholder'=>'Password') ) !!}
	        		<p class="help-block">Leave empty if you don't want to change the password</p>
	        	</div>
	        </div>

	        <div class="row form-group">
	        	<div class="col-sm-5">
					{!!Form::label('active','Active',array('class'=>'control-label'))!!}
				</div>

				<div class="col-sm-7">
					<div class="make-switch" data-on="primary" data-off="info" data-on-label="Yes" data-off-label="No">
						{!!Form::checkbox('active',1,true,array('class'=>'checkbox')) !!}
					</div>

				</div>

			</div>

			<div class="row form-group"></div>

	        	<div class="col-sm-offset-5 col-sm-7">
	            	{!! Form::submit('Update User',array('class'=>'btn btn-primary form-control'))  !!}
	        	</div>
	        </div>
        	
        {!! Form::close() !!}
    	
    </div>

@stop


@section('script')
    @include('admin.users.script')
@stop