@extends('admin.layout')
@section('title','New User')

@section('main')
    <h1 class="page-header"><i class="glyphicon glyphicon-user"></i> New User</h1>
    
    @include('partial.form_error')

    <div class="row">

        {!! Form::open(['url'=>'admin/users','class'=>'col-sm-5']) !!}
        	
        	<div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('name','Full Name',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::text('name',null,array('class'=>'form-control','placeholder'=>'Full Name') ) !!}
	        	</div>
	        </div>

	        <div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('role','Role',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::select('role',array(
	            		'location_manager' => 'Location Manager',
	            		'company_manager' => 'Company Manager',
	            		'admin'=>'Admininstrator'

	            		),null,array('class'=>'form-control') ) !!}
	        	</div>
	        </div>

        	<div class="row form-group location_selector_wrapper">
	        	<div class="col-sm-5">
	        		{!! Form::label('location_id','Select Location',array('class'=>'form-control-static')) !!}

	            	<a class="popover_link" title="Optional" 
	            		data-content="This field is optional, you can set location later."> 
	            		<i class="glyphicon text-muted glyphicon-info-sign"></i>
	            	</a>

	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::select('location_id[]',$locations,null,array('class'=>'form-control searchable_select','multiple'=>'multiple') ) !!}
	        	</div>
	        </div>

	        <div class="row form-group company_selector_wrapper">
	        	<div class="col-sm-5">
	        		{!! Form::label('company_id','Select Company',array('class'=>'form-control-static')) !!}	            	
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::select('company_id',[''=>'Select a Company']+$companies,null,array('class'=>'form-control searchable_select') ) !!}
	        	</div>
	        </div>
        	
        	

	        <div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('email','Email Address',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::text('email',null,array('class'=>'form-control','placeholder'=>'Email') ) !!}
	        	</div>
	        </div>
        	
        	<div class="row form-group">
	        	<div class="col-sm-5">
	        		{!! Form::label('password','New Password',array('class'=>'form-control-static')) !!}
	        	</div>

	        	<div class="col-sm-7">
	            	{!! Form::password('password',array('class'=>'form-control','placeholder'=>'Password') ) !!}
	        	</div>
	        </div>

	        <div class="row form-group">
	        	<div class="col-sm-5">
					{!!Form::label('active','Active',array('class'=>'control-label'))!!}
				</div>

				<div class="col-sm-7">

					<div class="make-switch" data-on="primary" data-off="info" data-on-label="Yes" data-off-label="No">
						{!!Form::checkbox('active',1,true,array('class'=>'checkbox')) !!}
					</div>

				</div>

			</div>

		    <div class="row form-group"></div>

	        	<div class="col-sm-offset-5 col-sm-7">
	            	{!! Form::submit('Create User',array('class'=>'btn btn-primary form-control'))  !!}
	        	</div>
	        </div>
        	
        {!! Form::close() !!}
    	
    </div>

@stop

@section('script')
    @include('admin.users.script')
@stop
