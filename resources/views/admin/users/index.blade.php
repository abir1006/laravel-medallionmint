@extends('admin.layout')
@section('title','All Users')

@section('main')
	<h1 class="page-header">Users
        <a href="{{URL::to('admin/users/create')}}" class="btn btn-sm btn-primary pull-right">
            <i class="glyphicon glyphicon-plus"></i> Add New User
        </a>
    </h1>

	<?php $sl = 1; ?>


    <table class="table table-striped table-bordered first-last-auto" id="users_table_index">
	    <thead>
    	    <tr><th>SL</th><th>Name</th><th>Email</th><th class="text-center">User ID</th><th>Role</th><th>Company/Location</th><th>Created At</th><th>Action</th></tr>
	    </thead>
    	@foreach($users as $user)
	    	<tr data-user-id="{{$user->id}}" class="{{($user->role=='company_manager' ? 'info ':'')}}">
                <td>{{$sl++}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td class="text-center">{{sprintf('%04d',$user->id)}}</td>
	    		<td>{{ucwords(str_replace('_',' ',$user->role))}}</td>
	    		<td class="locations">
	    			@if($user->role == 'admin')

	    			@elseif($user->role == 'company_manager')
	    				<ul class="list-unstyled no-margin">
	    				@foreach($user->companies as $company)
                            <li>
                                <i class="glyphicon glyphicon-home text-info"></i> <a href="{{url('admin/companies',$company->id)}}">{{$company->name}}</a>
                            </li>
                        @endforeach
                        </ul>

	    			@elseif($user->role == 'location_manager')
	    				<ul class="users_locations list-unstyled no-margin">
                        @foreach($user->locations as $location)
                            <li>
                                <a class="location" data-location-id="{{$location->id}}" href="{{url('admin/locations',$location->id)}}">{{$location->name}}</a>
                                <a title="Remove Access" data-url="{{url('admin/users/'.$user->id.'/remove/'.$location->id)}}" data-token="{{csrf_token()}}" class="admin_location_manager_remove_btn text-danger" href="#"><i class="glyphicon glyphicon-remove-circle"></i></a>

                            </li>
                        @endforeach
                        </ul>
	    			@endif
	    		</td>

	    		<td>{{$user->created_at->diffForHumans()}}</td>
	    		<td>

	    			<a title="Edit Basic Info" href="{{ URL::route('admin.users.edit',$user->id) }}" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i> Edit</a>&nbsp;
                    <a title="Delete Permanently" data-url="{{url('admin/users/'.$user->id)}}" data-token="{{csrf_token()}}" class="admin_user_del_btn btn btn-xs btn-danger" href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a>
                    @if($user->role == 'location_manager')
                        <a title="Manage Location Access" href="#"  data-toggle="modal" data-target="#manage_location_access_modal" class="manage_location_access_btn btn btn-xs btn-success"><i class="glyphicon glyphicon-wrench"></i> Manage</a>
                    @endif

	    		</td>
	    	</tr>
    	@endforeach
   	</table>


   	<div class="modal fade" id="manage_location_access_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="glyphicon glyphicon-wrench"></i> Manage Location Access</h4>
                </div>
                <div class="modal-body">
                	<div class="existing_locations">

                	</div>

                	<h3>Add more locations..</h3>
                	{!!Form::open(['url'=>'admin/users/attach','class'=>'manage_location_access_form row'])!!}
                		<input type="hidden" name="user_id" id="user_id">
                		<div class="col-sm-9">
                		{!!Form::select('location_id[]',$locations,null,array('class'=>'form-control location_selector searchable_select','multiple'=>'multiple','data-placeholder'=>'Select Locations','required'=>'required') )!!}
                		</div>

                		<div class="col-sm-3">
                		{!!Form::submit('Add',array('class'=>'form-control btn btn-primary'))!!}
                		</div>
                	{!!Form::close()!!}
                	<br>

                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@stop

@section('script')
    @include('admin.users.script')
@stop