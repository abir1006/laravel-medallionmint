@extends('admin.layout')

@section('main')
    <h1 class="page-header">Add New Dies</h1>

    <div class="row">

        {!! Form::open(['url'=>'admin/dies','files'=>'true','class'=>'col-sm-12 col-md-6 col-lg-5']) !!}

        @include('admin.dies.form',['submit_btn_text'=>'Add New Dies'])


        {!!Form::close()!!}
    </div>

@stop