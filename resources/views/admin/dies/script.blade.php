<script type="text/javascript">
    $(document).ready(function(){

	    $('#dies_table_index').dataTable({
		    "iDisplayLength": -1,
		    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
	    });

        $('.admin_dies_del_btn').click(function(e){
            e.preventDefault();

            var target = $(this);
            var url = $(this).data('url');
            var token = $(this).data('token');

            alertify.confirm('Are you sure you want to delete this Dies ? This action cannot be undone.').setting({
                'title':'<span class="glyphicon glyphicon-trash"></span> Delete Dies',
                'labels':{ok:'DELETE'},
                'onok': function(){

                    $.ajax({

                        url: url,
                        type: 'POST',
                        data: {_method: 'delete', _token: token},

                        beforeSend:function(){

                            $(target).addClass('disabled');
                            $(target).parent().parent().css('opacity','.5');

                        },
                        success: function (res) {

                            if(res=='success'){

                                $('.nav-sidebar .badge.dies').html(parseInt($('.nav-sidebar .badge.dies').html())-1);

                                $(target).parent().parent().fadeOut(500,function(){
                                    $(target).parent().parent().remove();
                                });

                                alertify.success('Dies has been deleted');

                            }else{

                                $(target).removeClass('disabled');
                                $(target).parent().parent().css('opacity','1');
                                alertify.error('Failed to delete dies. <br>'+res);
                            }


                        }
                    });


                },
                'oncancel':function(){


                }
            });
        });




    });
</script>
