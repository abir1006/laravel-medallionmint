
<div class="form-group">

    @include('partial.form_error')

</div>

<!--
<div class="form-group row">

	<div class="col-sm-12">
		{!!Form::checkbox('stock',1,null,array('id'=>'stock')) !!}
		{!!Form::label('stock','Stock Die',array('class'=>'') )!!}

	</div>

</div>
-->
@if(isset($dies))
    <div class="form-group row">
        <div class="col-sm-6">
            {!! Html::image('upload/200-'.$dies->photo,$dies->photo,array('class'=>'dies_photo')) !!}
        </div>
    </div>
@endif

<div class="form-group row">
    <div class="col-sm-6">
        {!! Form::label('name','Dies Name') !!}
        {!! Form::text('name',null,array('class'=>'form-control') ) !!}
    </div>

    <div class="col-sm-6">
        {!! Form::label('location_id','Select Die Location') !!}
        {!! Form::select('location_id',$locations,null,array('class'=>'form-control searchable_select') ) !!}
    </div>
</div>

<div class="form-group row">
    <div class="col-xs-3">
        {!! Form::label('cabinet','Cabinet') !!}
        {!! Form::select('cabinet',array(
        'A'=>'A',
        'B'=>'B',
        'C'=>'C',
        'D'=>'D'
        ),null,array('class'=>'form-control') ) !!}
    </div>

    <div class="col-xs-3">
        {!! Form::label('drawer','Drawer') !!}
        {!! Form::selectRange('drawer',1,8,null,array('class'=>'form-control')) !!}
    </div>

    <div class="col-xs-3">
        {!! Form::label('row','Row') !!}
        {!! Form::select('row',array(
        'A'=>'A',
        'B'=>'B',
        'C'=>'C',
        'D'=>'D',
        'E'=>'E',
        'F'=>'F',
        'G'=>'G'
        ),null,array('class'=>'form-control') ) !!}
    </div>

    <div class="col-xs-3">
        {!! Form::label('col','Col') !!}
        {!! Form::selectRange('col',1,6,null,array('class'=>'form-control')) !!}
    </div>

</div>

<div class="form-group row">
    <div class="col-sm-6">
        {!! Form::label('photo','Upload Dies Photo') !!}
        {!! Form::file('photo',array('class'=>'form-control')) !!}
    </div>

    <div class="col-sm-6">
        <br> <p class="text-muted"><i class="glyphicon glyphicon-info-sign"></i>
            Dies photo size should be 200x200 pixel</p>
    </div>
</div>

<div class="form-group">
    {!!Form::label('active','Active',array('class'=>'control-label'))!!}
    <div class="make-switch" data-on="primary" data-off="info" data-on-label="Yes" data-off-label="No">
        {!!Form::checkbox('active',1,true,array('class'=>'checkbox')) !!}
    </div>
</div>



<div class="form-group">
    <a href="{{ URL::route('admin.dies.index') }}" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancel</a>

    {!! Form::submit($submit_btn_text,['class'=>'btn btn-primary']) !!}

</div>
