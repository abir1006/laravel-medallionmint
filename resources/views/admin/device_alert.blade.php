@extends('admin.layout')

@section('main')

    <h3 class="page-header">Device Alerts</h3>

    @if($alerts)

        <table class="table table-bordered table-striped table_sortable">

            <thead>

            <tr>
                <th>SL</th>
                <th>Location</th>
                <th>Machine</th>
                <th>Device</th>
                <th>Alert</th>
                <th>Detected At</th>
            </tr>
            </thead>

            <tbody>
            @foreach($alerts as $index=>$alert)
                <tr class="{{ ($alert->alert_type=='error'?'danger ':'')  }} {{($alert->machine_id ? '':'warning')}}">
                    <td>{{$index+1}}</td>

                    @if($alert->machine_id)
                        <td><a href="/admin/locations/{{$alert->machine->location->id}}" >{{$alert->machine->location->name}}</a></td>
                        <td><a href="/admin/machines/{{$alert->machine->id}}" >{{$alert->machine->name}}</a></td>

                    @else
                        <td>Not Found</td>
                        <td>Not Found</td>
                    @endif

                    <td>{{$alert->device}}</td>
                    <td>{{$alert->alert}}</td>
                    <td>{{date('m-d-Y h:i A',strtotime($alert->detected_at))}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    @else

        <p class="alert alert-warning">No Device Alert Found!</p>
    @endif
@stop

@section('script')
    <script type="text/javascript">

        $(document).ready(function(){


        });
    </script>
@stop