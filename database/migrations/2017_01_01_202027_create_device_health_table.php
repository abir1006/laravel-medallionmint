<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceHealthTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('device_health')) {
            Schema::create('device_health', function (Blueprint $table) {
                $table->increments('id');

                $table->integer('machine_id')->unsigned()->nullable();

                $table->string('customer');
                $table->string('region');
                $table->string('asset');
                $table->string('location');
                $table->string('device');

                $table->integer('days_since_last_call_in');
                $table->dateTime('date_of_last_call_in');

                $table->integer('days_since_last_credit_tran');
                $table->dateTime('date_of_last_credit_tran');

                $table->integer('days_since_last_cash_tran');
                $table->dateTime('date_of_last_cash_tran');

                $table->integer('days_since_last_fill');
                $table->dateTime('date_of_last_fill');

                $table->integer('days_since_last_dex');
                $table->dateTime('date_of_last_dex');

                $table->string('status');
                $table->string('signal_strength');
                $table->string('business_type');

                $table->string('last_dex_alert');
                $table->string('firmware_version');
                $table->string('communication_method');

                $table->dateTime('hopper_1_last_transaction_date');
                $table->dateTime('hopper_2_last_transaction_date');
                $table->dateTime('hopper_3_last_transaction_date');
                $table->dateTime('hopper_4_last_transaction_date');
                $table->dateTime('hopper_5_last_transaction_date');
                $table->dateTime('hopper_6_last_transaction_date');
                $table->dateTime('hopper_7_last_transaction_date');
                $table->dateTime('hopper_8_last_transaction_date');


                $table->timestamps();

                //$table->foreign('machine_id')->references('id')->on('machines')->onDelete('set null');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('device_health');
    }
}
