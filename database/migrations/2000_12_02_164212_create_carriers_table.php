<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carriers', function (Blueprint $table) {
	        $table->increments('id');
	        $table->boolean('active')->default(1);
	        $table->string('name', 50)->required();
	        $table->string('slug', 30)->required();
	        $table->string('track_url');
	        $table->string('type');
	        $table->decimal('rate_per_box', 10, 2);
	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('carriers');
    }
}
