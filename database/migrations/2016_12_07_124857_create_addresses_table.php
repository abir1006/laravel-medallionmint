<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {

            $table->increments('id');

            $table->string('type');
            $table->string('contact');

            $table->string('address_line_1');
            $table->string('address_line_2');

            $table->string('city');
            $table->string('state');
            $table->string('zip');
            $table->string('country');

            $table->string('phone');
            $table->string('fax');

            $table->integer('addressable_id');
            $table->string('addressable_type');

            $table->timestamps();

            $table->integer('time_zone_id')->unsigned()->nullable();
            $table->foreign('time_zone_id')->references('id')->on('time_zones')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('addresses');
    }
}
