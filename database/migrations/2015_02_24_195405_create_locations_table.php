<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locations', function(Blueprint $table) {
			$table->increments('id');


            $table->string('name')->required();
            $table->integer('company_id')->unsigned()->nullable();
            $table->integer('rep_company_id')->unsigned()->nullable();
            $table->integer('managing_company_id')->unsigned()->nullable();



            $table->string('corporate_shipper_no');

            //$table->smallInteger('managing_type_id')->nullable();


			$table->boolean('has_custom_cc_percent')->default(0);
            $table->decimal('custom_cc_percent', 5, 2);

            $table->boolean('has_seasons')->default(0);
            $table->date('season_start_at');
            $table->date('season_end_at');

            $table->string('sales_tax_rate');
			$table->boolean('has_tax_stamp')->default(0);
			$table->integer('est_guests');
            $table->string('website');

            $table->integer('carrier_id')->unsigned()->nullable();
            $table->integer('location_type_id')->unsigned()->nullable();
            $table->integer('mm_rep_id')->unsigned()->nullable();
            $table->integer('program_id')->unsigned()->nullable();
            $table->integer('sales_type_id')->unsigned()->nullable();
            $table->integer('split_id')->unsigned()->nullable();
            $table->integer('term_id')->unsigned()->nullable();
            $table->integer('time_zone_id')->unsigned()->nullable();

            //$table->boolean('is_parent')->default(1);

            $table->text('notes');
            $table->boolean('active')->default(1);
            $table->boolean('is_exclude')->default(0);

            $table->integer('default_photo_id')->unsigned();

            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('set null');
            $table->foreign('rep_company_id')->references('id')->on('rep_companies')->onDelete('set null');
            $table->foreign('managing_company_id')->references('id')->on('managing_companies')->onDelete('set null');

            $table->foreign('carrier_id')->references('id')->on('carriers')->onDelete('set null');
            $table->foreign('location_type_id')->references('id')->on('location_types')->onDelete('set null');
            $table->foreign('mm_rep_id')->references('id')->on('mm_reps')->onDelete('set null');
            $table->foreign('program_id')->references('id')->on('programs')->onDelete('set null');
            $table->foreign('sales_type_id')->references('id')->on('sales_types')->onDelete('set null');
            $table->foreign('split_id')->references('id')->on('splits')->onDelete('set null');
            $table->foreign('term_id')->references('id')->on('terms')->onDelete('set null');
            $table->foreign('time_zone_id')->references('id')->on('time_zones')->onDelete('set null');



        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}

}
