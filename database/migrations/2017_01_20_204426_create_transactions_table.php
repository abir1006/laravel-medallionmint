<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('transactions')) {

            Schema::create('transactions', function (Blueprint $table) {

                $table->increments('id');

                $table->integer('machine_id')->unsgined()->nullable();
                $table->integer('coin_id')->unsigned()->nullable();

                $table->string('device_serial');
                $table->string('transaction_ref');
                $table->char('transaction_type', 4);
                $table->string('card_num');
                $table->float('total_amount');

                $table->string('item_number');
                $table->smallInteger('items');

                $table->timestamp('transaction_time');

                $table->timestamps();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('transactions');
    }
}
