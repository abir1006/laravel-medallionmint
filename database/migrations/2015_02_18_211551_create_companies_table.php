<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table)
		{

            $table->increments('id');
            $table->string('name');
            $table->string('website');

            $table->integer('term_id')->unsigned()->nullable();
            $table->integer('time_zone_id')->unsigned()->nullable();
            $table->integer('carrier_id')->unsigned()->nullable();
            $table->string('shipper_no');

            $table->text('notes');
            $table->boolean('active')->default(1);
            $table->timestamps();

            $table->foreign('term_id')->references('id')->on('terms')->onDelete('set null');
            $table->foreign('time_zone_id')->references('id')->on('time_zones')->onDelete('set null');
            $table->foreign('carrier_id')->references('id')->on('carriers')->onDelete('set null');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
