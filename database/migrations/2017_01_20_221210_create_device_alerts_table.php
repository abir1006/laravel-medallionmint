<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('device_alerts')) {

            Schema::create('device_alerts', function (Blueprint $table) {

                $table->increments('id');

                $table->integer('machine_id')->unsigned()->nullable();

                $table->string('device', 20);
                $table->text('alert');
                $table->string('alert_type', 20);
                $table->timestamp('detected_at');
                $table->timestamp('reported_at');

                $table->timestamps();

                //$table->foreign('machine_id')->references('id')->on('machines')->onDelete('cascade');

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::drop('device_alerts');
    }
}
