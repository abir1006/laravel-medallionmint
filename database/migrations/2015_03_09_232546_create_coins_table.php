<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('coins', function(Blueprint $table)
		{
            $table->increments('id');
            $table->boolean('active')->default(1);
            $table->string('name');
            $table->integer('location_id')->unsigned();
            $table->integer('top_inventory');
            $table->integer('inventory');
            $table->string('metal');
            $table->string('finish');
            $table->string('front_dies_type');
            $table->string('back_dies_type');
            $table->integer('front_dies');
            $table->integer('back_dies');
            $table->text('notes');
            $table->timestamps();

            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('coins');
	}

}
