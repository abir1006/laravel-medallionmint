<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('machines', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->boolean('active')->default(1);

			$table->integer('location_id')->unsigned();
			$table->string('name');
			$table->integer('default_photo_id')->unsigned();
			$table->string('specific_location');
			$table->string('asset_num_1');
            $table->string('asset_num_2');
            $table->date('date_shipped');
            $table->date('date_installed');

            $table->integer('machine_types')->unsigned()->nullable();
            $table->integer('machine_colors')->unsigned()->nullable();
            $table->integer('machine_keys')->unsigned()->nullable();
            $table->integer('prices')->unsigned()->nullable();
            $table->integer('motors')->unsigned()->nullable();
            $table->integer('button_lights')->unsigned()->nullable();


            $table->integer('touch_screen_id')->unsigned()->nullable();
            $table->integer('touch_screen_fws');

            /*Coins*/
            $table->integer('coins');
            $table->integer('coin_1_id')->unsigned()->nullable();
            $table->integer('coin_2_id')->unsigned()->nullable();
            $table->integer('coin_3_id')->unsigned()->nullable();
            $table->integer('coin_4_id')->unsigned()->nullable();
            $table->integer('coin_5_id')->unsigned()->nullable();
            $table->integer('coin_6_id')->unsigned()->nullable();
            $table->integer('coin_7_id')->unsigned()->nullable();
            $table->integer('coin_8_id')->unsigned()->nullable();

            /*Gizmo*/
            $table->integer('gizmo_id')->unsigned()->nullable();
            $table->integer('meter_box_id')->unsigned()->nullable();

            $table->integer('gizmo_fws');
            $table->string('gizmo_pic');

            /*Credit Card*/
            $table->boolean('has_cc');
            $table->integer('cc_companies');
            $table->integer('cc_antennas');

            $table->integer('cc_modem_id')->unsigned()->nullable();
            $table->integer('cc_reader_id')->unsigned()->nullable();

            /*Bill Validator*/
            $table->integer('bill_validator_id')->unsigned()->nullable();
            $table->integer('vault_id')->unsigned()->nullable();

            $table->integer('vault_keys');

            /*Hopper*/
            $table->integer('hopper_1_id')->unsigned()->nullable();
            $table->integer('hopper_2_id')->unsigned()->nullable();
            $table->integer('hopper_3_id')->unsigned()->nullable();
            $table->integer('hopper_4_id')->unsigned()->nullable();
            $table->integer('hopper_5_id')->unsigned()->nullable();
            $table->integer('hopper_6_id')->unsigned()->nullable();
            $table->integer('hopper_7_id')->unsigned()->nullable();
            $table->integer('hopper_8_id')->unsigned()->nullable();



            /*Meters*/
            $table->integer('current_meter_1');
            $table->integer('current_meter_2');
            $table->integer('current_meter_3');
            $table->integer('current_meter_4');
            $table->integer('current_meter_5');
            $table->integer('current_meter_6');
            $table->integer('current_meter_7');
            $table->integer('current_meter_8');
            $table->integer('current_meter_9');
            $table->integer('current_meter_10');

            $table->integer('calculated_meter_1');
            $table->integer('calculated_meter_2');
            $table->integer('calculated_meter_3');
            $table->integer('calculated_meter_4');
            $table->integer('calculated_meter_5');
            $table->integer('calculated_meter_6');
            $table->integer('calculated_meter_7');
            $table->integer('calculated_meter_8');
            $table->integer('calculated_meter_9');
            $table->integer('calculated_meter_10');

            $table->integer('calculated_inventory_1');
            $table->integer('calculated_inventory_2');
            $table->integer('calculated_inventory_3');
            $table->integer('calculated_inventory_4');
            $table->integer('calculated_inventory_5');
            $table->integer('calculated_inventory_6');
            $table->integer('calculated_inventory_7');
            $table->integer('calculated_inventory_8');

            $table->integer('starting_meter_1');
            $table->integer('starting_meter_2');
            $table->integer('starting_meter_3');
            $table->integer('starting_meter_4');
            $table->integer('starting_meter_5');
            $table->integer('starting_meter_6');
            $table->integer('starting_meter_7');
            $table->integer('starting_meter_8');
            $table->integer('starting_meter_9');
            $table->integer('starting_meter_10');
            /*Shipped*/
            $table->integer('shipped_meter_1');
            $table->integer('shipped_meter_2');
            $table->integer('shipped_meter_3');
            $table->integer('shipped_meter_4');
            $table->integer('shipped_meter_5');
            $table->integer('shipped_meter_6');
            $table->integer('shipped_meter_7');
            $table->integer('shipped_meter_8');
            $table->integer('shipped_meter_9');
            $table->integer('shipped_meter_10');

            $table->integer('shipped_coin_1_id');
            $table->integer('shipped_coin_2_id');
            $table->integer('shipped_coin_3_id');
            $table->integer('shipped_coin_4_id');
            $table->integer('shipped_coin_5_id');
            $table->integer('shipped_coin_6_id');
            $table->integer('shipped_coin_7_id');
            $table->integer('shipped_coin_8_id');

            $table->integer('shipped_coin_1_quantity');
            $table->integer('shipped_coin_2_quantity');
            $table->integer('shipped_coin_3_quantity');
            $table->integer('shipped_coin_4_quantity');
            $table->integer('shipped_coin_5_quantity');
            $table->integer('shipped_coin_6_quantity');
            $table->integer('shipped_coin_7_quantity');
            $table->integer('shipped_coin_8_quantity');


            $table->dateTime('hopper_1_last_transaction_date');
            $table->dateTime('hopper_2_last_transaction_date');
            $table->dateTime('hopper_3_last_transaction_date');
            $table->dateTime('hopper_4_last_transaction_date');
            $table->dateTime('hopper_5_last_transaction_date');
            $table->dateTime('hopper_6_last_transaction_date');
            $table->dateTime('hopper_7_last_transaction_date');
            $table->dateTime('hopper_8_last_transaction_date');

			$table->timestamps();

			$table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');

            $table->foreign('machine_types')->references('id')->on('machine_settings')->onDelete('set null');
            $table->foreign('machine_colors')->references('id')->on('machine_settings')->onDelete('set null');
            $table->foreign('machine_keys')->references('id')->on('machine_settings')->onDelete('set null');
            $table->foreign('prices')->references('id')->on('machine_settings')->onDelete('set null');
            $table->foreign('motors')->references('id')->on('machine_settings')->onDelete('set null');
            $table->foreign('button_lights')->references('id')->on('machine_settings')->onDelete('set null');

            $table->foreign('gizmo_id')->references('id')->on('gizmos')->onDelete('set null');
            $table->foreign('meter_box_id')->references('id')->on('meter_boxes')->onDelete('set null');
            $table->foreign('cc_modem_id')->references('id')->on('cc_modems')->onDelete('set null');
            $table->foreign('cc_reader_id')->references('id')->on('cc_readers')->onDelete('set null');
            $table->foreign('bill_validator_id')->references('id')->on('bill_validators')->onDelete('set null');
            $table->foreign('vault_id')->references('id')->on('vaults')->onDelete('set null');
            $table->foreign('touch_screen_id')->references('id')->on('touch_screens')->onDelete('set null');

            $table->foreign('hopper_1_id')->references('id')->on('hoppers')->onDelete('set null');
            $table->foreign('hopper_2_id')->references('id')->on('hoppers')->onDelete('set null');
            $table->foreign('hopper_3_id')->references('id')->on('hoppers')->onDelete('set null');
            $table->foreign('hopper_4_id')->references('id')->on('hoppers')->onDelete('set null');
            $table->foreign('hopper_5_id')->references('id')->on('hoppers')->onDelete('set null');
            $table->foreign('hopper_6_id')->references('id')->on('hoppers')->onDelete('set null');
            $table->foreign('hopper_7_id')->references('id')->on('hoppers')->onDelete('set null');
            $table->foreign('hopper_8_id')->references('id')->on('hoppers')->onDelete('set null');

            $table->foreign('coin_1_id')->references('id')->on('coins')->onDelete('set null');
            $table->foreign('coin_2_id')->references('id')->on('coins')->onDelete('set null');
            $table->foreign('coin_3_id')->references('id')->on('coins')->onDelete('set null');
            $table->foreign('coin_4_id')->references('id')->on('coins')->onDelete('set null');
            $table->foreign('coin_5_id')->references('id')->on('coins')->onDelete('set null');
            $table->foreign('coin_6_id')->references('id')->on('coins')->onDelete('set null');
            $table->foreign('coin_7_id')->references('id')->on('coins')->onDelete('set null');
            $table->foreign('coin_8_id')->references('id')->on('coins')->onDelete('set null');

                  

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('machines');
	}

}
