<?php

use Illuminate\Database\Seeder;

class MachinesTableSeeder extends Seeder
{

    /*
     * 8 coin1id not exist in coins table (including 0)
     * 1037,241,248,485,487,917,919,922
     *
     * 4 coin2id not exist
     * 486,918,920,921
     *
     * 3 coin3id not exist
     * 975,887,1089
     *
     * 3 coin4id not exist
     * 975,887,1089

    */

    public function run()
    {
        $this->command->info('Seeding Machines:');

        DB::table('machines')->delete();
        
        DB::table('machines')->insert([
            [
                'id' => 1,
                'location_id' => 1,
                'name' => 'Test Machine 1',
                'coins' => 1,
                'coin_1_id' => 1
            ],
        ]);

        /******************************************************************************************/
        $settings = \App\MachineSettings::all();

        $coin_ids = \App\Coin::all('id');

	    $button_lights = $settings->where('setting','button_lights')->map(function($item,$key){
		    $item->type = json_decode($item->data)->type;
		    return $item;
	    });

	    $gizmo_fws = $settings->where('setting','gizmo_fws')->map(function($item,$key){
		    $item->type = json_decode($item->data)->version;
		    return $item;
	    });

	    $machine_keys = $settings->where('setting','machine_keys')->map(function($item,$key){
		    $item->machine_key = json_decode($item->data)->machine_key;
		    return $item;
	    });

        $machine_types = $settings->where('setting','machine_types')->map(function($item,$key){
            $item->type = json_decode($item->data)->type;
            return $item;
        });

        $motors = $settings->where('setting','motors')->map(function($item,$key){
            $item->voltage = json_decode($item->data)->voltage;
            return $item;
        });

	    $prices = $settings->where('setting','prices')->map(function($item,$key){
		    $item->prices = json_decode($item->data)->prices;
		    return $item;
	    });



        /******************************************************************************************/
        $cc_modems = \App\Parts\CcModem::all();
        $cc_modem_models = collect();


        /******************************************************************************************/


        $url = 'http://my.medallionmint.com/json_exporters/exporter.php?key=1Zx33S908zi$$Wet33&method=machines';
        $machines = json_decode(file_get_contents($url));

        $this->command->getOutput()->progressStart(count($machines));

        $new_machines = array();

        foreach($machines as $machine){

            $cc_modem = null;

            if($machine->cccompany == '' || strtolower($machine->cccompany) == 'na' ){
               $machine->cccompany = 'Unknown Company';
            }

            if($machine->ccmodemmodel == '' || strtolower($machine->ccmodemmodel) == 'na' ){
               $machine->ccmodemmodel = 'Unknown Model';
            }

            if($machine->ccmodemserial != '' && $machine->ccmodemserial != 'na'){

                $cc_modem_data = '{"company":"'.$machine->cccompany.'","model":"'.$machine->ccmodemmodel.'"}';
                $cc_modem_model = $cc_modem_models->where('data',$cc_modem_data)->first();

                if(!$cc_modem_model){

                    $cc_modem_model = \App\MachineSettings::create([
                        'setting' => 'cc_modem_models',
                        'data'    => $cc_modem_data
                    ]);

                    $cc_modem_models->push($cc_modem_model);
                }

                //firstOrCreate() easy to use but runs extra db queries

                $cc_modem = $cc_modems
                    ->where('model_id',$cc_modem_model->id)
                    ->where('serial',$machine->ccmodemserial)
                    ->first();

                if(!$cc_modem){

                    $cc_modem = \App\Parts\CcModem::create([
                        'model_id' => $cc_modem_model->id,
                        'serial'   => $machine->ccmodemserial
                    ]);

                    $cc_modems->push($cc_modem);

                }


            }

            $new_machine = [
                'id'                => $machine->machineid,
                'name'              => $machine->machinedesc == '' ? 'Untitled Machine': $machine->machinedesc,
                'location_id'       => $machine->locationid,
                'active'            => strtolower($machine->active) == 'yes' ? true : false,

                'asset_num_1'      => $machine->assetnum,
                'asset_num_2'      => $machine->assetnum2,
                'date_shipped'      => date('Y-m-d',strtotime($machine->dateshipped)),
                'date_installed'    => date('Y-m-d',strtotime($machine->dateinstalled)),

                'coins'                => $machine->numcoins,
                'coin_1_id'            => $coin_ids->contains($machine->coin1id) ? $machine->coin1id : null,
                'coin_2_id'            => $coin_ids->contains($machine->coin2id) ? $machine->coin2id : null,
                'coin_3_id'            => $coin_ids->contains($machine->coin3id) ? $machine->coin3id : null,
                'coin_4_id'            => $coin_ids->contains($machine->coin4id) ? $machine->coin4id : null,
                'coin_5_id'            => null,
                'coin_6_id'            => null,
                'coin_7_id'            => null,
                'coin_8_id'            => null,

                'shipped_meter_1'   => $machine->meter1shipped,
                'shipped_meter_2'   => $machine->meter2shipped,
                'shipped_meter_3'   => $machine->meter3shipped,
                'shipped_meter_4'   => $machine->meter4shipped,
                'shipped_meter_5'   => $machine->meter5shipped,
                'shipped_meter_6'   => $machine->meter6shipped,

                'shipped_coin_1_id' => $machine->coin1shipped,
                'shipped_coin_2_id' => $machine->coin2shipped,
                'shipped_coin_3_id' => $machine->coin3shipped,
                'shipped_coin_4_id' => $machine->coin4shipped,

                'shipped_coin_1_quantity' => $machine->coin1shippedquantity,
                'shipped_coin_2_quantity' => $machine->coin2shippedquantity,
                'shipped_coin_3_quantity' => $machine->coin3shippedquantity,
                'shipped_coin_4_quantity' => $machine->coin4shippedquantity,

                'starting_meter_1'  => $machine->meter1starting,
                'starting_meter_2'  => $machine->meter2starting,
                'starting_meter_3'  => $machine->meter3starting,
                'starting_meter_4'  => $machine->meter4starting,
                'starting_meter_5'  => $machine->meter5starting,
                'starting_meter_6'  => $machine->meter6starting,

                'current_meter_1'   => $machine->meter1current,
                'current_meter_2'   => $machine->meter2current,
                'current_meter_3'   => $machine->meter3current,
                'current_meter_4'   => $machine->meter4current,
                'current_meter_5'   => $machine->meter5current,
                'current_meter_6'   => $machine->meter6current,

                'machine_types'     => ($setting = $machine_types->where('type',$machine->machinetype)->first() ) ? $setting->id : null,
                'prices'            => ($setting = $prices->where('prices',$machine->price)->first() ) ? $setting->id : null,
                'machine_keys'      => ($setting = $machine_keys->where('machine_key',$machine->keynum)->first() ) ? $setting->id : null,
                'motors'            => ($setting = $motors->where('voltage',$machine->motors)->first() ) ? $setting->id : null,
                'button_lights'     => ($setting = $button_lights->where('type',$machine->buttons)->first() ) ? $setting->id : null,
                'gizmo_fws'         => ($setting = $gizmo_fws->where('version',$machine->gizmofw)->first() ) ? $setting->id : null,



                'has_cc'            => strtolower($machine->ccyesno) == 'yes' ? true : false,
                'cc_modem_id'       => $cc_modem ? $cc_modem->id : null,


            ];

            array_push($new_machines, $new_machine);

            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();


        DB::table('machines')->insert($new_machines);

    }
}
