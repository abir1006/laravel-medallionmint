<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('companies')->delete();

        $companies = array(
            array('id' => 1, 'name' => 'Bass Pro Shops', 'website' => 'basspro.com'),
            array('id' => 2, 'name' => 'Ripleys', 'website' => 'ripleys.com'),
            array('id' => 3, 'name' => 'Cedar Fair', 'website' => 'cedarfair.com')
        );

        DB::table('companies')->insert($companies);
    }
}