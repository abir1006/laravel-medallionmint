<?php

use Illuminate\Database\Seeder;

class CoinsTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('coins')->delete();

        $test_coins = array(
            array('id' 				=> 1, 
            	'name' 				=> 'Test Coin 1',
            	'location_id' 		=> 1, 
            	'top_inventory' 	=> '125' , 
            	'inventory' 		=> '500' , 
            	'metal' 			=> 'Brass', 
            	'finish' 			=> 'Shiny', 
            	'front_dies_type' 	=> 'Location Dies', 
            	'back_dies_type' 	=> 'Location Dies', 
            	'front_dies' 		=> 2, 
            	'back_dies' 		=> 3),
            array('id' 				=> 2, 
            	'name' 				=> 'Test Coin 2', 
            	'location_id' 		=> 1, 
            	'top_inventory' 	=> '125' , 
            	'inventory' 		=> '1500' , 
            	'metal' 			=> 'Brass', 
            	'finish' 			=> 'Shiny', 
            	'front_dies_type' 	=> 'Location Dies', 
            	'back_dies_type' 	=> 'Location Dies', 
            	'front_dies' 		=> 2, 
            	'back_dies' 		=> 3)
        );

        DB::table('coins')->insert($test_coins);


        /******************************************************************************************/


        $url = 'http://my.medallionmint.com/json_exporters/exporter.php?key=1Zx33S908zi$$Wet33&method=coins';
        $coins = json_decode(file_get_contents($url));

        $new_coins = array();

        foreach($coins as $coin){

            /*
             3 locations doesn't exist , but using at coins table

            41 Bass Pro - Las Vegas,
            54 Greater Cleveland Aquarium
            383 Crayola Experience CTM

            10 coins using above 3 locationid
            27,28,59,60,61,62,1962,1963,1964,1965
            */

            if($coin->locationid == 41 || $coin->locationid == 54 || $coin->locationid == 383){
                continue;
            }

            $new_coin = [
                'id'            => $coin->coinid,
                'name'          => $coin->coindesc,
                'location_id'   => $coin->locationid,
                'metal'         => $coin->metal,
                'finish'        => $coin->finish,
                'top_inventory' => $coin->topinventory,
                'inventory'     => $coin->inventory,
            ];

            array_push($new_coins, $new_coin);
        }

        DB::table('coins')->insert($new_coins);
    }
}