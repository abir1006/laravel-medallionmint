<?php

use Illuminate\Database\Seeder;

class RepCompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    DB::table('rep_companies')->delete();

	    $rep_companies = array(
		    array('id' => 1, 'name' => 'Characters Unlimited Inc'),
		    array('id' => 2, 'name' => 'CTM Group, Inc.'),
		    array('id' => 3, 'name' => 'Medallion Mint'),
	    );

	    DB::table('rep_companies')->insert($rep_companies);
    }
}
