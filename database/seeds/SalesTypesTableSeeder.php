<?php

use Illuminate\Database\Seeder;

class SalesTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('sales_types')->delete();

	    $sales_types = array(
		    array('id' => 1, 'active' => 1, 'name' => 'Revenue Share'),
		    array('id' => 2, 'active' => 1, 'name' => 'Purchase'),
	    );

	    DB::table('sales_types')->insert($sales_types);
    }
}
