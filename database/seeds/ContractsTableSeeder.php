<?php

use Illuminate\Database\Seeder;

class ContractsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('contracts')->delete();

        $url = 'http://my.medallionmint.com/json_exporters/exporter.php?key=1Zx33S908zi$$Wet33&method=contracts';
        $contracts = json_decode(file_get_contents($url));

        $new_contracts = array();

        foreach($contracts as $contract){

            $new_contract = [
                'location_id'   => $contract->location_id,
                'terms'         => $contract->terms,
                'start_date'    => $contract->start_date,
                'end_date'      => $contract->end_date,
                'auto_renew'    => $contract->auto_renew,

                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s'),
            ];

            array_push($new_contracts,$new_contract);
        }

        DB::table('contracts')->insert($new_contracts);


    }
}
