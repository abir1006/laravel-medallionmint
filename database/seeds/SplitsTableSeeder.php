<?php

use Illuminate\Database\Seeder;

use App\Split;

class SplitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $this->command->warn('Seeding Splits:');

//        DB::table('splits')->delete();

	    $url = 'http://my.medallionmint.com/json_exporters/exporter.php?key=1Zx33S908zi$$Wet33&method=splits';
	    $Objects = json_decode(file_get_contents($url));

	    $this->command->getOutput()->progressStart(count($Objects));

	    foreach($Objects as $object) {
		    $oSplit = Split::where('id', $object->splitid)->first();
		    if(!$oSplit){
			    $oSplit = new Split();
			    $oSplit->id = $object->splitid;
		    }

		    $oSplit->name = (string) $object->splitdesc;
		    $oSplit->split = $object->repsplit;
		    $oSplit->save();

		    $this->command->getOutput()->progressAdvance();
	    }

	    $this->command->getOutput()->progressFinish();
    }
}
