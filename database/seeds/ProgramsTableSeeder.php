<?php

use Illuminate\Database\Seeder;
use App\Program;

class ProgramsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $this->command->warn('Seeding Programs:');

//        DB::table('programs')->delete();

	    $url = 'http://my.medallionmint.com/json_exporters/exporter.php?key=1Zx33S908zi$$Wet33&method=programs';
	    $Objects = json_decode(file_get_contents($url));

	    $this->command->getOutput()->progressStart(count($Objects));

	    foreach($Objects as $object) {
		    $oProgram = Program::where('id', $object->id)->first();
		    if(!$oProgram){

			    $oProgram = new Program();
			    $oProgram->id = $object->id;
		    }

		    $oProgram->name = trim((string) $object->program);
		    $oProgram->value =$object->amount;

		    $oProgram->save();

		    $this->command->getOutput()->progressAdvance();
	    }
	    $this->command->getOutput()->progressFinish();
    }
}
