<?php

use Illuminate\Database\Seeder;

class TermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('terms')->delete();

	    $terms = array(
		    array('id' => 1, 'name' => '1% 10 Net 30'),
		    array('id' => 2, 'name' => '1/2 Due with order'),
		    array('id' => 3, 'name' => '2% 10 Net 30'),
		    array('id' => 4, 'name' => 'As Noted'),
		    array('id' => 5, 'name' => 'Auto Draft Payment'),
		    array('id' => 6, 'name' => 'Consignment'),
		    array('id' => 7, 'name' => 'Due 10th of month'),
		    array('id' => 8, 'name' => 'Due by 10th of month'),
		    array('id' => 9, 'name' => 'Due June 30th'),
		    array('id' => 10, 'name' => 'Due May 30th'),
		    array('id' => 11, 'name' => 'Due on receipt'),
		    array('id' => 12, 'name' => 'Net 10 Days'),
		    array('id' => 13, 'name' => 'Net 15 Days'),
		    array('id' => 14, 'name' => 'Net 25 Days'),
		    array('id' => 15, 'name' => 'Net 30 Days'),
		    array('id' => 16, 'name' => 'Net 60 Days'),
		    array('id' => 17, 'name' => 'Net 90 Days'),
		    array('id' => 18, 'name' => 'Net 120 Days'),
	    );

	    DB::table('terms')->insert($terms);
    }
}
