<?php

use Illuminate\Database\Seeder;

class MachineSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('machine_settings')->delete();

        DB::table('machine_settings')->insert([
	        ['setting'=>'gizmo_fws','data'=>'{"version":"00.91.01"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.00.00 Test"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.00.01"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.01.01"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.01.02"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.01.03"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.01.04"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.01.05"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.01.06-F"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.01.08-Apr"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.01.07-A"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.01.02b"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"01.01.02c"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"2.2.10-15"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"2.2.11-1"}','created_at'=>'2015-08-31 15:46:10'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"2.3.0-2"}','created_at'=>'2015-08-31 16:11:00'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"2.3.1-1"}','created_at'=>'2016-02-05 17:14:43'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"2.3.3-1"}','created_at'=>'2016-04-07 08:51:30'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"2.3.4-1"}','created_at'=>'2016-07-28 09:15:32'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"2.3.5-1"}','created_at'=>'2016-08-25 15:17:13'],
	        ['setting'=>'gizmo_fws','data'=>'{"version":"2.4.0-1"}','created_at'=>'2016-11-03 09:10:24']
        ]);

        DB::table('machine_settings')->insert([
            ['setting'=>'prices','data'=>'{"prices":"$5 each","level_1_credit":"5","level_1_coin":"1","level_2_credit":"5","level_2_coin":"1"}'],
            ['setting'=>'prices','data'=>'{"prices":"$3 each or 2 for $5","level_1_credit":"3","level_1_coin":"1","level_2_credit":"5","level_2_coin":"2"}'],
            ['setting'=>'prices','data'=>'{"prices":"$4 each or 3 for $10","level_1_credit":"4","level_1_coin":"1","level_2_credit":"10","level_2_coin":"3"}'],
            ['setting'=>'prices','data'=>'{"prices":"$5 each or 3 for $10","level_1_credit":"5","level_1_coin":"1","level_2_credit":"10","level_2_coin":"3"}'],
            ['setting'=>'prices','data'=>'{"prices":"$5 each or 4 for $15","level_1_credit":"5","level_1_coin":"1","level_2_credit":"15","level_2_coin":"4"}'],
            ['setting'=>'prices','data'=>'{"prices":"$1 each or 5 for $5","level_1_credit":"1","level_1_coin":"1","level_2_credit":"5","level_2_coin":"5"}'],
            ['setting'=>'prices','data'=>'{"prices":"$4 each or 3 for $12","level_1_credit":"4","level_1_coin":"1","level_2_credit":"12","level_2_coin":"3"}'],
            ['setting'=>'prices','data'=>'{"prices":"$5 each or 3 for $12","level_1_credit":"5","level_1_coin":"1","level_2_credit":"12","level_2_coin":"3"}'],
            ['setting'=>'prices','data'=>'{"prices":"$3 each or 4 for $10","level_1_credit":"3","level_1_coin":"1","level_2_credit":"10","level_2_coin":"4"}'],
        ]);

        DB::table('machine_settings')->insert([
            ['setting'=>'machine_types','data'=>'{"type":"Wood(Oak)"}'],
            ['setting'=>'machine_types','data'=>'{"type":"Graphics"}'],
            ['setting'=>'machine_types','data'=>'{"type":"Wood"}'],
            ['setting'=>'machine_types','data'=>'{"type":"Plexi Wrapped Graphics"}'],
            ['setting'=>'machine_types','data'=>'{"type":"Outdoor"}']
        ]);

        DB::table('machine_settings')->insert([
            ['setting'=>'machine_keys','data'=>'{"machine_key":"L56804"}'],
            ['setting'=>'machine_keys','data'=>'{"machine_key":"YH1"}'],
            ['setting'=>'machine_keys','data'=>'{"machine_key":"YH2"}'],

        ]);

        DB::table('machine_settings')->insert([
            ['setting'=>'motors','data'=>'{"company":"Unknown Company", "voltage":"6v"}'],
            ['setting'=>'motors','data'=>'{"company":"Unknown Company", "voltage":"12v"}'],

        ]);

        DB::table('machine_settings')->insert([
            ['setting'=>'button_lights','data'=>'{"company":"Unknown Company","type":"Incandescent"}'],
            ['setting'=>'button_lights','data'=>'{"company":"Unknown Company","type":"LED"}'],
        ]);
    }
}
