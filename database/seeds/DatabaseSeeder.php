<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

    public function run()
    {
        $this->call(TimeZonesSeeder::class);
        $this->call(CarriersTableSeeder::class);
   
        $this->call(MMRepsTableSeeder::class);
        $this->call(ProgramsTableSeeder::class);
        $this->call(ManagingCompaniesTableSeeder::class);
        $this->call(RepCompaniesTableSeeder::class);
        $this->call(SplitsTableSeeder::class);
        $this->call(SalesTypesTableSeeder::class);
        $this->call(LocationTypesTableSeeder::class);
        $this->call(TermsTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(RepCompaniesTableSeeder::class);
        $this->call(LocationsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CoinsTableSeeder::class);
        $this->call(MachineSettingsTableSeeder::class);
        $this->call(MachinesTableSeeder::class);
        $this->call(DiesTableSeeder::class);
        $this->call(ContractsTableSeeder::class);

    }

}