<?php

use Illuminate\Database\Seeder;

class LocationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('location_types')->delete();

	    $location_types = array(
		    array('id' => 1, 'name' => 'Amusement Park'),
		    array('id' => 2, 'name' => 'Aquarium'),
		    array('id' => 3, 'name' => 'Cavern / Cave'),
		    array('id' => 4, 'name' => 'Dinosaur'),
		    array('id' => 5, 'name' => 'Gift Shop'),
		    array('id' => 6, 'name' => 'Iconic'),
		    array('id' => 7, 'name' => 'Museum'),
		    array('id' => 8, 'name' => 'Park / National Park'),
		    array('id' => 9, 'name' => 'Tourist Destination'),
		    array('id' => 10, 'name' => 'Zoo'),
	    );

	    DB::table('location_types')->insert($location_types);
    }
}
