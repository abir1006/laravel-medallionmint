<?php

use Illuminate\Database\Seeder;

use App\Location;
use App\LocationType;
use App\MmRep;
use App\Program;
use App\RepCompany;
use App\SalesType;
use App\Split;
use App\Term;

class LocationsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('locations')->delete();

        $test_locations = [
             ['id' => 1,'company_id' => 1,'rep_company_id' => 1, 'name' => 'Test Location 001' ],
             ['id' => 2,'company_id' => 1,'rep_company_id' => 1, 'name' => 'Test Location 002' ],
             ['id' => 3,'company_id' => 1,'rep_company_id' => 2, 'name' => 'Test Location 003'],
             ['id' => 4,'company_id' => 2,'rep_company_id' => 2, 'name' => 'Test Location 004'],
        ];

        DB::table('locations')->insert($test_locations);

        /******************************************************************************************/


        $url = 'http://my.medallionmint.com/json_exporters/exporter.php?key=1Zx33S908zi$$Wet33&method=locations';
        $locations = json_decode(file_get_contents($url));

        $new_locations = array();

        $location_types = LocationType::all();
        $mm_reps = MmRep::all();
        $programs = Program::all();
        $rep_companies = RepCompany::all();
        $sales_types = SalesType::all();
        $splits = Split::all();
        $terms = Term::all();

        foreach($locations as $location){

	        $location_type = $location_types->where('name', $location->location_type)->first();
	        $mm_rep = $mm_reps->where('name', $location->mm_rep)->first();
	        $program = $programs->where('name', $location->program)->first();
	        $rep_company = $rep_companies->where('name', $location->repcompanyname)->first();
	        $sales_type = $sales_types->where('name', $location->sales_type)->first();
	        $split = $splits->where('name', $location->split)->first();
	        $term = $terms->where('id', $location->termid)->first();

            $new_location = [
                'id'    => $location->locationid,
                'name'  => $location->locationname,
	            'location_type_id' => $location_type?$location_type->id:null,
	            'mm_rep_id' => $mm_rep?$mm_rep->id:null,
	            'program_id' => $program?$program->id:null,
	            'rep_company_id' => $rep_company?$rep_company->id:null,
	            'sales_type_id' => $sales_type?$sales_type->id:null,
	            'split_id' => $split?$split->id:null,
	            'term_id' => $term?$term->id:null,
            ];

            array_push($new_locations, $new_location);
        }

        DB::table('locations')->insert($new_locations);

    }

}