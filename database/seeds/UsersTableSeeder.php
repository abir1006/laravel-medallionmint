<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    public function run()
    {

        DB::table('users')->delete();

        $users = [
        
            ['id' => 1, 'name' => 'Fida Hasan', 'email' => 'fidahasan08@gmail.com', 'password' => Hash::make('123'), 'role' => 'admin','created_at'=>'2015-1-25'],
            ['id' => 2, 'name' => 'Sumu Hasan', 'email' => 'sumuhasan08@gmail.com', 'password' => Hash::make('123'), 'role' => 'company_manager','created_at'=>'2015-1-25'],
            ['id' => 3, 'name' => 'Tim McClure', 'email' => 'tim@medallionmint.com', 'password' => Hash::make('123'), 'role' => 'admin','created_at'=>'2015-1-25'],
            ['id' => 4, 'name' => 'Pam McClure', 'email' => 'pam@medallionmint.com', 'password' => Hash::make('123'), 'role' => 'location_manager','created_at'=>'2015-1-25']
        ];

        DB::table('users')->insert($users);


        DB::table('location_managers')->delete();
        DB::table('location_managers')->insert(
            [
                ['location_id'=>'1','user_id'=>'4'],
                ['location_id'=>'2','user_id'=>'4'],
                ['location_id'=>'3','user_id'=>'4']
            ]
        );

        DB::table('company_managers')->delete();
        DB::table('company_managers')->insert(['company_id'=>'1','user_id'=>'2']);


    }
}