<?php

use Illuminate\Database\Seeder;

class ManagingCompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('managing_companies')->delete();

	    $managing_companies = array(
		    array('id' => 1, 'name' => 'Evelyn Hills'),
		    array('id' => 2, 'name' => 'Xanterra'),
		    array('id' => 3, 'name' => 'Wildlife Trading Co'),
	    );

	    DB::table('managing_companies')->insert($managing_companies);
    }
}
