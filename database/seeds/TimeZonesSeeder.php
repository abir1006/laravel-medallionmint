<?php

use Illuminate\Database\Seeder;

class TimeZonesSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('time_zones')->insert([
			[
				'id' => 1,
				'name' => 'EST Eastern Standard Time',
				'value' => -5
			],
			[
				'id' => 2,
				'name' => 'CST Central Standard Time',
				'value' => -6
			],
			[
				'id' => 3,
				'name' => 'MT Rocky Mountain Time Zone',
				'value' => -6
			],
			[
				'id' => 4,
				'name' => 'PST Pacific Standard Time',
				'value' => -8
			],
			[
				'id' => 5,
				'name' => 'AKDT Alaska Time Zone',
				'value' => -9
			],
			[
				'id' => 6,
				'name' => 'HAST Hawaii Aleutian Standard Time',
				'value' => -10
			],
		]);
	}
}