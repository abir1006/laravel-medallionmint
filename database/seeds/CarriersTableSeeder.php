<?php

use Illuminate\Database\Seeder;

class CarriersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('carriers')->insert([
            [
                'id' => 1,
                'slug' => 'usps',
                'name' => 'USPS',
                'type' => 'Small Package',
                'track_url'   => 'https://tools.usps.com/go/TrackConfirmAction.action?tRef=fullpage&tLc=1&tLabels={track_code}',
	            'rate_per_box' => '13.45'
            ],
		    [
			    'id' => 2,
			    'slug' => 'ups',
			    'name' => 'UPS',
			    'type' => 'Small Package',
			    'track_url'   => 'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums={track_code}',
			    'rate_per_box' => ''
		    ],
		    [
			    'id' => 3,
			    'slug'  => 'fedex',
			    'name' => 'FedEx',
			    'type' => 'Small Package',
			    'track_url' => '',
			    'rate_per_box' => ''
		    ],
		    [
			    'id' => 4,
			    'slug'  => 'other',
			    'name' => 'Other',
			    'type' => '',
			    'track_url' => '',
			    'rate_per_box' => ''
		    ],
	    ]);
    }
}
