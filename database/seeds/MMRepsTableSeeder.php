<?php

use Illuminate\Database\Seeder;

class MMRepsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('mm_reps')->delete();

	    $mm_reps = array(
		    array('id' => 1, 'name' => 'House'),
		    array('id' => 2, 'name' => 'Camelia'),
		    array('id' => 3, 'name' => 'Cynthia'),
		    array('id' => 4, 'name' => 'Pam'),
		    array('id' => 5, 'name' => 'Tim'),
		    array('id' => 6, 'name' => 'Timilie'),
	    );

	    DB::table('mm_reps')->insert($mm_reps);
    }
}
